﻿package org.devboy.hydra.examples.clickcommand
{
	import flash.events.TransformGestureEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import org.devboy.hydra.examples.videostream.PublishStreamCommand;
	import org.devboy.hydra.HydraChannel;
	import org.devboy.hydra.HydraEvent;
	import org.devboy.hydra.HydraService;
	import org.devboy.hydra.commands.HydraCommandEvent;
	import org.devboy.hydra.stream.DirectConnectionChannel;
	import utils.DebugWindow;

	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.GroupSpecifier;

	/**
	 * @author Dominic Graefen - devboy.org
	 */
	public class ClickCommandExample extends Sprite
	{
		public var _hydraService : HydraService;
		public var _clickChannel : DirectConnectionChannel;
		private var _shapeContainer : Sprite;
		private var d : TextField = new TextField();
	
		public function ClickCommandExample()
		{
			init();
			_shapeContainer = new Sprite();
			_shapeContainer.graphics.beginFill(0, 0);
			_shapeContainer.graphics.drawRect(0, 0, 1000, 1000);
			_shapeContainer.graphics.endFill();
		//addChild(_shapeContainer);
			//addEventListener(Event.ENTER_FRAME, enterFrame);
			//addEventListener(MouseEvent.MOUSE_MOVE, mouseClick );
			//Multitouch.inputMode = MultitouchInputMode.GESTURE
			//stage.addEventListener(TransformGestureEvent.GESTURE_SWIPE , onSwipe)
			//addChild(d)
			trace("CITRUS STARTEDsssssssss")
			
		}
		
		

		private function init() : void
		{
			var stratusServiceUrl : String = "rtmfp://stratus.rtmfp.net/da57d3de270c92ad0a05d643-4dc0ea403d51";
			_hydraService = new HydraService("HydraClickExample", stratusServiceUrl);
			_hydraService.addEventListener(HydraEvent.SERVICE_CONNECT_SUCCESS, serviceEvent);
			_hydraService.addEventListener(HydraEvent.SERVICE_CONNECT_FAILED, serviceEvent);
			_hydraService.addEventListener(HydraEvent.SERVICE_CONNECT_REJECTED, serviceEvent);
			var groupSpecifier : GroupSpecifier = new GroupSpecifier("HydraClickExample/ClickChannel");
			groupSpecifier.postingEnabled = true;
			groupSpecifier.serverChannelEnabled = true;
			_clickChannel = new DirectConnectionChannel(_hydraService, "HydraClickExample/ClickChannel", groupSpecifier, false );
			_clickChannel.addEventListener(HydraCommandEvent.COMMAND_RECEIVED, handleCommand);
			_hydraService.commandFactory.addCommandCreator(new ClickCommandCreator());
			//_hydraService.commandFactory.addCommandCreator(new PublishStreamCommand());
			_hydraService.connect(new Date().time.toFixed(0)+"/"+(Math.random()*100000).toFixed());
			
			}

		private function enterFrame(event : Event) : void
		{
			var invisible : Vector.<DisplayObject> = new Vector.<DisplayObject>();
			var displayObject : DisplayObject;
			var i : int = 0;
			var l : int = _shapeContainer.numChildren;
			for( ;i<l;i++ )
			{
				displayObject = _shapeContainer.getChildAt(i);
				displayObject.alpha -= 0.2;
				displayObject.scaleX -= 0.2;		
				displayObject.scaleY -= 0.2;
				if( displayObject.alpha < 0 )
					invisible.push(displayObject);		
			}
			for each(displayObject in invisible)
				_shapeContainer.removeChild(displayObject);
		}

		private function mouseClick(event : MouseEvent) : void
		{
			if(_hydraService.connected && _clickChannel.connected) {
			_clickChannel.sendDirectCommand( new ClickCommand(mouseX, mouseY) );
			
			}
		}
		
		private function handleCommand(event : HydraCommandEvent) : void
		{
			trace("Recived")
			switch( event.command.type )
			{
				case ClickCommand.TYPE:
					handleRemoteClick( event.command as ClickCommand );
					break;	
			}
		}

		private function handleRemoteClick(clickCommand : ClickCommand) : void
		{
			DebugWindow.Trace("G", clickCommand.x ,clickCommand.y )
			dispatchEvent(new CEvent("MoveUDP", clickCommand))
			//hand.x += clickCommand.x
			//hand.y += clickCommand.y
			//trace("Recived", clickCommand.x)
			//if (clickCommand.x == -1) {
				//trace("Inn")
				//stage.dispatchEvent(new Event("loadPrev"))
				//}
			//else {
				//stage.dispatchEvent(new Event("loadNext"))
				//}
		}

		private function serviceEvent(event : HydraEvent) : void
		{
			trace(event ,99);
		}
	}
}
