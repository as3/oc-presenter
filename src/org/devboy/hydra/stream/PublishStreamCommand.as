package org.devboy.hydra.stream
{
	import org.devboy.hydra.commands.HydraCommand;

	/**
	 * @author Dominic Graefen - devboy.org
	 */
	public class PublishStreamCommand extends HydraCommand
	{
		public static const TYPE : String = "org.devboy.hydra.examples.videostream.PublishStreamCommand.TYPE";
        private var _streamId:String;
        private var _color : uint;

		public function PublishStreamCommand( streamId : String, color : uint )
		{
            _streamId = streamId;
            _color = color;
			super(TYPE);
		}

		override public function get info() : Object
		{
			var info : Object = new Object();
				info.streamId = _streamId;
                info.color = _color;
			return info;
		}

        public function get streamId():String {
            return _streamId;
        }

        public function get color():uint {
            return _color;
        }
    }
}