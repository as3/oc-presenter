package org.devboy.hydra.stream {
import flash.events.NetStatusEvent;
import flash.media.Video;
import flash.net.GroupSpecifier;

import flash.net.NetStream;

import org.devboy.hydra.HydraChannel;
import org.devboy.hydra.HydraEvent;
import org.devboy.hydra.HydraService;
import org.devboy.hydra.commands.HydraCommandEvent;
import org.devboy.hydra.commands.IHydraCommand;
import org.devboy.hydra.users.HydraUserEvent;

public class DirectConnectionChannel extends HydraChannel
{
    private var _publisher : NetStream;
    private var _publisherID : String;
    private var _receivers : Vector.<NetStream>;
    private var _receiverIDs : Vector.<String>;

    public function DirectConnectionChannel(hydraService : HydraService, channelId : String, specifier : GroupSpecifier, withAuthorization : Boolean, autoConnect : Boolean = true)
    {
        super(hydraService,channelId,specifier,withAuthorization,autoConnect);
        init();
    }

    private function init() : void
    {
        hydraService.commandFactory.addCommandCreator( new PublishStreamCommandCreator() );
        addEventListener( HydraCommandEvent.COMMAND_RECEIVED, commandReceived );
        addEventListener( HydraEvent.CHANNEL_CONNECT_SUCCESS, channelConnected );
        addEventListener( HydraUserEvent.USER_CONNECT, userUpdate );
        addEventListener( HydraUserEvent.USER_DISCONNECT, userUpdate );
        _receivers = new Vector.<NetStream>();
        _receiverIDs = new Vector.<String>();
    }

    private function userUpdate(event:HydraUserEvent):void
    {
        trace("DirectConnectionChannel->userUpdate");
        if( _publisher )
            sendCommand( new PublishStreamCommand(_publisherID,0) );    
    }

    private function channelConnected(event:HydraEvent):void
    {
        trace("DirectConnectionChannel->channelConnected");
        _publisherID = channelId+"/"+hydraService.user.uniqueId;
        _publisher = new NetStream(hydraService.netConnection,NetStream.DIRECT_CONNECTIONS);
        _publisher.publish(_publisherID);
        sendCommand( new PublishStreamCommand(_publisherID,0) );
    }

    private function commandReceived( event:HydraCommandEvent ):void
    {
        switch( event.command.type )
        {
            case PublishStreamCommand.TYPE:
                handlePublishedStream(event.command as PublishStreamCommand);
                break;
        }
    }

    private function handlePublishedStream(publishStreamCommand:PublishStreamCommand):void
    {
        trace("DirectConnectionChannel->handlePublishedStream");
        if(containsStreamID(publishStreamCommand.streamId))
            return;

        var receiver : NetStream = new NetStream(hydraService.netConnection,publishStreamCommand.senderPeerId);
            receiver.addEventListener( NetStatusEvent.NET_STATUS, receiverNetStatus );
            receiver.client = this;
            receiver.play(publishStreamCommand.streamId);
        _receivers.push(receiver);
        _receiverIDs.push(publishStreamCommand.streamId);
    }

    public function sendDirectCommand( command : IHydraCommand ) : void
    {
        if(!_publisher)
            throw new Error( "Publisher not instanciated yet!" );

        command.userId = hydraService.user.uniqueId;
		command.timestamp = new Date().getTime();
		var message : Object = new Object();
		message.userId = command.userId;
		message.type = command.type;
		message.timestamp = command.timestamp;
		message.info = command.info;
		message.senderPeerId = hydraService.netConnection.nearID;
	    _publisher.send("receiveCommand",message);
	    dispatchEvent( new HydraCommandEvent(HydraCommandEvent.COMMAND_SENT, command));
    }

    public function receiveCommand( message : Object ) : void
    {
        var userId : String = message.userId;
		var type : String = message.type;
		var timestamp : Number = message.timestamp;
		var info : Object = message.info;
		var senderPeerId : String = message.senderPeerId;
		var command : IHydraCommand = hydraService.commandFactory.createCommand(type, timestamp, userId, senderPeerId, info);
		if( command )
			dispatchEvent( new HydraCommandEvent(HydraCommandEvent.COMMAND_RECEIVED, command));
    }

    private function receiverNetStatus(event:NetStatusEvent):void
    {
        trace(event.info.code);
    }

    private function containsStreamID(streamID : String):Boolean
    {
        return _receiverIDs.indexOf(streamID)>-1;
    }
}
}