package slides
{
	import com.greensock.TweenMax;
	import data.Data;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import uk.soulwire.utils.display.Alignment;
	import uk.soulwire.utils.display.DisplayUtils;
	
	public class slideTextImage extends Sprite
	{
		
		private var Thiles:Vector.<MovieClip> = new Vector.<MovieClip>;
		private var dots:Array = [];
		private var baseURL:String = Globals.baseURL + 'resources/files/images/';
		private var dragedThumbnail:MovieClip;
		private var biImageLoade:Loader = new Loader();
		private var bit:Bitmap;
	
		public var numSteps:int = 1;
		public var fittofill:Boolean = false;
		
		public function slideTextImage(e:Object, generateThumbnail:Sprite = null)
		{
			
	
			fittofill = e.content.fit_to_fill == '0' ? false : true;
			if (e.content.bg_image && e.content.bg_image != "")
			{
				biImageLoade.contentLoaderInfo.addEventListener(Event.COMPLETE, showImage)
				
				var loaderContext:LoaderContext = new LoaderContext();
				if (Globals.isMobile)
				{
					loaderContext.imageDecodingPolicy = "onLoad"
				}
			
				Data.getInstance().getImage(e.id , displayLoadProgress , showImage);
				//return
				
				trace("IMG" , baseURL + e.content.bg_image);
				var requestAddress = new URLRequest(baseURL + e.content.bg_image);
				biImageLoade.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS , displayLoadProgress)
				biImageLoade.load(requestAddress, loaderContext)
				biImageLoade.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR , errorMYFIREND)
				addEventListener(Event.ADDED_TO_STAGE, init)
			}
			//trace(requestAddress.url, "CALLED SLIDE TEXT AND IMAGEW")
			
		}
	
		private function errorMYFIREND(e:IOErrorEvent):void 
		{
			trace("ERROR MY FRIEND")
		}
		
		private function displayLoadProgress(e:ProgressEvent):void 
		{
			dispatchEvent(e);
			
		}
		
		private function showImage(e:Event):void
		{

			bit = Bitmap(biImageLoade.content)
			bit.smoothing = true
			addChild(bit)
			dispatchEvent(e);
			Resize()
			Data.getInstance().ResumePreload()
			
		
		}
		
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init)
		}
		
		private function showProgress(e:Event):void
		{
		
		}
		
		public function Resize(e:Event = null)
		{
			
			if (bit)
			{
				var r:Rectangle = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight)
				DisplayUtils.fitIntoRect(bit, r, fittofill, Alignment.MIDDLE);
			}
		}
		
		public function NEXT(e = null)
		{
			stage.dispatchEvent(new Event(CEvent.LOAD_NEXT))
		}
		
		public function PREV(e = null)
		{
			stage.dispatchEvent(new Event(CEvent.LOAD_PREV))
		}
		
		public function TransitionDone()
		{
		}
		
		public function HideControls()
		{
		}
		
		public function ShowControls()
		{
		}
		
		public function Destroy()
		{
			if (biImageLoade)
			{
				biImageLoade.contentLoaderInfo.removeEventListener(Event.COMPLETE, showImage)
				biImageLoade.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS , displayLoadProgress)
				try
				{
					biImageLoade.unload()
					biImageLoade = null
				}
				catch (e)
				{
					trace("===ERR")
				}
				biImageLoade = null
			}
			if (bit)
			{
				bit = null
			}
		
		}
	
	}

}