package slides
{
	import com.greensock.TweenMax;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.ui.Mouse;
	
	public class slideVideo extends SlideVideolib
	{
		
		private var baseURL:String = Globals.baseURL+ 'resources/files/images/';
		private var baseURLvideo:String = Globals.baseURL+ 'resources/files/videos/';
		private var vp:VPVasa;
		private var videoPath:String;
		public var GetThumbnail:Sprite
		public var numSteps:int = 1;
		public var ID:int = 0;
		public function slideVideo(e:Object, generateThumbnail:Sprite = null)
		{
			trace(3343 , JSON.stringify(e))
			GetThumbnail = generateThumbnail
			
			if (e.content.title)
			{
				this.stuff.title.text = e.content.title
				this.stuff.title.visible = false
			}
			
			vp = new VPVasa();
			this.stuff.addChild(vp)
			
			if (GetThumbnail)
			{
				//private var baseURLvideo:String = Globals.baseURL+ 'resources/files/videos/';
				Resize()
				
				GetThumbnail.GetBit(this)
			}
		
			if (e.content.movie_file_high && e.content.movie_file_high != "" && !GetThumbnail)
			{
				trace("*****" , e.content.movie_file_high)
				videoPath =   e.content.movie_file_high
				addEventListener(Event.ADDED_TO_STAGE, init)
			}
			else
			{
				vp.playBtn.gotoAndStop(3)
			}
			
			
			bkg.addChild( new Bitmap(OCP.bkgBitmapDat))
		}
		public function pauseUnpose () {
			
			vp.pauseUnpose(null)
			
			}
		public function TransitionDone() {
			vp.resetTimer(null);
			
			}
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init)
			
			vp.setVideo(baseURLvideo + videoPath)
			//vp.setVideo('http://192.168.11.31/videogrinder/www/resources/videos/195/_1400828336_MultiTaction_iWall_10.m4v')
			trace("setting video path" , baseURLvideo + videoPath)
			vp.playIt(null);
			
			
			//caseiOS:
			//VPAccelerated
			
			
			//bkg.visible = false
			//bkg.alpha = .5
			//
			Resize()
		}
		
		private function closeSlide(e:MouseEvent):void
		{
			TweenMax.to(this, .4, {alpha: 0, onComplete: outDone})
		}
		
		private function outDone():void
		{
			dispatchEvent(new Event("close"))
		}
		
		private function showProgress(e:Event):void
		{
		
		}
		
		private function onError(e:Object):void
		{
			trace("error no internet")
		}
		
		public function Resize(e:Event = null)
		{
			
			
			if (GetThumbnail) {
			
					stuff.width = 1920
					stuffheight = 1200
			
				return
				}
				
				
			var WDiff = 1 - (1920 - Globals._DIM.width) / 1920
			var HDiff = 1 - (1080 - Globals._DIM.height) / 1080
			
			if (HDiff < WDiff)
			{
				WDiff = HDiff
			}
			
			this.bkg.width = Globals._DIM.width
			this.bkg.height = Globals._DIM.height
			this.stuff.scaleX = this.stuff.scaleY = WDiff //- .15
			this.stuff.x = Globals._DIM.width / 2 - this.stuff.width / 2
			this.stuff.y = Globals._DIM.height / 2 - this.stuff.height / 2
			//caseiOS:
			
				
			//if(stage && stage.stageVideos.length > 0 && Globals.isMobile) {
			//var point:Point   = localToGlobal(new Point(stuff.x, stuff.y))
			//stage.stageVideos[0].viewPort = new Rectangle( point.x + Globals._DIM.x , point.y + Globals._DIM.y, stuff.width, stuff.height)
			//}
			//
			
			
			var bw = 1 - ( bkg.getChildAt(0).width -  Globals._DIM.width) / bkg.getChildAt(0).width
			var bh = 1 - (bkg.getChildAt(0).height - Globals._DIM.height) /bkg.getChildAt(0).height
			if (bh > bw)
			{
				bw = bh
			}
			
			bkg.scaleX = bkg.scaleY = bw
			bkg.x =  Globals._DIM.width / 2 - bkg.width / 2
			bkg.y = Globals._DIM.height / 2 - bkg.height / 2
		}
		
		public function Destroy()
		{
			trace("SSSSS")
			if (vp)
			{
				
				Mouse.show()
				vp.Destroy()
				trace("Destouid video slide")
			}
		}
		
		public function NEXT(e=null)
		{
			trace("??????????????????", e)
			//trace( KeyboardEvent(e).bubbles, KeyboardEvent(e).bubbles, KeyboardEvent(e).cancelable)
			if ( ! vp.hitTestPoint(stage.mouseX, stage.mouseY) || KeyboardEvent(e).bubbles) {
				stage.dispatchEvent(new Event(CEvent.LOAD_NEXT))
				Mouse.show()
				}
			
		
		}
		
		public function HideControls()
		{
			
			vp.hideControls(null)
		
		}
		
		public function ShowControls()
		{
			Mouse.show()
			vp.showCOntrols(null)
		}
		
		public function PREV(e=null)
		{
			
			stage.dispatchEvent(new Event(CEvent.LOAD_PREV))
			Mouse.show()
		}
	
	}

}