package remote
{
	
	import com.adobe.crypto.MD5;
	import com.greensock.TweenMax;
	import controllers.PresentationController;
	import flash.display.Bitmap;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.navigateToURL;
	import flash.net.Socket;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	import flash.utils.Timer;
	import org.qrcode.QRCode;
	import player.PresentationPlayer;
	import popups.PopupStartRemo;
	import thumbs.MySlide;
	import utils.GUID;
	//import org.devboy.hydra.examples.clickcommand.ClickCommandExample;
	
	public class AirClient extends Sprite
	{
		public var connected:Boolean = false;
		private var curentVolume:Number = 1;
		private var curs:Array = []
		private var mouse:CURR
		private var socket:Socket;
		private var connectTimer:Timer;
		private var dispatchedMOver:Array = [];
		//private var UDP:ClickCommandExample;
		private var IP:String;
		private var remoPop:PopupStartRemo;
		private var hash:String;
		private var _view:Sprite;
		private var _uniqID:String = ""//"Random=" + Math.random();
		
		//public function AirClient(view:Sprite, ip:String = "192.168.1.57")
		public function AirClient(view:Sprite, ip:String = "146.255.33.208")
		{
			_view = view
			mouse = new CURR()
			mouse.x = 2000
			hash = MD5.hash(GUID.create());
			hash = hash.substr(0, 10)
			mouse.visible = false
			
			IP = ip
			socket = new Socket();
			socket.addEventListener(Event.CONNECT, connectHandler);
			socket.addEventListener(Event.CLOSE, closeHandler);
			socket.addEventListener(IOErrorEvent.IO_ERROR, onError)
			setTimeout(connectToRemote, 3000)
			connectTimer = new Timer(1000);
			connectTimer.addEventListener(TimerEvent.TIMER, myConnect);
			connectTimer.start();
			trace("SSS");
			
		
		}
		
		private function connectToRemote():void
		{
			socket.connect(IP, 1234);
		}
		
		public function Pair() {
			trace("SEND Pair ID",  PresentationPlayer.presentationID)
			if (socket.connect)
			{
			  socket.writeObject({"setPlayerID": PresentationPlayer.presentationID + _uniqID, "baseURL": Globals.servicURL})
			  socket.flush();
			}
			
			}
		
		
		private function makeQR(shortUrl)
		{
			var QR:QRCode = new QRCode();
			remoPop.box.linkU.addEventListener(MouseEvent.CLICK, goTOLINK)
			remoPop.box.linkU.text = shortUrl
			QR.encode(shortUrl);
			var qrImg:Bitmap = new Bitmap(QR.bitmapData);
			var colorToReplace:uint = 0xffffffff;
			var newColor:uint = 0xff393C44;
			var maskToUse:uint = 0xffffffff;
			var colorToReplace2:uint = 0xff000000;
			var newColor2:uint = 0xff919694;
			
			var rect:Rectangle = new Rectangle(0, 0, QR.bitmapData.width, QR.bitmapData.height);
			var p:Point = new Point(0, 0);
			QR.bitmapData.threshold(QR.bitmapData, rect, p, "==", colorToReplace, newColor, maskToUse, true);
			QR.bitmapData.threshold(QR.bitmapData, rect, p, "==", colorToReplace2, newColor2, maskToUse, true);
			
			qrImg.smoothing = true
			remoPop.box.addChild(qrImg)
			qrImg.y = 70
			qrImg.x = remoPop.box.youConnected.x
			qrImg.y = remoPop.box.youConnected.y
			qrImg.width = qrImg.height = 240
			_view.stage.addChild(remoPop)
		
		}
		
		private function onShortenIOError(event:Event):void
		{
			trace("Error uRL", event.target.errorText);
		}
		
		private function onShortenSecurityError(event:Event):void
		{
			trace("Error uRL", event.target.errorText);
		}
		
		private function goTOLINK(e:MouseEvent):void
		{
			trace("CLICLED TEXT")
			navigateToURL(new URLRequest(remoPop.box.linkU.text))
		}
		
		private function myConnect(e:TimerEvent):void
		{
			if (!socket.connect)
			{
				socket.connect(IP, 1234);
			}
		}
		
		private function onError(e:IOErrorEvent):void
		{
			trace("Io Errror", e)
		}
		
		private function closeHandler(e:Event):void
		{
			
			trace("CLOSE socket connection")
		}
		
		private function connectHandler(e:Event):void
		{
			
			trace("TRY CONNECT", socket.connected);
			_view.stage.dispatchEvent(new CEvent(CEvent.AIR_SERVER_CONNECTED , "" ));
			
			socket.addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
			
			if (socket.connected)
			{
			  connected = true;
			  
			  socket.writeObject({"setPlayerID": PresentationPlayer.presentationID + _uniqID, "baseURL": Globals.servicURL})
			  socket.flush();
			 
			}
			//socket.writeObject({"setPlayerID": hash, "baseURL": Globals.servicURL})
			//socket.flush();
		}
		
		var finalX:Number = 0
		var finalY:Number = 0
		
		private function socketDataHandler(event:ProgressEvent):void
		{
			trace("SOCKET DATA", socket, socket.connected)
			while (socket && socket.bytesAvailable > 0)
			{
				try
				{
					var message:Object = socket.readObject();
					
					hadleOBject(message)
					
				}
				catch (e:Error)
				{
					trace("ERR" ,e.message)
				}
			}
				trace("Read DATA")
		}
		
		private function hadleUDPOBject(e:Event):void
		{
			if (e.data.hasOwnProperty("x") && e.data.hasOwnProperty("y"))
			{
				_view.stage.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_MOVE))
				mouse.x += e.data.x
				mouse.y += e.data.y
				checkBorder(mouse) //})
				_view.stage.setChildIndex(mouse, _view.stage.numChildren - 1);
				
			}
		}
		
		private function ajustLight(e:CEvent):void
		{
			TweenMax.to(_view, .6, {colorMatrixFilter: {brightness: e.data}})
		}
		
		private function ajustVolume(e:CEvent):void
		{
			trace("volume here", e.data)
			var vol = e.data
			if (e.data < 0)
			{
				vol = 0
			}
			SoundMixer.soundTransform = new SoundTransform(vol);
		}
		
		private function hadleOBject(e:Object):void
		{
			
			if (e.data.hasOwnProperty("connected"))
			{
				
				_view.stage.dispatchEvent(new CEvent(CEvent.REMOTE_CONNECTED , ''));
			
				
				trace("EVENT DISPATCHED");
				
				if (!PresentationPlayer.isPresenting) {
					
					_view.stage.dispatchEvent(new CEvent(CEvent.PLAY_THIS_PRESENTATION , PresentationController.NumSlides))
					}
				
				
			}
			
			if (e.data.hasOwnProperty("conn"))
			{
				
				_view.stage.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN))
			}
			//if (e.data.hasOwnProperty("disconected"))
			//{
				//mouse.x = 20
				//mouse.y = 0
				//return
			//}
			//if (e.data.hasOwnProperty("Light"))
			//{
				//trace("Getting light", e.data.Light), ajustLight(new CEvent(CEvent.REMO_LIGNT, e.data.Light));
			//}
			if (e.data.hasOwnProperty("Volume"))
			{
				ajustVolume(new CEvent(CEvent.REMO_VOLUME, e.data.Volume));
			}
			if (e.data.hasOwnProperty("Play"))
			{
				dispatchEvent(new CEvent(CEvent.REMO_PLAY, e.data.Play));
			}
			
			//if (e.data.hasOwnProperty("Click"))
			//{
				//
				//var objs:Array = _view.stage.getObjectsUnderPoint(new Point(mouse.x, mouse.y))
				//objs.pop()
				//var up:Object = (objs.pop())
				//
				//while (up != null)
				//{
					//
					//if (up.parent)
					//{
						//
						//if (up.parent.hasEventListener(MouseEvent.CLICK))
						//{
							//var localP:Point = up.parent.globalToLocal(new Point(finalX, finalY))
							//up.parent.dispatchEvent(new MouseEvent(MouseEvent.CLICK, true, false, localP.x, localP.y))
						//}
						//
					//}
					//up = up.parent
					//if (up && up.mouseEnabled == false)
					//{
						//up = objs.pop()
					//}
				//}
				//
				////if (mouse.pointer.currentFrame == 2)
				////{
					////_view.stage.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN, true, false, 32, 32))
				////}
				////else if (mouse.pointer.currentFrame == 3)
				////{
					////_view.stage.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN, true, false, 37, 37))
				////}
			//}
			
			//if (e.data.hasOwnProperty("x") && e.data.hasOwnProperty("y") )
			//{
				//_view.stage.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_MOVE))
				//mouse.x += e.data.x
				//mouse.y += e.data.y
				//mouse.visible = true
				//checkBorder(mouse) //})
				//_view.stage.setChildIndex(mouse, _view.stage.numChildren - 1);
				//
			//}
			
			if (e.data.hasOwnProperty("SL"))
			{
				
				//trace("SL >>", remoPop.box.youConnected.visible)
				trace(883423 , e.data.SL)
				
				if (e.data.SL == 0)
				{
					_view.stage.dispatchEvent(new CEvent(CEvent.REMOTE_SHOW_SLIDE, e.data.SR))
					return;
				}
				
				//if (remoPop.box.youConnected.visible)
				//{
					//remoPop.box.youConnected.visible = false
					//remoPop.Close();
					//return;
				//}
				PresentationPlayer.goForward = true
				MySlide(PresentationPlayer.Slides[e.data.SL]).Select("JUSTPLAY")
			}
			if (e.data.hasOwnProperty("SR"))
			{
				trace(3423 , e.data.SR)
				if (e.data.SR == 0)
				{
					_view.stage.dispatchEvent(new CEvent(CEvent.REMOTE_SHOW_SLIDE, e.data.SR))
					return;
				}
				
				PresentationPlayer.goForward = false
				MySlide(PresentationPlayer.Slides[e.data.SR]).Select("JUSTPLAY")
			}
			socket.writeObject({"a": 1, "b": 1})
			socket.flush();
		}
		
		private function checkBorder(e:CURR):void
		{
			if (e.x >= e.stage.stageWidth - 10)
			{
				e.pointer.gotoAndStop(2);
				trace("Reighet right border", e.stage.stageWidth);
				e.x = e.stage.stageWidth
			}
			else if (e.x < 0)
			{
				e.x = 0
				trace("Reighet left border");
				e.pointer.gotoAndStop(3);
			}
			else
			{
				e.pointer.gotoAndStop(1);
				
			}
			if (e.y > e.stage.stageHeight)
			{
				e.y = e.stage.stageHeight
				
				trace("DOWN LIMIT")
			}
			else if (e.y < 0)
			{
				e.y = 0
			}
			
			var mouse = e
			var objs:Array = _view.stage.getObjectsUnderPoint(new Point(mouse.x, mouse.y))
			var up:Object = (objs[objs.length - 2])
			while (up != null)
			{
				
				if (up.parent)
				{
					
					if (up.hasEventListener(MouseEvent.ROLL_OVER))
					{
						
						if (dispatchedMOver.indexOf(up) == -1)
						{
							if (up is SimpleButton)
							{
								up.rotation = 25
							}
							up.dispatchEvent(new MouseEvent(MouseEvent.ROLL_OVER))
							dispatchedMOver.push(up)
						}
						
					}
					
					else if (up.hasEventListener(MouseEvent.MOUSE_OVER))
					{
						
						if (dispatchedMOver.indexOf(up) == -1)
						{
							up.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OVER))
							dispatchedMOver.push(up)
						}
						
					}
				}
				up = up.parent
				
			}
			
			for (var i = 0; i < dispatchedMOver.length; i++)
			{
				if (dispatchedMOver[i].hitTestPoint(mouse.x, mouse.y))
				{
					
				}
				else
				{
					dispatchedMOver[i].dispatchEvent(new MouseEvent(MouseEvent.ROLL_OUT))
					dispatchedMOver[i].dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OUT))
					dispatchedMOver.splice(dispatchedMOver.indexOf(dispatchedMOver[i], 1))
				}
			}
		}
	}
}