﻿package utils {
	import controllers.PresentationController;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.text.TextField;
	import player.PresentationPlayer;
	public class Gesture extends Sprite {
		private var string_dir:String = '';
		public var drawing:Boolean=false;
		public var freemouse:Sprite=new Sprite();
		public var stepmouse:Sprite=new Sprite();
		public var dirmouse:Sprite=new Sprite();
		
		public var px,py,px2,py2:int;
		public var directions:TextField=new TextField();
		public var latest_direction:Number;
		public function Gesture():void {
		
			
			//addChild(stepmouse);
			//addChild(dirmouse);
			//addChild(directions);
			stepmouse.x=250;
			dirmouse.y=200;
			directions.x=250;
			directions.y=200;
			directions.height = 200;
			addEventListener(Event.ADDED_TO_STAGE , init);
			
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.ENTER_FRAME,on_enter_frame);
			stage.addEventListener(MouseEvent.MOUSE_DOWN,on_mouse_down);
			stage.addEventListener(MouseEvent.CLICK,on_mouse_up);
			
		}
		public function on_mouse_down(e:MouseEvent):void {
			if (! drawing) {
				directions.text="";
				latest_direction=-1;
				drawing=true;
				freemouse.graphics.clear();
				freemouse.graphics.lineStyle(1,0x0000ff);
				freemouse.graphics.moveTo(mouseX,mouseY);
				stepmouse.graphics.clear();
				stepmouse.graphics.lineStyle(1,0xff0000);
				stepmouse.graphics.moveTo(mouseX,mouseY);
				dirmouse.graphics.clear();
				dirmouse.graphics.lineStyle(1,0x00ff00);
				dirmouse.graphics.moveTo(mouseX,mouseY);
				px=px2=mouseX;
				py=py2=mouseY;
			}
		}
		public function on_mouse_up(e:MouseEvent):void {
			drawing = false;
			
			if ( PresentationPlayer.isPresenting) {
			stage.dispatchEvent(new Event(string_dir));
			e.stopImmediatePropagation()
			e.stopPropagation()
			}
			
		}
		public function on_enter_frame(e:Event):void {
			if (drawing) {
				freemouse.graphics.lineTo(mouseX,mouseY);
				var dx=px-mouseX;
				var dy=py-mouseY;
				var distance:Number = dx * dx + dy * dy;
				string_dir="nop\n";
				if (distance>400 && PresentationPlayer.isPresenting) {
					stepmouse.graphics.lineTo(mouseX,mouseY);
					var angle:Number=Math.atan2(dy,dx)*57.2957795;
					var refined_angle:Number;
					string_dir;
					if (angle>=22*-1&&angle<23) {
						refined_angle=0;
						string_dir="loadNext";
					}
					 if (angle>=23&&angle<68) {
						refined_angle=Math.PI/4;
						string_dir="Up Left";
					}
					 if (angle>=68&&angle<113) {
						refined_angle=Math.PI/2;
						string_dir="Up";
					}
					 if (angle>=113&&angle<158) {
						refined_angle=Math.PI/4*3;
						string_dir="Up Right";
					}
					 if (angle>=135||angle<157*-1) {
						refined_angle=Math.PI;
						string_dir="loadPrev";
					}
					 if (angle>=157*-1&&angle<112*-1) {
						refined_angle=- Math.PI/4*3;
						string_dir="Down Right";
					}
					 if (angle>=112*-1&&angle<67*-1) {
						refined_angle=- Math.PI/2;
						string_dir="Down";
					}
					 if (angle>=67*-1&&angle<22*-1) {
						refined_angle=- Math.PI/4;
						string_dir="Down Left";
					}
					
					px2-=Math.sqrt(distance)*Math.cos(refined_angle);
					py2-=Math.sqrt(distance)*Math.sin(refined_angle);
					if (refined_angle!=latest_direction) {
						directions.appendText(string_dir);
						latest_direction=refined_angle;
					}
					dirmouse.graphics.lineTo(px2,py2);
					px=mouseX;
					py=mouseY;
				}
			}
		}
	}
}