package utils 
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.FileReferenceList;
	import flash.system.ImageDecodingPolicy;
	import flash.system.LoaderContext;
	/**
	 * ...
	 * @author radu
	 */
	public class SlideCreator extends Sprite
	{
		private var fr:FileReferenceList = new FileReferenceList ()
		private var _fileFilter:FileFilter;
		private var uploadedImages:Number;
		private var localloadedImages:Number;
		private var localBitmaps:Array;
		
		public function SlideCreator() 
		{
			_fileFilter = new FileFilter("Image", "*.jpeg;*.jpg;*.gif;*.png;*.swf;"  );
		}
		
		public function browseForSlides():void 
		{
			fr.browse([_fileFilter]);	
			fr.addEventListener(Event.SELECT, _onImageSelect);
		}
		
	
		
		private function _onImageSelect(e:Event):void
		{
			
			
			uploadedImages = 0
			localloadedImages = 0
			localBitmaps = []
			
			_fileRef.fileList[0].addEventListener(Event.COMPLETE, _onDataLoaded);
			_fileRef.fileList[0].load()
		
		}
		
		private function _onDataLoaded(e:Event):void
		{
			e.target.removeEventListener(Event.COMPLETE, _onDataLoaded);
			var tempFileRef:FileReference = FileReference(e.target);
			var _loader = new Loader()
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, _onImageLoaded);
			
			var loaderContext:LoaderContext = new LoaderContext();
			loaderContext.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD
			
			_loader.loadBytes(tempFileRef.data);
		
		}
		
	}

}