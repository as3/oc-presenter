﻿package utils 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;

	//import ab.fl.utils.json.JSON
	public class DebugWindow
	{	
		public static var _view:Sprite;
		private static var _output:TextField;
		private static var _width:Number = 300;
		private static var _height:Number = 500;
		private static var _clearBtn:Sprite;
		private static var _timer:Timer = new Timer(100,0)
		//public static private var mytree:Tree = new Tree();
		//;
		//
		private static function getView():Sprite
		{
			if (_view == null)
			{
				createView();
			}
			_view.addEventListener(MouseEvent.MOUSE_DOWN, sd)
			_timer.addEventListener(TimerEvent.TIMER, goOnTop)
			_timer.start()
			return _view;
		}
		
		static private function goOnTop(e:TimerEvent):void 
		{
			if(_view.parent) {
			_view.parent.setChildIndex(_view, _view.parent.numChildren -1 ) }
		}
		
		static private function sd(e:MouseEvent):void 
		{
			_view.startDrag()
			_view.stage.addEventListener(MouseEvent.MOUSE_UP, std)
		}
		
		static private function std(e:MouseEvent):void 
		{
			_view.stopDrag()
			_view.stage.removeEventListener(MouseEvent.MOUSE_UP, std)
		}
		
		private static function createView():void
		{
			_view = new Sprite();
			_view.x = 10;
			_view.y = 10;
			addOutputBox();
			addClearBtn();
			_view.addChild(_output);
		}
		
		private static function addOutputBox():void
		{
			_output = new TextField();
			_output.y = 10;
			_output.multiline = true;
			_output.background = true;
			_output.backgroundColor = 0xffffff;
			_output.wordWrap = true
			_output.border = true;
			_output.width = _width;
			_output.height = _height;
		}
		private static function addClearBtn():void
		{
			_clearBtn = new Sprite();
			_clearBtn.graphics.beginFill(0x990000);
			_clearBtn.graphics.drawRect(0, 0, 25, 10);
			_clearBtn.graphics.endFill();
			_clearBtn.buttonMode = true;
			_clearBtn.addEventListener(MouseEvent.CLICK, onClearBtnClick);
			_clearBtn.x = _width - _clearBtn.width;
			_view.addChild(_clearBtn);
		}
		
		static private function onClearBtnClick(e:MouseEvent):void 
		{
			_output.text = "";
		}
		public static function get View():Sprite
		{
			return getView();
		}
		public static function Trace(...args):void
		{
			if (_output != null)
			{
				_output.appendText(args.toString() +"\n");
			}
			
			
			if ( args [0] == "json") {
				
				DebugWindow.Trace((args [1]))
			
				}
		}
		public static function Clrscr():void
		{
			_output.text = "";
		}
		
		public static function replaceBackslashes(s:String):String
		{
			//return s.replace(/\\/g, "\\");
			//return s.replace(/&#92;/g, "\\");
			return s
		}
	}
}