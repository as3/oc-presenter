package utils 
{
	/**
	 * uk.soulwire.utils.display.DisplayUtils
	 * 
	 * @version v1.0
	 * @since May 26, 2009
	 * 
	 * @author Justin Windle
	 * @see http://blog.soulwire.co.uk
	 * 
	 * About DisplayUtils
	 * 
	 * DisplayUtils is a set of static utility methods for dealing 
	 * with DisplayObjects
	 * 
	 * Licensed under a Creative Commons Attribution 3.0 License
	 * http://creativecommons.org/licenses/by/3.0/
	 */
	 
	import flash.display.PixelSnapping;	
	import flash.display.Bitmap;	
	import flash.display.BitmapData;	 
	import flash.events.ContextMenuEvent;
	import flash.geom.Matrix;	
	import flash.geom.Rectangle;	
	import flash.display.DisplayObject;	
	import flash.net.URLRequest;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuBuiltInItems;
	import flash.ui.ContextMenuItem;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	import uk.soulwire.utils.display.Alignment;

	public class DisplayUtils 
	{	

		//---------------------------------------------------------------------------
		//------------------------------------------------------------ PUBLIC METHODS
		
		/**
		 * Fits a DisplayObject into a rectangular area with several options for scale 
		 * and alignment. This method will return the Matrix required to duplicate the 
		 * transformation and can optionally apply this matrix to the DisplayObject.
		 * 
		 * @param displayObject
		 * 
		 * The DisplayObject that needs to be fitted into the Rectangle.
		 * 
		 * @param rectangle
		 * 
		 * A Rectangle object representing the space which the DisplayObject should fit into.
		 * 
		 * @param fillRect
		 * 
		 * Whether the DisplayObject should fill the entire Rectangle or just fit within it. 
		 * If true, the DisplayObject will be cropped if its aspect ratio differs to that of 
		 * the target Rectangle.
		 * 
		 * @param align
		 * 
		 * The alignment of the DisplayObject within the target Rectangle. Use a constant from 
		 * the DisplayUtils class.
		 * 
		 * @param applyTransform
		 * 
		 * Whether to apply the generated transformation matrix to the DisplayObject. By setting this 
		 * to false you can leave the DisplayObject as it is but store the returned Matrix for to use 
		 * either with a DisplayObject's transform property or with, for example, BitmapData.draw()
		 */

		 
		  public static function indexOf(source:Array, filter:Function, startPos:int = 0):int {
               var len:int = source.length;
               for (var i:int = startPos; i < len; i++) 
                    if (filter(source[i],i,source)) return i;
               return -1;
          }
		  
		 private static const ALPHA_CHAR_CODES:Array = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70];
    
    
        /**
         *  Generates a UID (unique identifier) based on ActionScript's
         *  pseudo-random number generator and the current time.
         *
         *  <p>The UID has the form
         *  <code>"XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"</code>
         *  where X is a hexadecimal digit (0-9, A-F).</p>
         *
         *  <p>This UID will not be truly globally unique; but it is the best
         *  we can do without player support for UID generation.</p>
         *
         *  @return The newly-generated UID.
         *  
         *  @langversion 3.0
         *  @playerversion Flash 9
         *  @playerversion AIR 1.1
         *  @productversion Flex 3
         */
        public static function createUID():String
        {
            var uid:Array = new Array(36);
            var index:int = 0;
            
            var i:int;
            var j:int;
            
            for (i = 0; i < 8; i++)
            {
                uid[index++] = ALPHA_CHAR_CODES[Math.floor(Math.random() *  16)];
            }
    
            for (i = 0; i < 3; i++)
            {
                uid[index++] = 45; // charCode for "-"
                
                for (j = 0; j < 4; j++)
                {
                    uid[index++] = ALPHA_CHAR_CODES[Math.floor(Math.random() *  16)];
                }
            }
            
            uid[index++] = 45; // charCode for "-"
    
            var time:Number = new Date().getTime();
            // Note: time is the number of milliseconds since 1970,
            // which is currently more than one trillion.
            // We use the low 8 hex digits of this number in the UID.
            // Just in case the system clock has been reset to
            // Jan 1-4, 1970 (in which case this number could have only
            // 1-7 hex digits), we pad on the left with 7 zeros
            // before taking the low digits.
            var timeString:String = ("0000000" + time.toString(16).toUpperCase()).substr(-8);
            
            for (i = 0; i < 8; i++)
            {
                uid[index++] = timeString.charCodeAt(i);
            }
            
            for (i = 0; i < 4; i++)
            {
                uid[index++] = ALPHA_CHAR_CODES[Math.floor(Math.random() *  16)];
            }
            
            return String.fromCharCode.apply(null, uid);
        }
		
		public static function fitIntoRect(displayObject : DisplayObject, rectangle : Rectangle, fillRect : Boolean = true, align : String = "C", applyTransform : Boolean = true) : Matrix
		{
			var matrix : Matrix = new Matrix();
			
			var wD : Number = displayObject.width / displayObject.scaleX;
			var hD : Number = displayObject.height / displayObject.scaleY;
			
			var wR : Number = rectangle.width;
			var hR : Number = rectangle.height;
			
			var sX : Number = wR / wD;
			var sY : Number = hR / hD;
			
			var rD : Number = wD / hD;
			var rR : Number = wR / hR;
			
			var sH : Number = fillRect ? sY : sX;
			var sV : Number = fillRect ? sX : sY;
			
			var s : Number = rD >= rR ? sH : sV;
			var w : Number = wD * s;
			var h : Number = hD * s;
			
			var tX : Number = 0.0;
			var tY : Number = 0.0;
			
			switch(align)
			{
				case Alignment.LEFT :
				case Alignment.TOP_LEFT :
				case Alignment.BOTTOM_LEFT :
					tX = 0.0;
					break;
					
				case Alignment.RIGHT :
				case Alignment.TOP_RIGHT :
				case Alignment.BOTTOM_RIGHT :
					tX = w - wR;
					break;
					
				default : 					
					tX = 0.5 * (w - wR);
			}
			
			switch(align)
			{
				case Alignment.TOP :
				case Alignment.TOP_LEFT :
				case Alignment.TOP_RIGHT :
					tY = 0.0;
					break;
					
				case Alignment.BOTTOM :
				case Alignment.BOTTOM_LEFT :
				case Alignment.BOTTOM_RIGHT :
					tY = h - hR;
					break;
					
				default : 					
					tY = 0.5 * (h - hR);
			}
			
			matrix.scale(s, s);
			matrix.translate(rectangle.left - tX, rectangle.top - tY);
			
			if(applyTransform)
			{
				displayObject.transform.matrix = matrix;
			}
			
			return matrix;
		}

		public static function isValidEmail(email:String):Boolean {
			
			var emailExpression:RegExp = /^[a-z][\w.-]*@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
			return emailExpression.test(email);
		}
		public static function createThumb(image : BitmapData, width : int, height : int, align : String = "C", smooth : Boolean = true) : Bitmap
		{
			var source : Bitmap = new Bitmap(image);
			var thumbnail : BitmapData = new BitmapData(width, height, false, 0x0);
			
			thumbnail.draw(image, fitIntoRect(source, thumbnail.rect, true, align, false), null, null, null, smooth);
			source = null;
			
			return new Bitmap(thumbnail, PixelSnapping.AUTO, smooth);
		}
		
		public static function get R():String
		{
			return '&rand=' + Math.random()
		}
		
		public static function getCompilationDate(e):Date{
		  if(!e.stage) throw new Error("No stage");

		  var swf:ByteArray = e.stage.loaderInfo.bytes;
		  swf.endian = Endian.LITTLE_ENDIAN;
		  // Signature + Version + FileLength + FrameSize + FrameRate + FrameCount
		  swf.position = 3 + 1 + 4 + (Math.ceil(((swf[8] >> 3) * 4 - 3) / 8) + 1) + 2 + 2;
		  while(swf.position != swf.length){
			var tagHeader:uint = swf.readUnsignedShort();
			if(tagHeader >> 6 == 41){
			  // ProductID + Edition + MajorVersion + MinorVersion + BuildLow + BuildHigh
			  swf.position += 4 + 4 + 1 + 1 + 4 + 4;
			  var milli:Number = swf.readUnsignedInt();
			  var date:Date = new Date();
			  date.setTime(milli + swf.readUnsignedInt() * 4294967296);
			  return date; // Sun Oct 31 02:56:28 GMT+0100 2010
			}else
			  swf.position += (tagHeader & 63) != 63 ? (tagHeader & 63) : swf.readUnsignedInt() + 4;
		  }
		  throw new Error("No ProductInfo tag exists");
		}
		public static function setContextMenu(e):void
		{
			if (ContextMenu.isSupported)
			{
				var menu:ContextMenu = new ContextMenu();
				
				var defaultItems:ContextMenuBuiltInItems = menu.builtInItems;
				defaultItems.print = false;
				var item:ContextMenuItem = new ContextMenuItem("OC Presenter V1.99");
				menu.customItems.push(item);
				var item5:ContextMenuItem = new ContextMenuItem("Produced by:");
				var item6:ContextMenuItem = new ContextMenuItem("One Communication");
				var item88:ContextMenuItem = new ContextMenuItem(DisplayUtils.getCompilationDate(e) );
				menu.customItems.push(item88);
				item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, function()
					{
						navigateToURL(new URLRequest("http://www.onecom.no"), "_blank");
					});
				item5.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, function()
					{
						navigateToURL(new URLRequest("http://www.onecom.no"), "_blank");
					});
				item6.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, function()
					{
						navigateToURL(new URLRequest("http://www.onecom.no"), "_blank");
					});
				menu.customItems.push(item5);
				menu.customItems.push(item6);
				e.contextMenu = menu;
			}
		}
	}
}
