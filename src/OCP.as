﻿package
{
	
	import com.hurlant.util.Base64;
	import com.jac.mouse.MouseWheelEnabler;
	import data.Data;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	import player.EditScreen;
	import player.PresentationPlayer;
	import popups.PopupStartRemo;
	import popups.UploadPop;
	import remote.AirClient;
	import thumbs.MYPresentationList;

	public class OCP extends Sprite
	{
		private var air:AirClient;
		public var editScreen:EditScreen
		public var PPlayer:PresentationPlayer
		public static var LG:Object 
		public static var uploadPop:UploadPop;
		public static var _pleaseFlexLoadMyClass:CEvent;
		public function OCP():void
		{
			addEventListener(Event.ADDED_TO_STAGE, init)	
			
		}

		private function init(e:Event = null):void
		{
			
			removeEventListener(Event.ADDED_TO_STAGE, init);	
			MouseWheelEnabler.init(stage);
			MouseWheelEnabler.useRawValues = true;
			MouseWheelEnabler.useRawValues = true;
			
			if (LoaderInfo(stage.loaderInfo).parameters.baseURL)
			{
				Globals.baseURL = LoaderInfo(stage.loaderInfo).parameters.baseURL
			}
			if (LoaderInfo(stage.loaderInfo).parameters.serviceURL)
			{
				Globals.servicURL = LoaderInfo(stage.loaderInfo).parameters.serviceURL
			}
            
			new Data(stage);	
			stage.addEventListener(CEvent.LANGUAGE_HAS_ARIVED, init2)
	
		}
		
		private function init2(e:CEvent):void 
		{
			
			stage.removeEventListener(CEvent.LANGUAGE_HAS_ARIVED, init2)
			editScreen = new EditScreen();
			addChild(editScreen);
			stage.addEventListener(CEvent.SHOW_REMO_POP, showRemoPOP)
			stage.addEventListener(CEvent.SHOW_EDIT_VIEW, showEditView);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, showMouse, true)
			stage.addEventListener(CEvent.PLAY_THIS_PRESENTATION , playPresentation)
			
			PPlayer = new PresentationPlayer();
			PPlayer.visible  = false
			addChild(PPlayer);
			air = new AirClient(PPlayer);
			loadSharedPresentation()
			
		}
		
		private function loadSharedPresentation():void
		{
			var id = stage.loaderInfo.parameters.id
				//id = 'Njc0'
		       //id = 'Njcw'
			if (id)
			{
				//var sharePop:SharedStartPop = new SharedStartPop()
				//stage.addChild(sharePop)
				//
				
				
				if (int(Base64.decode(id)) != 0)
				{
					id = Base64.decode(id)
				}
				
				var remoPop:PopupStartRemo = new PopupStartRemo(air, id)
				stage.addChild(remoPop);
				//air.showRemoPop(null, id);
				
				
				Data.getInstance().loadAndPlayTimelineID(id)
				editScreen.visible = false
				PresentationPlayer.presentationID = id
			}

		}
		
		private function playPresentation(e:CEvent):void 
		{	
			PPlayer.PlayPresentation(e.data[0] , e.data[1], e.data[2])
			editScreen.visible = false
		}
		
		private function showMouse(e:MouseEvent):void
		{
			Mouse.show();
		}
		
		private function showRemoPOP(e:CEvent):void 
		{
			//air.showRemoPop(e);
			
				trace("Aici se afisaza Remote" , MYPresentationList.HiglightedThumb)
			var remoPop:PopupStartRemo = new PopupStartRemo(air, MYPresentationList.HiglightedThumb.id )
				PresentationPlayer.presentationID = MYPresentationList.HiglightedThumb.id
				stage.addChild(remoPop);
			
		}
		
		private function showEditView(e:CEvent):void 
		{
			PPlayer.visible = false	
			editScreen.Show(e)
		}
	
		public static function get R():String
		{
			return '&rand=' + Math.random()
		}
	}
}