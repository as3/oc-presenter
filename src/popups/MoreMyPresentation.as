package popups 
{
	import com.hurlant.util.Base64;
	import controllers.PresentationController;
	import data.Data;
	import data.Login;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import thumbs.MYPresentationList;
	/**
	 * ...
	 * @author Radu
	 */
	public class MoreMyPresentation extends menuPreentation
	{
		private var buttons: Array = [];
		
		public function MoreMyPresentation() 
		{
			addEventListener(Event.ADDED_TO_STAGE, init)
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			bc.playBtn.addEventListener(MouseEvent.CLICK, startPresenting)
			bc.remoBtn.addEventListener(MouseEvent.CLICK, dispatchRemo)
			bc.autoplayPresentationBtn.addEventListener(MouseEvent.CLICK, showAutoPlayPOP)
			bc.sharePresentationBtn.addEventListener(MouseEvent.CLICK, sharePresentation)
			bc.pdfBtn.addEventListener(MouseEvent.CLICK, showPDFPopup)
			bc.saveACopyBtn.addEventListener(MouseEvent.CLICK, showDuplicatForm)
			bc.saveBtn.addEventListener(MouseEvent.CLICK, savePresentation)
			bc.clearBtn.addEventListener(MouseEvent.CLICK, deleteMiniThumbs)
			bc.infoBtn.addEventListener(MouseEvent.CLICK, editInfo)
			bc.delBtn.addEventListener(MouseEvent.CLICK, deleteCurrentPresentation)
			
			
			buttons = [[bc.playBtn, 1], [bc.remoBtn, 1], [bc.autoplayPresentationBtn, 1], [bc.sharePresentationBtn, 1],
										[bc.pdfBtn, 1], [bc.saveACopyBtn, 2], [bc.saveBtn, 2], [bc.clearBtn, 1], [bc.infoBtn, 1 ], [bc.delBtn, 2]]; 
		
			//bc.x = -182.25;
			bc.y = -23.65 
			Hide();
		}
		
		public function Show() {
			this.visible = true;
			bc.gotoAndStop(1)
		  if ( Login.isLogedIn == 1) {
			  bc.gotoAndStop(2)
			  
				}
			bkg.height = bc.height +21 
			}
		public function Hide() {
			this.visible = false;
			
		}
		
		
		private function editInfo(e:MouseEvent):void {
			stage.dispatchEvent(new CEvent(CEvent.SHOW_PRESENTATION_INFO, MYPresentationList.HiglightedThumb.OBJ))
			visible = false
			}
		
			private function deleteCurrentPresentation(e:MouseEvent):void
		{
			trace("DEL PRESENTATION")
			delPresPop = new DeletePresentationPop(OCP.LG.areyousureDeletePresentation, OCP.LG.DeleteT)
			stage.addChild(delPresPop)	
			visible = false
			
		}
		
		
		private function startPresenting(e:MouseEvent):void
		{
			if (PresentationController.NumSlides.length > 0) {
				PresentationController.NumSlides[0].Select("JUSTPLAY")
				}
				visible = false
		}
		
		private function dispatchRemo(e:MouseEvent):void 
		{
			trace(PresentationController.NumSlides[0], 333);
			if (PresentationController.NumSlides.length > 0) {
				PresentationController.NumSlides[0].Select("JUSTPLAY")
				stage.dispatchEvent(new CEvent(CEvent.SHOW_REMO_POP, null));
				}
				visible = false
		}
		
		private function showAutoPlayPOP(e:MouseEvent):void 
		{
			
			PresentationController.NumSlides[0].Select("JUSTPLAY")	
			var autoPlay:PopUpTimer = new PopUpTimer()
			stage.addChild(autoPlay);
			visible = false
		}
		
		private function sharePresentation(e:MouseEvent):void
		{
			var h:String = ''
			if(!PresentationController.NumSlides) return
			for (var i:int = 0; i < PresentationController.NumSlides.length; i++)
			{
				h += PresentationController.NumSlides[i].id + '|'
			}
			h = h.substring(0, h.length - 1)
			var sharedPop = new SharedPop(Globals.baseURL + "site/index.php?id=" + Base64.encode(MYPresentationList.HiglightedThumb.id))
			
			stage.addChild(sharedPop)
			OCP.prompt = sharedPop
			visible = false
		}
		
		private function showPDFPopup(e:MouseEvent):void 
		{
			var h:String = ''
			if(!PresentationController.NumSlides) return
			for (var i:int = 0; i < PresentationController.NumSlides.length; i++)
			{
				h += PresentationController.NumSlides[i].id + '|'
			}
			h = h.substring(0, h.length - 1)
			var pdfPop = new PDFPop(MYPresentationList.HiglightedThumb.numSlides.text, Login.Name)
			
			stage.addChild(pdfPop)
			visible = false
		}
		
		private function showDuplicatForm(e:MouseEvent):void 
		{
			
			var o:Object = new Object()
			o.title =  OCP.LG.copyOF + MYPresentationList.HiglightedThumb.OBJ.title 
			o.description =  MYPresentationList.HiglightedThumb.OBJ.description 
			o.slide_ids =  MYPresentationList.HiglightedThumb.OBJ.slide_ids 
			o.access_level =  MYPresentationList.HiglightedThumb.OBJ.access_level 
			o.thiles =  PresentationController.NumSlides
			o.instance_id = "-1"
			stage.dispatchEvent(new CEvent(CEvent.SHOW_DUPLICATE_FORM, o))
			visible = false
		}
		
		private function deleteMiniThumbs(e:MouseEvent):void
		{
			var delPop = new DeletePop(OCP.LG.wantToClearSlidesT, OCP.LG.clearT )
			stage.addChild(delPop)
			visible = false
		}
		
		private function savePresentation(e:MouseEvent):void 
		{
			var o:Object = MYPresentationList.HiglightedThumb.OBJ
				o.thiles = formTimeline(PresentationController.NumSlides);
				o.title = unescape(decodeURI(MYPresentationList.HiglightedThumb.OBJ.title))
				o.description =  unescape(decodeURI(MYPresentationList.HiglightedThumb.OBJ.description))
				
				Data.getInstance().SavePresentation(MYPresentationList.HiglightedThumb.OBJ.instance_id, o.title,
													o.description, o.thiles, MYPresentationList.HiglightedThumb.OBJ.access_level )
				//stage.dispatchEvent(new CEvent(CEvent.SAVE_PRESENTATION, 
				//[MYPresentationList.HiglightedThumb.OBJ.instance_id , o.title, o.description, null,formTimeline(_view.Thiles)]))
				visible = false
		}
		
		private  function formTimeline(e):String {
			var csv:Array = []
			for (var i:int = 0; i < e.length; i++) 
			{
				csv.push( (e[i].isVideo?'V':'' )+ e[i].id )
			}
			
			return  '[' + csv + ']'
		}
	}

}