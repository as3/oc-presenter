package popups
{
	import com.greensock.TweenMax;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.BinaryDataLoader;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.LoaderMax;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.text.TextField;
	import managers.ImageCacheManager;

	public class SyncPop extends SyncPopLib
	
	{
		
		
		private var urlLoader:URLLoader = new URLLoader();
		private var allGroupsIDs:Array;
		private var COBJ:Object = new Object();
		private var loadrCache:LoaderMax;
		
		public function SyncPop()
		{
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			this.alpha = 0
			TweenMax.to(this, .3, {alpha: 1})
		}
		
		public function Show():void 
			{
				this.visible = true
				box.t.text = ''
				cahceNewGroups()
			}

		
		public function closePop(e:MouseEvent):void
		{
			this.parent.removeChild(this)
		}
	
		public function Resize()
		{
			
			bkg.width = OCP._DIM.width
			bkg.height = OCP._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		
		}
		
		public function cahceNewGroups(e:Event = null) {
			
				COBJ = new Object();
				box.t2.text = "Checking services... This will take a few minutes depending on the speed of your Internet connection."
				box.t.text = ""
	
			if (loadrCache) {
				trace ("allreadySyncing")
				}
			var request:URLRequest = new URLRequest(OCP.servicURL + "?what=get_slide_groups&lang=" + OCP.LANG + OCP.useCache)
			urlLoader.addEventListener(Event.COMPLETE , parceGroups)
			urlLoader.load(request)
			
			}
		private function parceGroups(e:Event):void
		{
			loadrCache = new LoaderMax( { name:"mainQueue", onProgress:progressHandler, onComplete:completeHandler, onError:errorHandler } );
			urlLoader.removeEventListener(Event.COMPLETE , parceGroups)
			COBJ.langGruoupsURL =  String(urlLoader.data)
			
			var jsonOBJ:Object = OCP.decodeMethod(urlLoader.data)
			allGroupsIDs = []
			for (var i:int = 0; i < jsonOBJ.groups.length; i++) 
			{
				allGroupsIDs.push(jsonOBJ.groups[i].id)
			}
			recursive(OCP.decodeMethod(urlLoader.data))

			var r:String = OCP.servicURL + '?what=get_slides_by_groups'
			var request:URLRequest = new URLRequest(r)
				request.method = URLRequestMethod.POST
				var variables:URLVariables = new URLVariables()
				variables['group_id[]'] = allGroupsIDs
				request.data = variables				
				urlLoader.addEventListener(Event.COMPLETE , loadAllSlides)
				urlLoader.load(request)	
				box.round.gotoAndPlay(2)

		}
		
		
		public function loadAllSlides(e:Event) {
			
			urlLoader.removeEventListener(Event.COMPLETE , loadAllSlides)
	
		var addObj:Object = OCP.decodeMethod(urlLoader.data)
			COBJ.slideRequests =  new Object();
			COBJ.allGroups = urlLoader.data
			for (var i:int = 0; i < addObj.groups.length ; i++) 
			{
			
			
				var grup = addObj.groups[i]
				var groupPages =  addObj.groups[i].pages
				var groupID =  addObj.groups[i].id
				
				
				for (var j:int = 0; j < groupPages.length; j++) 
				{
					var r:String = OCP.servicURL + '?what=get_slides&slide_id[]=' + groupPages[j].id 
					loadrCache.append(new DataLoader(r, { name: "slideCache" + groupPages[j].id, onComplete: LoadElementComplete } ));
				}

			}
			loadrCache.load()
			
		}
		
		private function LoadElementComplete(e:Event) {
				
			var r:DataLoader = DataLoader( e.target)
				r.autoDispose = true

			COBJ.slideRequests[r.name] = r.content
			
			var slideJSON:Object = OCP.decodeMethod(r.content)
			for (var i:int = 0; i < slideJSON.pages.length; i++)
			{
				var slideJ:Object = slideJSON.pages[i]
				 recursive(slideJ.content)
			}
		
		}	
		function progressHandler(event:LoaderEvent):void {
			box.t.text = Math.round(event.target.progress * 100) + '%'
		}
		
		function errorHandler(event:LoaderEvent):void {
			trace("error occured with " + event.target + ": " + event.text);
		}
		
		function completeHandler(event:LoaderEvent):void {
			trace("Syncronization finished")
			box.t.text = ''
			ImageCacheManager.clearOldFiles()
			
			trace("Saving new lang cookie for" , OCP.LANG)
			OCP.cookie.data[OCP.LANG] = COBJ
			OCP.cookie.flush()
			loadrCache = null
			box.round.gotoAndStop(2)
			
		
			
		}
		  
		
		public  function recursive (oObj:Object, sPrefix:String = ""):void
			{
			    sPrefix == "" ? sPrefix = "---" : sPrefix += "---";
			   
			    for (var i:* in oObj)
			    {    
			      	if ( typeof( oObj[i] ) == 'string' ) {
							var ss:String = String(oObj[i])
							trace("!",ss)
								ss = ss.substring( ss.length - 4)
							var base:String
							
							if (ss  == ".mp4" || ss  == ".flv" || ss  == ".f4v"  || ss  == ".mov" || ss  == ".m4v"){
						        base = OCP.servicURL + 'resources/files/videos/' + String(oObj[i])
								trace("found movie=====================================", base, String(oObj[i]))
								
								if (! ImageCacheManager.getInstance().checkIfFileIsCached( String(oObj[i]))) {
									
									loadrCache.append(new BinaryDataLoader(base,{ name:String(oObj[i]),  onComplete: ImgCache}))
									}
							
								}
							else if ( ss  == ".png" || ss  == ".jpg" ) {
								base =  OCP.servicURL + 'resources/files/images/'  + String(oObj[i])
							
								if (! ImageCacheManager.getInstance().checkIfFileIsCached( String(oObj[i]))) {
									
									trace("found image")
									loadrCache.append(new BinaryDataLoader(base,{ name:String(oObj[i]), onComplete: ImgCache}))
									}
								}				
						} 
			        else if  (typeof( oObj[i] ) == "object") {recursive ( oObj[i], sPrefix);  }// around we go again     
					  
			    }
			}
		
			function ImgCache(e:Event) {
			var i:ImageCacheManager = ImageCacheManager.getInstance()
			var bd:BinaryDataLoader = BinaryDataLoader(e.target)
			i.addImage(bd.name, bd.content)
			bd.dispose(true)
			}	
			
		
	
			
	
	}

}