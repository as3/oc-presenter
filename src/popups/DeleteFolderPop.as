package popups
{
	import com.greensock.TweenMax;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;

	public class DeleteFolderPop extends deletePresentationPOP
	{
		private var _id:int
		private var _force:Boolean = false
		public function DeleteFolderPop(id:int , prompText:String = ''  , force:Boolean = false )
		{
			box.okBtn.addEventListener(MouseEvent.CLICK, OKPressed)
			box.okBtn.t.text = OCP.LG.DeleteT;
			_id = id;
			_force = force
			box.cancelBtn.addEventListener(MouseEvent.CLICK, closePop)
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			Resize()
			this.alpha = 0
			TweenMax.to(this, .3, { alpha: 1 } )
			
			box.okBtn.t.text = OCP.LG.DeleteT
			if (prompText.length > 2) {
				box.t.text = prompText ;
				}
			addEventListener(Event.ADDED_TO_STAGE , init)
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, detectEnter)
		}
		private function detectEnter(e:KeyboardEvent):void 
		{
			if (e.keyCode == 13) {
				OKPressed(null)
				}
		}
		public function closePop(e:MouseEvent):void
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, detectEnter)
			box.okBtn.removeEventListener(MouseEvent.CLICK, OKPressed)
			box.closeBtn.removeEventListener(MouseEvent.CLICK, closePop)
			box.cancelBtn.removeEventListener(MouseEvent.CLICK, closePop)
			this.parent.removeChild(this)	
		}
		
		private function OKPressed(e:MouseEvent):void
		{
			stage.dispatchEvent(new CEvent(CEvent.DELETE_CURRENT_FOLDER, [_id, _force]))
			closePop(e)
		}
		
		public function Resize()
		{
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		}
	
	}

}