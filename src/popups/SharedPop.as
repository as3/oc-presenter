package popups
{
	import com.adobe.images.JPGEncoder;
	import com.adrianparr.utils.GoogleUrlShortener;
	import com.greensock.loading.BinaryDataLoader;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import com.greensock.TweenMax;
	import controllers.PresentationController;
	import data.Data;
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.PNGEncoderOptions;
	import flash.display.JPEGEncoderOptions;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import org.alivepdf.colors.RGBColor;
	import org.alivepdf.drawing.Caps;
	import org.alivepdf.drawing.Joint;
	import org.alivepdf.drawing.WindingRule;
	import org.alivepdf.images.ColorSpace;
	import org.alivepdf.layout.Mode;
	import org.alivepdf.layout.Orientation;
	import thumbs.MYPresentationList;
	import utils.DisplayUtils;
	import org.alivepdf.layout.Unit;
	import org.alivepdf.pages.Page;
	import org.alivepdf.pdf.PDF;
	import org.alivepdf.saving.Download;
	import org.alivepdf.saving.Method;
	
	
	
	public class SharedPop extends sharedLinkLIB
	{
		
		private var _bitmapDatas:Array  = []
		private var _pngJpg:Array  = []
		private var _url:String = ''
		private var _borderColor:Number = 0x929496;
		private var _rectColor:Number =  0xF0F0F0;
		public function SharedPop(url:String)
		{

			box.copyBtn.addEventListener(MouseEvent.CLICK, copyLinkToClipBoard)
			box.openBtn.addEventListener(MouseEvent.CLICK, gotoLink)
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			Resize()
			
			_url = url;
			this.alpha = 0
			TweenMax.to(this, .3, { alpha: 1 } )
			addEventListener(Event.ADDED_TO_STAGE , init)
			box.linkT.text = url;
			
		}
		
		
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			TextField(box.t).setSelection(0, box.t.text.length)
			TextField(box.t).scrollH = 0;
			stage.focus = box.t
			stage.addEventListener(Event.RESIZE , Resize)
		}
		
		private function gotoLink(e:MouseEvent):void 
		{
			navigateToURL(new URLRequest(_url));
		}
		
		private function copyLinkToClipBoard(e:MouseEvent):void 
		{
			Clipboard.generalClipboard.clear();
            Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, _url);
		}
		
		public function closePop(e:MouseEvent):void
		{	
			box.openBtn.removeEventListener(MouseEvent.CLICK, gotoLink)
			box.closeBtn.removeEventListener(MouseEvent.CLICK, closePop)
			box.copyBtn.removeEventListener(MouseEvent.CLICK, copyLinkToClipBoard)
			this.parent.removeChild(this)
		}

		public function Resize(e=null)
		{
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		}
		
	
	
	}

}