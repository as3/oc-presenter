package  popups
{
	import com.adobe.crypto.MD5;
	import com.adobe.webapis.URLLoaderBase;
	import data.Login;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.net.navigateToURL;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.text.TextField;
	import utils.DisplayUtils;

	public class CreateUserPop extends createNewUserLIB
	{
		
		
		var loader:URLLoader = new URLLoader();
		public static var isLogedIn:int = 1;
		
		public function CreateUserPop() 
		{
			visible = false
			
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			box.user.tabIndex = 1;
			box.password.tabIndex = 2; 
			box.rePassword.tabIndex = 3; 
			box.fullName.tabIndex = 4; 
			box.phone.tabIndex = 5; 
			box.createBtn.tabIndex = 6; 
			box.userCreated.visible = false
			box.password.displayAsPassword = true
			box.rePassword.displayAsPassword = true
			box.userCreated.doneBtn.addEventListener(MouseEvent.CLICK, showLogin)
			box.user.addEventListener(FocusEvent.FOCUS_IN , focIN)
			box.user.addEventListener(FocusEvent.FOCUS_OUT , chechUserName)
			
			box.langSwitchBtn.noBtn.addEventListener(MouseEvent.CLICK  , switchToNO)
			box.langSwitchBtn.enBtn.addEventListener(MouseEvent.CLICK  , switchToEN)
			
			box.password.addEventListener(FocusEvent.FOCUS_IN , focIN)
			box.rePassword.addEventListener(FocusEvent.FOCUS_IN , focIN)
			
			box.password.addEventListener(Event.CHANGE , checkPasswordLength)
			box.rePassword.addEventListener(Event.CHANGE , checkPassword2Length)
			
			box.phone.addEventListener(FocusEvent.FOCUS_IN , focIN)
			box.phone.addEventListener(Event.CHANGE , checkPhone)
			box.fullName.addEventListener(FocusEvent.FOCUS_IN , focIN)
			box.fullName.addEventListener(Event.CHANGE , checkName)
			loader.addEventListener(Event.COMPLETE , ShowCreateResonce)
			addEventListener(Event.ADDED_TO_STAGE, init)
			
			
			Resize()
		}
		
		
			private function switchToEN(e:MouseEvent):void 
		{
			navigateToURL(new URLRequest(stage.loaderInfo.parameters.baseURL + '?lang=en'), '_self')
		}
		
		private function switchToNO(e:MouseEvent):void 
		{
			navigateToURL(new URLRequest(stage.loaderInfo.parameters.baseURL + '?lang=no'), '_self')
		}
		private function showLogin(e:MouseEvent):void 
		{
			closePop();
			stage.dispatchEvent(new Event('showLogin'));	
		}
		
		private function checkName(e:Event):void 
		{
			if (box.fullName.text.length > 3 ) {
				box.nameCheck.alpha = 1
				}
			else {
				box.nameCheck.alpha = .1
				}
		}
		
		private function checkPhone(e:Event):void 
		{
			if (box.phone.text.length > 5 &&  String(box.phone.text).charAt(0) == '0' || String(box.phone.text).charAt(0) == '+') {
				box.phoneCheck.alpha = 1
				}
			else {
				box.phoneCheck.alpha = .1
				}
		}
		
		private function chechUserName(e:Event):void 
		{
			
			if (!DisplayUtils.isValidEmail(box.user.text)) {
				  box.w.text = OCP.LG.pleaseEnterEmailT
				  box.checkUser.alpha = .1
				}
			else {
				var checkR:URLRequest = new URLRequest()
				checkR.url  = Globals.servicURL + "?what=user_exists&user_name=" + box.user.text 
				var v:URLVariables = new URLVariables()
		
				var v:URLVariables = new URLVariables();
				var miniL:URLLoader = new URLLoader();
				miniL.addEventListener(Event.COMPLETE , isTheUserAvailable)
				miniL.data =  v;
				miniL.load(checkR);
			 
				}
		}
		
		private function isTheUserAvailable(e:Event):void 
		{
			
			var o:Object  = JSON.parse(e.target.data);
		if (o.message && o.message == '1') {
			 box.checkUser.alpha = .1
			 box.w.text = OCP.LG.userExistsT
						  
			}
		else {
			 box.checkUser.alpha = 1
			 box.w.text = OCP.LG.RegisterT + ':'
			}
			  
		}
		
		private function checkPassword2Length(e:Event):void 
		{
			
			if (box.rePassword.text.length > 3 && box.rePassword.text == box.password.text) {
				box.checkBox2.alpha = 1
				}
			else {
				box.checkBox2.alpha = .1
				}
		}
		
		private function checkPasswordLength(e:Event):void 
		{
			
			if (box.password.text.length > 3) {
				box.checkBox.alpha = 1
				}
			else {
				box.checkBox.alpha = .1
				}
		}
		
		private function focIN(e:FocusEvent):void 
		{
			var s:String = e.target.text
			
		}
		
		private function ShowCreateResonce(e:Event):void 
		{
			var o:Object = JSON.parse(e.target.data)
			if (o.status == 'error') {
				box.w.text = String(o.status).replace('_' , ' ')  + ' ' +  String(o.message).replace('_' , ' ')
				}
			else {
				box.userCreated.visible = true
				box.userCreated.visible = true
				}
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(Event.RESIZE, Resize)
			if (stage.loaderInfo.parameters.lang == 'no') {
				box.langSwitchBtn.gotoAndStop(2);
				}
			Resize()
			
		}
		
		private function CreateUser(e:MouseEvent):void 
		{
			if(!checkFields()) return
			
			var r:URLRequest = new URLRequest()
			r.url  = Globals.servicURL + "?what=user_register"
			var v:URLVariables = new URLVariables()
			v.username   = box.user.text 
			v.password   = box.password.text 
			v['re_password'] = box.rePassword.text 
			v.phone 	 = box.phone.text 
			v['full_name'] = box.fullName.text
			v.hash = MD5.hash(v.username + v.password + 'R$TFGs2ab!ax')
			r.data  = v
			r.method = URLRequestMethod.POST;
			
			loader.load(r)
			
		}
		
		private function UpdateProfile(e:MouseEvent):void 
		{
			if (!checkFields()) return
				var chegedPass = '' 
				var r:URLRequest = new URLRequest(Globals.servicURL + "?what=user_profile_update")
				var v:URLVariables = new URLVariables()
					v.phone  = box.phone.text 
					v['full_name'] = box.fullName.text
				if (box.checkBox2.alpha == 1 && box.checkBox.alpha == 1 && box.password.text  != 'password') {
					v.password   = box.password.text 
					v['re_password'] = box.rePassword.text 
					chegedPass = box.password.text 
					}
					
			Login.LoggedOBJ.user_data.phone =  box.phone.text;
			Login.LoggedOBJ.user_data.full_name =  box.fullName.text;
				v.hash = MD5.hash(chegedPass + v['full_name'] +  v.phone + 'R$TFGs2ab!ax')
				r.data  = v
				r.method = URLRequestMethod.POST;	
				loader.load(r)
		}
		
		private function checkFields():Boolean 
		{
				if (!DisplayUtils.isValidEmail(box.user.text)) {
				   box.w.text = "Please use e-mail address in the user field"
				   return false
				}
				else if ( box.password.text != box.rePassword.text) {
					 box.w.text = "Passwords did not match"
					 return false
					}
					
				return true
		}
		
		private function keyItDown(e:KeyboardEvent):void 
		{
			if ( e.keyCode == 13) {
				e.preventDefault()
				LogUserIn(null);
				
				}
		}
		
		
		
		private function closePop(e:MouseEvent = null):void 
		{
			this.visible = false;
		}
		
		public function Resize(e=null):void 
		{
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		}
		
		public function ShowUpdate(newPass:Boolean = false):void 
		{
			
			box.createBtn.removeEventListener(MouseEvent.CLICK, CreateUser)
			box.createBtn.addEventListener(MouseEvent.CLICK, UpdateProfile)
			TextField(box.user).mouseEnabled = false
			TextField(box.user).alpha = .5
			this.visible = true;
			box.userCreated.visible = false
			box.user.text = Login.LoggedOBJ.user_data.username
			box.password.text = OCP.LG.passwordT
			box.rePassword.text =  OCP.LG.passwordT
			box.phone.text =Login.LoggedOBJ.user_data.phone
			box.fullName.text = Login.LoggedOBJ.user_data.full_name
			box.w.text = OCP.LG.enterDetailsT
			box.createBtn.t.text = OCP.LG.updateT
			box.checkUser.alpha = .1
			box.checkBox.alpha = .1
			box.checkBox2.alpha = .1
			box.nameCheck.alpha = .1
			box.phoneCheck.alpha = .1
			box.userCreated.doneBtn.visible = true
			box.userCreated.w.text = OCP.LG.updateCompleteT ;
			
			if ( newPass ) {
				box.w.text = OCP.LG.enterNewPasswordT
				box.password.text = ''
				box.rePassword.text = ''
				stage.focus = box.password
				
				}
			Resize()
		}
		
		
		public function Show():void 
		{
			box.createBtn.addEventListener(MouseEvent.CLICK, CreateUser)
			box.createBtn.removeEventListener(MouseEvent.CLICK, UpdateProfile)
			this.visible = true;
			box.userCreated.visible = false
			box.user.text = ''
			box.password.text = ''
			box.rePassword.text = ''
			box.phone.text = ''
			box.fullName.text = ''
			box.w.text = OCP.LG.enterDetailsT
			box.createBtn.t.text = OCP.LG.createT
			stage.focus = box.user
			TextField(box.user).setSelection(0, 199)
			box.userCreated.doneBtn.visible = true
			TextField(box.user).mouseEnabled = true
			TextField(box.user).alpha = 1
			box.checkUser.alpha = .1
			box.checkBox.alpha = .1
			box.checkBox2.alpha = .1
			box.nameCheck.alpha = .1
			box.phoneCheck.alpha = .1
			Resize()

		}
		
	}

}