package popups
{
	import com.greensock.TweenMax;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Radu
	 */
	public class DeletePresentationPop extends deletePresentationPOP
	{
		private var _fun:Function 
		private var _t:String ;
		public function DeletePresentationPop(prompText:String = '' , okTxt:String = 'Ok')
		{
			box.okBtn.addEventListener(MouseEvent.CLICK, OKPressed)
			
			box.cancelBtn.addEventListener(MouseEvent.CLICK, closePop)
		    box.okBtn.t.text= okTxt
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			Resize()
			this.alpha = 0
			TweenMax.to(this, .3, { alpha: 1 } )
			
			if (prompText.length > 2) {
				box.t.text = prompText;
				}
			
			
			addEventListener(Event.ADDED_TO_STAGE , init)
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, detectEnter)
		}
		private function detectEnter(e:KeyboardEvent):void 
		{
			if (e.keyCode == 13) {
				OKPressed(null)
				}
		}
		
		public function closePop(e:MouseEvent):void
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, detectEnter)
			box.okBtn.removeEventListener(MouseEvent.CLICK, OKPressed)
			box.closeBtn.removeEventListener(MouseEvent.CLICK, closePop)
			box.cancelBtn.removeEventListener(MouseEvent.CLICK, closePop)
			this.parent.removeChild(this)
			
		}
		
		private function OKPressed(e:MouseEvent):void
		{
			stage.dispatchEvent(new CEvent(CEvent.DELETE_CURRENT_PRESENTATION,null))
			closePop(e)
		}
		
		public function Resize()
		{
			
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		
		}
	
	}

}