package popups
{
	import com.adobe.images.JPGEncoder;
	import com.adrianparr.utils.GoogleUrlShortener;
	import com.greensock.loading.BinaryDataLoader;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import com.greensock.TweenMax;
	import controllers.PresentationController;
	import data.Data;
	import fl.controls.List;
	import fl.events.ListEvent;
	import fl.managers.StyleManager;
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.PNGEncoderOptions;
	import flash.display.JPEGEncoderOptions;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	import org.alivepdf.images.ColorSpace;
	import org.alivepdf.layout.Mode;
	import org.alivepdf.layout.Orientation;
	import org.pastila.utils.json.formatJSON;
	import thumbs.MYPresentationList;
	import org.alivepdf.layout.Unit;
	import org.alivepdf.pages.Page;
	import org.alivepdf.pdf.PDF;
	import org.alivepdf.saving.Download;
	import org.alivepdf.saving.Method;
	import utils.DisplayUtils;
	
	
	public class MessageBox extends messageBoxLIB
	{
		
		private var _bitmapDatas:Array  = []
		private var _pngJpg:Array  = []
		private var _url:String = ''
		public function MessageBox(url:String)
		{
			box.serviceURL.text  = url
			//box.serviceURL.addEventListener(MouseEvent.ROLL_OVER, showDetails)
			
			this.addEventListener(Event.ADDED_TO_STAGE, init)
		}
		
		
		
		private function detectKEY(e:KeyboardEvent):void 
		{
			
		if (e.keyCode == 192) {
			
			if (box.j.visible) {
				hideDetails()
				}
			else {
				showDetails()
				}
			//showDetails
			}
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(Event.RESIZE , Resize)
			stage.addEventListener(KeyboardEvent.KEY_DOWN, detectKEY)
			Resize()
			hideDetails()
			box.list.addEventListener(ListEvent.ITEM_CLICK, showItemDetals)
			var myFormat:TextFormat = new TextFormat()
			myFormat.color  = 0xffffff;
			StyleManager.setStyle('textFormat',myFormat);
		}
		
		private function showItemDetals(e:ListEvent):void 
		{
			box.j.text = formatJSON(e.item.data)
		}
		private function hideDetails(e=null) {
			
			box.list.visible = false
			box.j.visible = false
			box.bkg.visible = false
			}
		
		private function showDetails(e=null) {
			
			box.list.visible = true
			box.j.visible = true
			box.bkg.visible = true
			
			}
			
		public function addRequest(e) {

			//trace(JSON.stringify(e) , 2222)
			List(box.list).addItem( { label: e, url: 'nu URL', time: getTimer(), data:'no replay yet' } )
			}
		public function Resize(e=null)
		{

			box.x =  0
			box.y =  0
		
		}
		
		public function addResponce( data:Object):void 
		{
			return
			trace(2, List(box.list).getItemAt(List(box.list).length - 1).data )
			var lastItem = List(box.list).getItemAt(List(box.list).length - 1) 
			//if (e.name == lastItem.label) {
				lastItem.data = data
				lastItem.time = getTimer() - lastItem.time
				//trace(lastItem.time , 999)
				//}
		}
	
	}

}