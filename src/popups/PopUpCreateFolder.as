package popups
{
	import com.greensock.TweenMax;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class PopUpCreateFolder extends PopUpCreateLib
	{
		private var langLoader:URLLoader;
		
		public function PopUpCreateFolder()
		{
			box.startBtn.addEventListener(MouseEvent.CLICK, createTheFolder)
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			addEventListener(Event.ADDED_TO_STAGE, init)
			visible = false
			Resize()
			this.alpha = 0
			TweenMax.to(this, .3, {alpha: 1})
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function closePop(e:MouseEvent):void
		{
			this.parent.removeChild(this)
		}
		
		private function createTheFolder(e:MouseEvent):void
		{
			if (box.t.text.length < 2) {
				var tf:TextFormat = new TextFormat()
				tf.color = 0xBE191F
				TextField(box.nameT).setTextFormat(tf)
				TextField(box.nameT).defaultTextFormat = tf
				box.nameT.text = "Please enter folder name"
				return
				}
			box.startBtn.removeEventListener(MouseEvent.CLICK, startAutoPlay)
			langLoader = new URLLoader()
			var request:URLRequest = new URLRequest(Globals.servicURL + "?what=create_slide_group")
			var urlVariable:URLVariables = new URLVariables()
				urlVariable.group_name  = box.t.text
				request.data = urlVariable
				request.method = URLRequestMethod.POST;
			    langLoader.addEventListener(Event.COMPLETE , groupCreated)
			    langLoader.load(request)
		}
		
		private function groupCreated(e:Event):void 
		{		
			this.visible = false;
			stage.dispatchEvent(new Event(CEvent.REFRESH_GROUPS))
		}

		public function Show():void 
		{
			this.visible = true;
			box.t.text = ""
			
			stage.focus = box.t
			
		}
		
		public function Resize()
		{
			
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		
		}
		
		private function dispatchCLICK(e:CEvent):void
		{
			if (box.startBtn.hitTestObject(e.data.head))
			{
				box.startBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK))
				
			}
			else if (box.closeBtn.hitTestObject(e.data.head))
			{
				
				box.closeBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK))
				
			}
			
		}
	
	}

}