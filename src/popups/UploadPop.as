package popups
{
	import com.greensock.plugins.ShortRotationPlugin;
	import data.Uploader;
	import fl.controls.List;
	import fl.data.DataProvider;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.FileReferenceList;
	import flash.net.URLLoader;
	import flash.text.TextFormat;

	import thumbs.FileListUpload;
	import thumbs.MYPresentationList;
	import views.ListView;

	public class UploadPop extends uploadPopLIB
	{
		private var langLoader:URLLoader;
		private var totalFileSize:Number;
		private var _fileRef:FileReferenceList;
		private var targetFolder:String;
		private var tf:TextFormat;
		private var list:ListView
		public function UploadPop()
		{
			box.uploadBtn.addEventListener(MouseEvent.CLICK , dispatStart)
			box.browseBtn.addEventListener(MouseEvent.CLICK , browseForMore)
			addEventListener(Event.ADDED_TO_STAGE, init)
			
			DisableButtons(); 
			this.visible = false
			tf = new TextFormat();
			tf.font = "Lato Bold";
			tf.color = 0xffffff
			tf.size = 12;
			
			//box.lists.setRendererStyle('textFormat', tf);
			list = new ListView(0, 1, true, 'vertical', 1, true, 'top', true);
			list.x = 62.25
			list.y = 97.55
			
			box.addChild(list)
			
			//box.lists.iconFunction = function(item)
			//{
				//
				//if (item.label.substring(0, 2) == "--")
				//{
					//return new Bitmap(new Check())
					//
				//}
				//else
				//{
					//return null
				//}
			//
			//}
			box.cancelBtn.addEventListener(MouseEvent.CLICK, cancelPendingUploads)
			box.closeBtn.addEventListener(MouseEvent.CLICK, cancelPendingUploads)
			
			Resize()
			
		}
		
		private function cancelPendingUploads(e:Event):void
		{
			clearList(e);
			this.visible = false
			Uploader.getInstance().cancelPendingUploads(e);
			DisableButtons();
			
		}
		
		
		
		public function DisableButtons(e=null):void 
		{
			box.browseBtn.visible = true;
			box.uploadBtn.alpha = .2
			box.cancelBtn.alpha = .2
			box.uploadBtn.mouseEnabled  = false
			box.cancelBtn.mouseEnabled  = false
			clearList(e);
			
		}
		
		private function minimizePop(e:MouseEvent):void 	{trace("Minimize")}
		private function browseForMore(e:MouseEvent):void 
		{
			
			clearList(null);
			_fileRef = new FileReferenceList();
			_fileFilter = new FileFilter("Image", "*.jpeg;*.jpg;*.png;*.avi;*.mov;*.mpg;*.mp4;*.flv;*.wma;*.wmv;");
			_fileRef.browse([_fileFilter]);
			_fileRef.addEventListener(Event.SELECT, _onImageSelect);
		
		}
		
		private function clearList(e:Event)
		{
			if (_fileRef)
			{
				if (_fileRef.fileList)
				{
					for (var i:int = 0; i < _fileRef.fileList.length; i++)
					{
						var f:FileReference = FileReference(_fileRef.fileList[i])
						//f.removeEventListener(Event.COMPLETE, _onDataLoaded);
						f.cancel()
						f = null
					}
				}
				_fileRef = null
				
				list.Clear();
				//box.lists.dataProvider = new DataProvider();
				Uploader.localOBJ = new Object()
				_fileRef = new FileReferenceList();
				
			}
		}
		
		
		private function _onImageSelect(e:Event):void
		{
			totalFileSize = 0
			var formulate:Array = [];
			for (var i:int = 0; i < _fileRef.fileList.length; i++)
			{
				var fr:FileReference = _fileRef.fileList[i]
				//box.lists.addItem({label: fr.name + ' ( ' + formatFileSize(fr.size) + ' ) ', data: _fileRef.fileList[i], icon: "Check"})
				totalFileSize += fr.size
				var obj:Object = new Object()
				obj.thumb_image = 'http://www.as3.ro/i'
				obj.type = 'slideTextImage'
				obj.title = fr.name + ' ( ' + formatFileSize(fr.size) + ' ) '
				obj.data =  _fileRef.fileList[i]
				formulate.push(obj);
			}
			//list.createThumb
			list.Clear()
			list.Resize(532.45 ,  348.6);
			
			list.createThumbs(FileListUpload, formulate, true, 0, true)
			
			box.uploadTitle.text = "Uploading " + formulate.length + " files ( " + formatFileSize(totalFileSize) + " ) to " + targetFolder
			Enable()
		
		}
		
		
		public function showUploadPop(e:CEvent = null):void
		{
		
			box.title.text = 'files (size)'
			targetFolder = e.data.title
			box.uploadTitle.text = 'Upload files to: ' + targetFolder;
			Uploader.GroupID = e.data.id
			uploadedImages = 0
			localloadedImages = 0
			totalFileSize = 0
			retry = 0
			box.browseBtn.visible = true;
			DisableButtons();
			_s.addChild(pop)
			OCP.prompt = pop
			Resize()
		
		}
		
		public function formatFileSize(bytes:int):String
		{
			if (bytes < 1024)
				return bytes + " bytes";
			else
			{
				bytes /= 1024;
				if (bytes < 1024)
					return bytes + " Kb";
				else
				{
					bytes /= 1024;
					if (bytes < 1024)
						return bytes + " Mb";
					else
					{
						bytes /= 1024;
						if (bytes < 1024)
							return bytes + " Gb";
					}
				}
			}
			return String(bytes);
		}
		
		private function dispatStart(e:MouseEvent):void 
		{
			box.uploadBtn.mouseEnabled = false
			box.uploadBtn.alpha =  .5;
			box.browseBtn.visible = false;
	
			Uploader.getInstance().UploadList(list);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(CEvent.ALL_IMAGES_UPLOADED, allUploaded)
			stage.addEventListener(CEvent.UPOADING, showProgress)
			stage.addEventListener(Event.RESIZE, Resize)
		}
		
		private function showProgress(e:CEvent):void 
		{
			box.uploadTitle.text = e.data
		}
		
		private function allUploaded(e:Event):void 
		{
			DisableButtons()
			clearList(e);
			this.visible  = false
		}
		
		public function Enable() {
			box.uploadBtn.alpha = 1
			box.uploadBtn.mouseEnabled = true
			box.cancelBtn.alpha = 1
			box.uploadBtn.mouseEnabled  = true
			box.cancelBtn.mouseEnabled  = true
		}

		public function Show(e:String):void 
		{
		
			targetFolder = e
			this.visible = true;
			box.uploadTitle.text = ""	
			Resize();
			box.uploadTitle.text = "Uploading to "  + targetFolder
			stage.focus = box.uploadTitle
		}
		
		public function Resize(e=null)
		{
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			//trace(342 , list.bkg.width , list.bkg.height)
			//list.Resize(532.45 ,  348.6);
			box.x = bkg.width / 2 - box.bkg.width / 2
			box.y = bkg.height / 2 - box.bkg.height / 2
			
		
		
		}
		
		
	
	}

}