package popups
{
	import data.Data;
	import data.Login;
	import flash.display.MovieClip;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	public class InfoPresentationForm extends InfoPresentationLIB
	{
		private var _id:Number = -1;
		private var selectedRadio:MovieClip;
		public function InfoPresentationForm()
		{
			addEventListener(Event.ADDED_TO_STAGE, init)
		}
		
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			var tf:TextFormat = new TextFormat();
			tf.font = "Lato Italic";
			tf.color = 0xffffff
			tf.size = 12;
	
			saveBtn.addEventListener(MouseEvent.CLICK, saveForm)
			cancelBtn.addEventListener(MouseEvent.CLICK, Hide)
			
			CompanyRadio.addEventListener(MouseEvent.CLICK , selectCompany)
			PrivateRadio.addEventListener(MouseEvent.CLICK , selectPrivate)
			PublicRadio.addEventListener(MouseEvent.CLICK , selectPublic)
		}
		
		private function selectPublic(e:MouseEvent):void 
		{
			setRadios(true, 'public')
		}
		
		private function selectPrivate(e:MouseEvent):void 
		{
			setRadios(true, 'private')
		}
		
		private function selectCompany(e:MouseEvent):void 
		{
			setRadios(true, 'company')
			
		}
		
		private function setRadios(enabled:Boolean , acces:String):void
		{
			if (selectedRadio) {
				selectedRadio.gotoAndStop(1)
				selectedRadio = null
				
				}
			switch (acces)
			{
				case 'company': 
					selectedRadio = CompanyRadio
					CompanyRadio.gotoAndStop(2)
					break;
				case 'private': 
					selectedRadio = PrivateRadio
					PrivateRadio.gotoAndStop(2)
					break;
				case 'public': 
					selectedRadio = PublicRadio
					PublicRadio.gotoAndStop(2)
					break;
			}
			
			
			CompanyRadio.visible = enabled
			PrivateRadio.visible = enabled
			PublicRadio.visible = enabled
			
			CompanyRadioT.visible = enabled
			PrivateRadioT.visible = enabled
			PublicRadioT.visible = enabled
		}
		
		
		public function Show(id:Number, title:String  , desc:String ,
							 owner:String, showPrivacy:Boolean,
						     privacy:String, numSLides:int = 0 )
		{					
			
			_id = id;
			this.visible = true
			ownerT.text = OCP.LG.ownerT + ' ' + owner
			numSlidesT.text = numSLides + ' ' + OCP.LG.slidesInPresentationT
			
			
			descriptionT.text = desc			
			setRadios(showPrivacy , privacy)
			saveBtn.visible = showPrivacy
			
				
				
			//if (owner != Login.Name) {
				//tf.color = 0x06ACBD
			//}
			
			
			//ownerT.setTextFormat(tf)
			stage.focus = presentationnameT
			presentationnameT.text = title 
			presentationnameT.setSelection(0, 199)
			
		}
		
		
		
		
		private function saveForm(e:MouseEvent)
		{
			var title = presentationnameT.text
			var description = descriptionT.text
			var access_level:String 
			if (selectedRadio == PrivateRadio) {
				access_level = 'private'
				}
			else if (selectedRadio == CompanyRadio) {
				access_level = 'company'
				}
			else {
				access_level = 'public'
				}
			//var access_level = selectedRadio.name.toLocaleLowerCase()// CompanyRadio.group.selection.label.toLocaleLowerCase()
			
			stage.dispatchEvent(new CEvent(CEvent.SAVE_PRESENTATION, [_id, title, description, access_level]))
			
			Hide(e)
		}
		
		public function Hide(e)
		{
			this.visible = false
			this.dispatchEvent(new CEvent(CEvent.HIDE_FORM, false))
		}
	
	}

}