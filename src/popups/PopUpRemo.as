package popups
{
	import com.greensock.TweenMax;
	import flash.display.Bitmap;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class PopUpRemo extends RemoPopLib
	{
		
		public function PopUpRemo()
		{
			
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			addEventListener(Event.ADDED_TO_STAGE, init)
			bkg.addChild( new Bitmap(OCP.bkgBitmapDat))
			box.youConnected.visible = false
			Resize()
			this.alpha = 0
			TweenMax.to(this, .3, { alpha: 1 } )	
		}
		
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(Event.RESIZE , Resize)
			
			 if (CONFIG::debug != true) {
				 stage.displayState = StageDisplayState.FULL_SCREEN
				 }
			
		
		}
		
		private function closePop(e:MouseEvent):void
		{
			stage.removeEventListener(Event.RESIZE , Resize)
			this.parent.removeChild(this)
		}
		
		private function startAutoPlay(e:MouseEvent):void
		{
			this.parent.removeChild(this)
		
		}
		
		public function Resize(e=null)
		{
			
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = Globals._DIM.width/2 - box.width/2
			box.y =  Globals._DIM.height/2 - box.height/2
		
		}
		
		public function Close():void 
		{
			box.removeEventListener(MouseEvent.CLICK, closePop)
			this.parent.removeChild(this)
		}
	}
}