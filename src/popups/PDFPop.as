package popups
{
	import com.adobe.images.JPGEncoder;
	import com.adrianparr.utils.GoogleUrlShortener;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.BinaryDataLoader;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import com.greensock.TweenMax;
	import controllers.PresentationController;
	import data.Data;
	import data.Login;
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.PNGEncoderOptions;
	import flash.display.JPEGEncoderOptions;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import org.alivepdf.colors.RGBColor;
	import org.alivepdf.drawing.Caps;
	import org.alivepdf.drawing.Joint;
	import org.alivepdf.drawing.WindingRule;
	import org.alivepdf.images.ColorSpace;
	import org.alivepdf.layout.Mode;
	import org.alivepdf.layout.Orientation;
	import thumbs.MYPresentationList;
	import utils.DisplayUtils;
	import org.alivepdf.layout.Unit;
	import org.alivepdf.pages.Page;
	import org.alivepdf.pdf.PDF;
	import org.alivepdf.saving.Download;
	import org.alivepdf.saving.Method;
	
	public class PDFPop extends pdfPopLIB
	{
		
		private var _bitmapDatas:Array = []
		private var _pngJpg:Array = []
		private var _url:String = ''
		private var _borderColor:Number = 0x929496;
		private var _rectColor:Number = 0xF0F0F0;
		private var _title:String = 'pdf';
		private var logo:LogoV;
		private var _owner:String = '';
		
		public function PDFPop(title:String , owner:String = '')
		{
			
			_title = title
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			Resize()
			box.btn1.alpha = .5
			box.btn2.alpha = .5
			box.btn3.alpha = .5
			box.btn4.alpha = .5
			box.btn1.mouseEnabled = false
			box.btn2.mouseEnabled = false
			box.btn3.mouseEnabled = false
			box.btn4.mouseEnabled = false
			box.btn1.addEventListener(MouseEvent.CLICK, generatePDF1)
			box.btn2.addEventListener(MouseEvent.CLICK, generatePDF2)
			box.btn3.addEventListener(MouseEvent.CLICK, generatePDF3)
			box.btn4.addEventListener(MouseEvent.CLICK, generatePDF4)
			_owner = owner;
			this.alpha = 0
			TweenMax.to(this, .3, {alpha: 1})
			addEventListener(Event.ADDED_TO_STAGE, init)
			
			Data.getInstance().PreloadCurrentPResentation(Ready, showProgress, PresentationController.NumSlides);
			box.loadBar.visible = false;
			
			logo = new LogoV()
			
		}
		
		private function showProgress(e:LoaderEvent):void
		{
			//trace(333, JSON.stringify(e.target))
			box.loadBar.visible = true;
			box.loadBar.width = e.target.progress * box.gBkg.width
		}
		
		public function Ready(e):void
		{
			
			_bitmapDatas = []
			_pngJpg = []
			box.loadBar.visible = false
			var l:LoaderMax = e.target
			for (var i:int = 0; i < PresentationController.NumSlides.length; i++)
			{
				
				var d:ImageLoader = LoaderMax.getLoader("BIG" + PresentationController.NumSlides[i].id)
				
				if (d)
				{
					var b:Bitmap = d.rawContent
					var ba:ByteArray = new ByteArray()
					b.bitmapData.encode(b.getRect(stage), new JPEGEncoderOptions(80), ba)
					
					_bitmapDatas.push(ba)
					_pngJpg.push(b)
					
				}
			}
			box.btn1.alpha = 1
			box.btn2.alpha = 1
			box.btn3.alpha = 1
			box.btn4.alpha = 1
			box.btn1.mouseEnabled = true
			box.btn2.mouseEnabled = true
			box.btn3.mouseEnabled = true
			box.btn4.mouseEnabled = true
			box.preload.gotoAndStop(1)
		
		}
		
	function getYYMMDD():String
{
	var dateObj:Date = new Date();
	var year:String = String(dateObj.getFullYear());
	var month:String = String(dateObj.getMonth() + 1) ;
	if (month.length == 1) {
		month = "0"+month;
	}
	var date:String = String(dateObj.getDate());
	if (date.length == 1) {
		date = "0"+date;
	}
	//return year.substring(2, 4)+month+date;
	return date + '.' + month  + '.' + year.substring(2, 4);
}
 
		private function addHeader(pdf:PDF, totalPages:Number):void 
		{
			var k = pdf.getY()
			
			var w = pdf.getCurrentPage().wPt - 20
				pdf.setY(k)
				pdf.setX(20)
				pdf.addMultiCell(w, 30, "Printdate " +  getYYMMDD(), 0, 'L')
				pdf.setY(k)
				var ownerText 
				if (_owner != '') {
					ownerText = ' by: ' + _owner
					}
				pdf.addMultiCell(w, 30, _title + ownerText , 0,  'C')
				pdf.setY(k)
				pdf.setX(0)
				pdf.addMultiCell(w, 30, "Page " + pdf.totalPages + " of " + totalPages, 0,  'R')
		}
		
		
		private function generatePDF1(e:MouseEvent):void
		{
			
			var pdf:PDF = new PDF(Orientation.LANDSCAPE, Unit.POINT, false)
				pdf.setMargins(10, 10, 0, 0);
			var local:Boolean = true
			
			for (var i:int = 0; i < _bitmapDatas.length; i++)
			{
				
				pdf.addPage(new Page(Orientation.LANDSCAPE, Unit.POINT, null))
				
				var s = _pngJpg[i]
				var w = pdf.getCurrentPage().wPt - 20
				var h = w / 1.77777777778
				var R16x9:Rectangle = new Rectangle(0, 0, w, h)
				
				
				DisplayUtils.fitIntoRect(s, R16x9, false)
				R16x9.x = 10
				R16x9.y = (pdf.getCurrentPage().hPt - 20 -  h)/2
				
				
				pdf.beginFill(new RGBColor(_rectColor));
				
				
				pdf.drawRect(R16x9)
				
				pdf.addImageStream(_bitmapDatas[i], ColorSpace.DEVICE_RGB, null, (R16x9.width - s.width) / 2 , R16x9.y - 10 + (R16x9.height - s.height) / 2,
										 
										 s.width, s.height)
				
				pdf.endFill();
				pdf.lineStyle(new RGBColor(_borderColor), 1, 0, 1, WindingRule.NON_ZERO, null, null, Caps.NONE, Joint.MITER);
				pdf.drawRect(R16x9)
				
				pdf.addImage(logo, null, (pdf.getCurrentPage().wPt - 20) / 2 - logo.width/4 , pdf.getCurrentPage().hPt - 40, logo.width/2, logo.height/2)
				
				pdf.textStyle(new RGBColor ( 0x000000 ), 1 );
				
				
				//pdf.addMultiCell(w,10,"Last text into a cell",1,);
				//pdf.addText( "Printdate" +  getYYMMDD(), 10, 20)
				addHeader(pdf, _bitmapDatas.length)
				
				
			
				//pdf.addCell(w, 30, "Page" + (i+1) + "/" + _bitmapDatas.length, 0, 0, 'R')
			}
			savePDF(pdf, e)
		}
		

		private function generatePDF2(e:MouseEvent):void
		{
			
			var pdf:PDF = new PDF(Orientation.LANDSCAPE, Unit.POINT, false)
				pdf.setMargins(10, 10, 0, 0);
			var local:Boolean = true
			var theY:int = 0
			
			for (var i:int = 0; i < _bitmapDatas.length; i++)
			{
				if (i % 2 == 0)
				{
					pdf.addPage(new Page(Orientation.PORTRAIT, Unit.POINT, null))
					theY = 65
					addHeader(pdf,  Math.ceil(_bitmapDatas.length/2) )
				}
				
				
				var s = _pngJpg[i]
				var w = pdf.getCurrentPage().wPt - 80
				var h = w / 1.77777777778
				var R16x9:Rectangle = new Rectangle(0, 0, w, h)
				
				DisplayUtils.fitIntoRect(s, R16x9, false)
				R16x9.y = theY
				R16x9.x = 40
				pdf.beginFill(new RGBColor(_rectColor));
				pdf.drawRect(R16x9)
				pdf.beginFill(new RGBColor(_rectColor));
				pdf.drawRect(R16x9)
				pdf.addImageStream(_bitmapDatas[i], ColorSpace.DEVICE_RGB, null,  (R16x9.width - s.width) / 2 + 30 , R16x9.y - 10 + (R16x9.height - s.height) / 2, s.width, s.height)
				theY += (pdf.getCurrentPage().hPt - 20) / 2 + 5
				pdf.endFill();
				pdf.lineStyle(new RGBColor(_borderColor), 1, 0, 1, WindingRule.NON_ZERO, null, null, Caps.NONE, Joint.MITER);
				pdf.drawRect(R16x9)
				var chenar:Rectangle = new Rectangle(20, 50, pdf.getCurrentPage().wPt - 40, pdf.getCurrentPage().hPt - 100 )
				pdf.drawRect(chenar)
				pdf.addImage(logo, null, (pdf.getCurrentPage().wPt - 20) / 2 - logo.width/4 , pdf.getCurrentPage().hPt - 40, logo.width/2, logo.height/2)
				
			}
			savePDF(pdf, e)
		}
		
		private function generatePDF3(e:MouseEvent):void
		{
			
			var pdf:PDF = new PDF(Orientation.LANDSCAPE, Unit.POINT, false)
				pdf.setMargins(10, 10, 0, 0);
			var local:Boolean = true
			var theY:int = 0
			for (var i:int = 0; i < _bitmapDatas.length; i++)
			{
				if (i % 10 == 0)
				{
					pdf.addPage(new Page(Orientation.PORTRAIT, Unit.POINT, null))
					theY = 65
					addHeader(pdf,  Math.ceil(_bitmapDatas.length/10) )
				}
				
				
				var s = _pngJpg[i]
				
				var w = (pdf.getCurrentPage().wPt - 20) / 2 - 65
				var h = w / 1.77777777778
				var R16x9:Rectangle = new Rectangle(0, 0, w, h)
				
				DisplayUtils.fitIntoRect(s, R16x9, false)
				R16x9.y = theY + 10
				R16x9.x = i % 2 == 0 ? 45 : (pdf.getCurrentPage().wPt - 20) / 2 + 10 + 30
				pdf.beginFill(new RGBColor(_rectColor));
				pdf.drawRect(R16x9)
																				
				pdf.addImageStream(_bitmapDatas[i], ColorSpace.DEVICE_RGB, null, R16x9.x + (R16x9.width - s.width) / 2 - 10, R16x9.y - 10 + (R16x9.height - s.height) / 2, s.width, s.height)
				
				if (i % 2 == 1)
				{
					theY += R16x9.height + 15
				}
				
				pdf.endFill();
				pdf.lineStyle(new RGBColor(_borderColor), 1, 0, 1, WindingRule.NON_ZERO, null, null, Caps.NONE, Joint.MITER);
				pdf.drawRect(R16x9)
				var chenar:Rectangle = new Rectangle(20, 50, pdf.getCurrentPage().wPt - 40, pdf.getCurrentPage().hPt - 100 )
				pdf.drawRect(chenar)
				
				pdf.addImage(logo, null, (pdf.getCurrentPage().wPt - 20) / 2 - logo.width/4 , pdf.getCurrentPage().hPt - 40, logo.width/2, logo.height/2)
			}
			savePDF(pdf, e)
		}
		
		private function generatePDF4(e:MouseEvent):void
		{
			//if(pdf) pdf = null
			var pdf:PDF = new PDF(Orientation.LANDSCAPE, Unit.POINT, false)
				pdf.setMargins(10, 10, 0, 0);
			var local:Boolean = true
			var theY:int = 0
			var theX:int = 0
			
			
			for (var i:int = 0; i < _bitmapDatas.length; i++)
			{

				
				if (i % 3 == 0 && i > 0)
				{
					theY += h + 3 + 10
				}
				if (i % 21 == 0)
				{
					
					pdf.addPage(new Page(Orientation.PORTRAIT, Unit.POINT, null))
					theY = 75
					addHeader(pdf,  Math.ceil(_bitmapDatas.length/21) )
				}
				if (i % 3 == 0)
				{
					theX = 40
					
				}
				
			
				var s = _pngJpg[i]
				
				var w =  (pdf.getCurrentPage().wPt - 29) / 3 - 30
				var h = w / 1.77777777778
				var R16x9:Rectangle = new Rectangle(0, 0, w, h)
				
				
				DisplayUtils.fitIntoRect(s, R16x9, false)
				R16x9.x = theX 
				R16x9.y = theY 
				pdf.beginFill(new RGBColor(_rectColor));
				
				pdf.drawRect(R16x9)
				pdf.addImageStream(_bitmapDatas[i], ColorSpace.DEVICE_RGB, null, theX + (R16x9.width - s.width) / 2 - 10, 
														theY + (R16x9.height - s.height) / 2 - 10, s.width, s.height)
				
				pdf.endFill();
				pdf.lineStyle(new RGBColor(_borderColor), 1, 0, 1, WindingRule.NON_ZERO, null, null, Caps.NONE, Joint.MITER);
				pdf.drawRect(R16x9)
				var chenar:Rectangle = new Rectangle(20, 50, pdf.getCurrentPage().wPt - 40, pdf.getCurrentPage().hPt - 100 )
				pdf.drawRect(chenar)
				theX += R16x9.width + 20
				pdf.addImage(logo, null, (pdf.getCurrentPage().wPt - 20) / 2 - logo.width/4 , pdf.getCurrentPage().hPt - 40, logo.width/2, logo.height/2)	
			}
			
			
			savePDF(pdf, e)
		
		}
		
		private function savePDF(pdf:PDF, e:Event):void
		{
			
			var ff:FileReference = new FileReference()
			if (!e.ctrlKey)
			{
				
				ff.addEventListener(Event.COMPLETE, saveComplet)
				var FILE:ByteArray = pdf.save(Method.LOCAL)
				ff.save(FILE, _title + ".pdf");
				
			}
			else
			{
				
				pdf.save(Method.REMOTE, "http://clients.onecom.no/pdf/create.php", Download.INLINE, _title + ".pdf");
			}
		
		}
		
		private function saveComplet(e:Event):void
		{
		
		}
		
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.addEventListener(Event.RESIZE, Resize)
		}
		
		public function closePop(e:MouseEvent):void
		{
			
			box.closeBtn.removeEventListener(MouseEvent.CLICK, closePop)
			
			this.parent.removeChild(this)
		}
		
		public function Resize(e = null)
		{
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		}
		
		private function onExpandComplete(event:Event):void
		{
			var shortUrl:String = GoogleUrlShortener(event.target).shortUrl;
			var longUrl:String = GoogleUrlShortener(event.target).longUrl;
		}
		
		private function onExpandIOError(event:Event):void
		{
		}
		
		private function onExpandSecurityError(event:Event):void
		{
		}
		
		private function onShortenIOError(event:Event):void
		{
		
		}
		
		private function onShortenSecurityError(event:Event):void
		{
		}
	
	}

}