package popups
{
	import com.greensock.TweenMax;
	import flash.display.Bitmap;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import org.qrcode.QRCode;
	import player.PresentationPlayer;
	import remote.AirClient;

	public class PopupStartRemo extends RemoPopLib
	{
		private var _id:String 
		private var _air:AirClient;
		public function PopupStartRemo(air, id)
		{
			_id = id;
			_air = air;
			
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
		
			bkg.addChild( new Bitmap(OCP.bkgBitmapDat))
			box.youConnected.visible = false
			Resize()
			this.alpha = 0
			TweenMax.to(this, .3, { alpha: 1 } )	
			box.startBtn.addEventListener(MouseEvent.CLICK , closePop)
			addEventListener(Event.ADDED_TO_STAGE, init)
			//if (_air.connected)  {	}
			//else {
				//stage.addEventListener(CEvent.REMOTE_CONNECTED , init);
				//}
		}
		
		private function init(e:Event = null):void
		{
			
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(Event.RESIZE , Resize)
			stage.addEventListener(CEvent.REMOTE_CONNECTED , onConected)
	
			stage.addEventListener(CEvent.REMOTE_SHOW_SLIDE , Close);
			box.msg.t.text = OCP.LG.promptPresentationT
			
			
			if (_air.connected) {
				
				PairPlayerWithServer()
				}
			else {
				stage.addEventListener(CEvent.AIR_SERVER_CONNECTED, PairPlayerWithServer)
				}
			

		}
		
		private function PairPlayerWithServer(e:CEvent=null):void 
		{
			stage.removeEventListener(CEvent.AIR_SERVER_CONNECTED, PairPlayerWithServer)
			var url:String = Globals.baseURL + 'R' +"?id=" + _id//PresentationPlayer.presentationID
			_air.Pair();
		
			
			box.promp.linkU.text = url
			makeQR(url)
		}
		
		private function onConected(e:CEvent):void 
		{
			trace("ON CONECCETED")
			box.youConnected.visible = true;
			box.addChild(box.youConnected);
			
		}
		
		private function goTOLINK(e:MouseEvent):void
		{
			navigateToURL(new URLRequest(box.promp.linkU.text))
		}
		
		private function makeQR(shortUrl)
		{
			var QR:QRCode = new QRCode();
			box.promp.linkU.addEventListener(MouseEvent.CLICK, goTOLINK)
			box.promp.linkU.text = shortUrl
			QR.encode(shortUrl);
			var qrImg:Bitmap = new Bitmap(QR.bitmapData);
			var colorToReplace:uint = 0xffffffff;
			var newColor:uint = 0xff393C44;
			var maskToUse:uint = 0xffffffff;
			var colorToReplace2:uint = 0xff000000;
			var newColor2:uint = 0xff919694;
			
			var rect:Rectangle = new Rectangle(0, 0, QR.bitmapData.width, QR.bitmapData.height);
			var p:Point = new Point(0, 0);
			QR.bitmapData.threshold(QR.bitmapData, rect, p, "==", colorToReplace, newColor, maskToUse, true);
			QR.bitmapData.threshold(QR.bitmapData, rect, p, "==", colorToReplace2, newColor2, maskToUse, true);
			
			qrImg.smoothing = true
			box.addChild(qrImg)
			qrImg.y = 70
			qrImg.x = box.youConnected.x
			qrImg.y = box.youConnected.y
			qrImg.width = qrImg.height = 200
			//_view.stage.addChild(remoPop)
		
		}
		
		private function closePop(e:MouseEvent):void
		{
			stage.removeEventListener(Event.RESIZE , Resize)
			this.parent.removeChild(this)
		}
		
		private function startAutoPlay(e:MouseEvent):void
		{
			this.parent.removeChild(this)
		
		}
		
		public function Resize(e=null)
		{
			
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = Globals._DIM.width/2 - box.width/2
			box.y =  Globals._DIM.height/2 - box.height/2
		
		}
		
		public function Close(e=null):void 
		{
			trace("SDASDAS")
			box.removeEventListener(MouseEvent.CLICK, closePop)
			this.parent.removeChild(this)
		}
	}
}