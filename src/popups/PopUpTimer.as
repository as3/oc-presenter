package popups
{
	import com.greensock.TweenMax;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;

	public class PopUpTimer extends PopUpTimerLib
	{
		
		public function PopUpTimer()
		{
			box.startBtn.addEventListener(MouseEvent.CLICK, startAutoPlay)
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			addEventListener(Event.ADDED_TO_STAGE, init)
			Resize()
			this.alpha = 0
			TweenMax.to(this, .3, { alpha: 1 } )
			box.cancelBtn.addEventListener(MouseEvent.CLICK , closePop)
			
		addEventListener(Event.ADDED_TO_STAGE , init)
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, detectEnter)
			stage.addEventListener(Event.RESIZE, Resize)
		}
		private function detectEnter(e:KeyboardEvent):void 
		{
			if (e.keyCode == 13) {
				startAutoPlay(null)
				}
		}
		
		

		private function closePop(e:MouseEvent):void
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, detectEnter)
			this.parent.removeChild(this)
		
		}
		
		private function startAutoPlay(e:MouseEvent):void
		{
			box.startBtn.removeEventListener(MouseEvent.CLICK, startAutoPlay)
			box.closeBtn.removeEventListener(MouseEvent.CLICK, closePop)
			stage.dispatchEvent(new CEvent(CEvent.START_AUTO_PLAY, box.stepper.val))
			closePop(null)
		
		}
		
		public function Resize(e=null)
		{
			
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		
		}
		
		private function dispatchCLICK(e:CEvent):void
		{
			if (box.startBtn.hitTestObject(e.data.head))
			{
				
				box.startBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK))
				
			}
			else if (box.closeBtn.hitTestObject(e.data.head))
			{
				
				box.closeBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK))
				
			}
			else if (box.stepper.plus.hitTestObject(e.data.head))
			{
				
				box.stepper.plus.dispatchEvent(new MouseEvent(MouseEvent.CLICK))
				
			}
			else if (box.stepper.minus.hitTestObject(e.data.head))
			{
				
				box.stepper.minus.dispatchEvent(new MouseEvent(MouseEvent.CLICK))
				
			}
		}
	
	}

}