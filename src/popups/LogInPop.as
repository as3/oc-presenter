package  popups
{
	import com.adobe.crypto.MD5;
	import com.adobe.webapis.URLLoaderBase;
	import data.Data;
	import data.Login;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.net.navigateToURL;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import utils.DisplayUtils;
	
	public class LogInPop extends LogInPopLib
	{
	
		public function LogInPop() 
		{
			box.startBtn.addEventListener(MouseEvent.CLICK, LogUserIn)
			box.startBtnFull.addEventListener(MouseEvent.CLICK, LogUserInFull)
			box.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			recoverBox.backBtn.addEventListener(MouseEvent.CLICK, goBackToLogInScreen)
			recoverBox.gotoBtn.addEventListener(MouseEvent.CLICK, goBackToLogInScreen)
			box.logGoogle.addEventListener(MouseEvent.CLICK, Login.callGoogleLogIn)
			recoverBox.closeBtn.addEventListener(MouseEvent.CLICK, closePop)
			addEventListener(Event.ADDED_TO_STAGE, init)
			visible=false
			
			box.t.addEventListener(FocusEvent.FOCUS_IN, focusOnUser);
			box.p.addEventListener(FocusEvent.FOCUS_IN, focusOnPass);
			box.t.tabIndex = 1;
			box.p.tabIndex = 2; 
			box.p.addEventListener(KeyboardEvent.KEY_DOWN , keyItDown)
			box.p.displayAsPassword = true;
			
			box.langSwitchBtn.noBtn.addEventListener(MouseEvent.CLICK  , switchToNO)
			box.langSwitchBtn.enBtn.addEventListener(MouseEvent.CLICK  , switchToEN)
			box.recoverPasswordBtn.addEventListener(MouseEvent.CLICK  , showRecoveryBox)
			recoverBox.startBtn.addEventListener(MouseEvent.CLICK , askForNewPassword)
			//box.keepMe.addEventListener(Event.CHANGE , showChange)
			box.logGoogle.alpha =  .5
			box.logGoogle.mouseEnabled = false
			Login.getInstance().getGoogleLink();
			
			
			var myTf:TextFormat = new TextFormat(); 
			
			myTf.color = 0xffffff; 
			box.keepMe.setStyle("textFormat", myTf);

		}
		
		private function switchToEN(e:MouseEvent):void 
		{
			navigateToURL(new URLRequest(stage.loaderInfo.parameters.baseURL + '?lang=en'), '_self')
		}
		
		private function switchToNO(e:MouseEvent):void 
		{
			navigateToURL(new URLRequest(stage.loaderInfo.parameters.baseURL + '?lang=no'), '_self')
		}
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(CEvent.LOGED_OUT, logOUT)
			stage.addEventListener(CEvent.LOG_IN_FAIL, logInFail)
			stage.addEventListener(Event.RESIZE , Resize)
			stage.addEventListener(CEvent.WE_HAVE_LINK , enableGoogleBtn)
			if (stage.loaderInfo.parameters.lang == 'no') {
				box.langSwitchBtn.gotoAndStop(2);
				}
				
				//trace(2312312434, stage.loaderInfo.parameters.lang )
			
		}
		
		private function enableGoogleBtn(e:Event):void 
		{
			box.logGoogle.alpha =  1
			box.logGoogle.mouseEnabled = true
		}
		
		private function logOUT(e:CEvent):void 
		{	
		
		
				recoverBox.gotoBtn.visible = false
				recoverBox.startBtn.visible = false
				Login.getInstance().getGoogleLink();
			
		}

		private function goBackToLogInScreen(e:MouseEvent):void 
		{
			recoverBox.startBtn.visible = true
			recoverBox.gotoBtn.visible = true
			recoverBox.backBtn.visible = false
			recoverBox.visible = false
		}

		private function askForNewPassword(e:MouseEvent):void 
		{
			
			if (!DisplayUtils.isValidEmail(recoverBox.t.text )) {
				recoverBox.w.text = OCP.LG.pleaseEnterEmailT
				return
				}
				Login.getInstance().AskforNewPassword(recoverBox.t.text, showRecoverResults)		
		}
		
	
		private function showRecoverResults(e:Event):void 
		{
			
			var o:Object = JSON.parse(e.target.data)
			if (o.status == 'success') {
				recoverBox.backBtn.visible = true;
				recoverBox.startBtn.visible = false
				recoverBox.gotoBtn.visible = false
				recoverBox.w.text = OCP.LG.newPasswordSent
				 }
			else {
				 recoverBox.w.text =String(o.status).replace('_' , ' ')  + ' ' +  String(o.message).replace('_' , ' ')
					
				}
		}
		
		private function goBackToLogIN(e:MouseEvent):void 
		{
			this.recoverBox.visible  = false
			
		}
		
		private function showRecoveryBox(e:MouseEvent):void 
		{
			this.recoverBox.visible  = true
		}
		
		private function keyItDown(e:KeyboardEvent):void 
		{
			
		
		if ( e.keyCode == 13) {
				e.preventDefault()
				LogUserIn(null);
				
				}
		}
		
		private function focusOnPass(e:FocusEvent):void 
		{
			TextField(box.p).displayAsPassword = true;
			if (box.p.text == OCP.LG.passwordT) {
				box.p.text = ''
				}
				
		}
		
		private function focusOnUser(e:FocusEvent):void 
		{
			if (box.t.text == OCP.LG.usernameT) {
				box.t.text = '';
				}
		}
		
		private function closePop(e:MouseEvent):void 
		{
			this.visible = false;
			recoverBox.backBtn.visible = false
			recoverBox.gotoBtn.visible = true
			recoverBox.startBtn.visible = true
			OCP.prompt = null
		}
		
		private function LogUserIn(e:MouseEvent = null):void 
		{
			Login.getInstance().LogUserIn(box.t.text ,box.p.text, box.keepMe.selected)
		}
		
		private function LogUserInFull(e:MouseEvent = null):void 
		{
			Login.getInstance().LogUserIn(box.t.text , box.p.text, box.keepMe.selected)
			stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE
		}
		
		
		private function logInFail(e:CEvent):void 
		{
			box.w.text = OCP.LG.wrongUserorPass
		}
	
		
		
		public function Show():void 
		{
			
			
			this.visible = true;
			box.w.text = OCP.LG.LoginT
			OCP.prompt = this
			box.p.displayAsPassword = false;
			box.p.text = OCP.LG.passwordT
			
			
			box.t.text = OCP.LG.usernameT
	
			TextField(box.t).setSelection(0,199)
			this.recoverBox.visible = false
			recoverBox.w.text = OCP.LG.pleaseEnterEmailT
			recoverBox.t.text = ''
			recoverBox.startBtn.visible = true;
			//stage.focus = box.t
			Resize()

		}
		
		public function Resize(e=null):void 
		{
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
			recoverBox.x = box.x
			recoverBox.y = box.y
		}
		
		
	}

}