package popups
{
	import com.greensock.TweenMax;
	import controllers.PresentationController;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.MouseEvent;
	

	public class SharedStartPop extends sharedPresentationLIB
	{
		public static var isVisible = false;
		public function SharedStartPop(prompText= '')
		{
			box.startBtn.addEventListener(MouseEvent.CLICK , closePop)
			SharedStartPop.isVisible = true
			addEventListener(Event.ADDED_TO_STAGE, init)
			Resize(null)
			
			//box.shareBtn.addEventListener(MouseEvent.CLICK , shareP)
			box.remoteBtn.addEventListener(MouseEvent.CLICK , remoThis)
			box.downloadB.addEventListener(MouseEvent.CLICK , showPDFPopup)
		}
		
		
		private function showPDFPopup(e:MouseEvent):void 
		{
			var h:String = ''
			if(!PresentationController.NumSlides) return
			for (var i:int = 0; i < PresentationController.NumSlides.length; i++)
			{
				h += PresentationController.NumSlides[i].id + '|'
			}
			h = h.substring(0, h.length - 1)
			var pdfPop = new PDFPop('presentation')
			
			stage.addChild(pdfPop)
			
		}
		
		
		private function remoThis(e:MouseEvent):void 
		{
			    stage.dispatchEvent(new CEvent(CEvent.SHOW_REMO_POP, null));
				this.visible = false
		}
		
		private function shareP(e:MouseEvent):void 
		{
			
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(Event.RESIZE, Resize)
		}
		
		
		
		public function closePop(e:MouseEvent):void
		{
			
			box.startBtn.removeEventListener(MouseEvent.CLICK , closePop)
			SharedStartPop.isVisible = false
			
			stage.displayState =  StageDisplayState.FULL_SCREEN
			this.parent.removeChild(this)
		}
		
		
		
		public function Resize(e)
		{
			
			bkg.width = Globals._DIM.width
			bkg.height = Globals._DIM.height
			box.x = bkg.width / 2 - box.width / 2
			box.y = bkg.height / 2 - box.height / 2
		
		}
	
	}

}