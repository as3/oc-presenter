package menus
{
	import com.greensock.TweenLite;
	import data.Data;
	import data.Login;
	import data.Uploader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	import popups.DeleteFolderPop;
	import thumbs.FolderList;
	import thumbs.PresentationList;
	import views.TreeView;
	
	public class LeftMenu extends leftMenuLib
	{
		public var Thiles;
		public static var filter:String = 'public'
		private var OBJ:Object
		private var form;
		private var mPop:menuWheelDrive = new menuWheelDrive()
		private var speed:Number = .2;
		private var _list:TreeView;
		private var _selectIDX:Number;
		private var lastClicked:Sprite;
		
		public function LeftMenu()
		{
			addEventListener(Event.ADDED_TO_STAGE, init)
		}
		
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			_list = new TreeView(0, 1, true, 'vertical', 1, false);
			
			driveBtn.visible = false
			companyBtn.visible = false
			addChild(_list);
			
			form = new FolderForm();
			stage.addEventListener(CEvent.SHOW_FOLDER_INFO, showFolderInfo);
			stage.addEventListener(CEvent.SHOW_MINI_POP, showMiniPop)
			stage.addEventListener(CEvent.ALL_IMAGES_UPLOADED, refreshTheFolder)
			stage.addEventListener(CEvent.SHOW_FOLDER_INFO, showFolderInfo)
			stage.addEventListener(CEvent.REFRESH_CURRENT_FOLDER, listItemClicked)
			stage.addEventListener(CEvent.FOLDERS_RECIVED, fillListWIthFolders)
			stage.addEventListener(CEvent.HERE_ARE_THE_PUBLIC_PRESENTATIONS, fillListWIthPublicPresentation)
			stage.addEventListener(CEvent.DRAG_THIS_IN, externalSlideDragging);
			driveBtn.createBtn.addEventListener(MouseEvent.CLICK, showNewFolerForm)
			driveBtn.bkg.addEventListener(MouseEvent.CLICK, clickHandler);
			companyBtn.addEventListener(MouseEvent.CLICK, clickHandler);
			publicBtn.addEventListener(MouseEvent.CLICK, clickHandler);
			
			mPop.addAllBtn.addEventListener(MouseEvent.CLICK, addAllSlidesToCurrentPresentation)
			mPop.viewBtn.addEventListener(MouseEvent.CLICK, viewFolder)
			mPop.infoBtn.addEventListener(MouseEvent.CLICK, showFolderInfo)
			mPop.uploadBtn.addEventListener(MouseEvent.CLICK, uploadFolder)
			mPop.delBtn.addEventListener(MouseEvent.CLICK, deletFolder)
			mPop.visible = false
			_list.visible = false
			addChild(driveBtn)
			addChild(companyBtn)
			addChild(publicBtn)
			addChild(mPop);
			stage.addEventListener(CEvent.LOGED_IN, onLogedIN);
			stage.addEventListener(CEvent.LOGED_OUT, onLogedOUT);
		
		}
		
		private function onLogedOUT(e:CEvent):void
		{
			driveBtn.visible = false
			companyBtn.visible = false
		}
		
		private function externalSlideDragging(e:CEvent):void
		{
			
			if (e.data != _list)
			{
				_list.editPosition = true
				_list.dragedThumbnail = e.data.dragedThumbnail
				_list.showInserLocation()
				
			}
		
		}
		
		private function onLogedIN(e:Event):void
		{
			driveBtn.visible = true
			companyBtn.visible = true
			stage.dispatchEvent(new CEvent(CEvent.LOAD_PUBLIC_PRESENTATIONS, null))
		
		}
		
		private function listItemClicked(e:Event):void
		{
			Data.getInstance().GetSlidesInFolder(FolderList.HiglightedThumb.OBJ.id);
		}
		
		private function clickHandler(e:MouseEvent):void
		{
			
			
		
			if (lastClicked) {
				lastClicked.p.mouseEnabled = true
				lastClicked.p.mouseChildren = true
				lastClicked.ra(null)
				}
	
			//resetBtns()
			_list.y = 35
			_list.visible = false;
			_list.Clear();
			stage.dispatchEvent(new CEvent(CEvent.LOADING_MAIN_VIEW, null))
			if (e.currentTarget == publicBtn)
			{
				lastClicked = e.currentTarget
				LeftMenu.filter = 'public'
				TweenLite.to(publicBtn, speed, {y: 0});
				TweenLite.to(companyBtn, speed, {y: (bkg.height) - 70});
				TweenLite.to(driveBtn, speed, {y: (bkg.height) - 35});
				//publicBtn.gotoAndStop(2);
				setTimeout(showList, speed * 1001, 35);
				stage.dispatchEvent(new CEvent(CEvent.LOAD_PUBLIC_PRESENTATIONS, null))
					//Data.getInstance().loadPublicPresentations();
			}
			else if (e.currentTarget == companyBtn)
			{
				lastClicked = e.currentTarget
				LeftMenu.filter = 'company'
				TweenLite.to(publicBtn, speed, {y: 0});
				TweenLite.to(companyBtn, speed, {y: 35});
				TweenLite.to(driveBtn, speed, {y: (bkg.height) - 35});
				//companyBtn.gotoAndStop(2);
				setTimeout(showList, speed * 1001, 70);
				stage.dispatchEvent(new CEvent(CEvent.LOAD_PUBLIC_PRESENTATIONS, null))
					//Data.getInstance().loadPublicPresentations();
				
			}
			else if (e.currentTarget == driveBtn.bkg)
			{
				lastClicked = e.currentTarget.parent
				LeftMenu.filter = 'drive'
				TweenLite.to(publicBtn, speed, {y: 0});
				TweenLite.to(companyBtn, speed, {y: 35});
				TweenLite.to(driveBtn, speed, {y: 70});
				
				setTimeout(showList, speed * 1001, 105);
				Data.getInstance().loadFolders();
				Uploader.GroupID == 0
				//driveBtn.gotoAndStop(2);
			}
			
			
				
				lastClicked.p.mouseEnabled = false
				lastClicked.p.mouseChildren = false
				
			_list.Clear();
		}
		
		private function resetBtns():void
		{
			//driveBtn.gotoAndStop(1);
			//companyBtn.gotoAndStop(1);
			//publicBtn.gotoAndStop(1);
		
		}
		
		private function showList(e):void
		{
			_list.visible = true
			_list.y = e
			//_list.Resize(250, bkg.height - 105);
		
		}
		
		private function viewFolder(e:MouseEvent):void
		{
			mPop.visible = false;
			FolderList.HiglightedThumb.Select();
		
		}
		
		private function addAllSlidesToCurrentPresentation(e:MouseEvent):void
		{
			mPop.visible = false
			Data.getInstance().GetSlidesInFolder(FolderList.HiglightedThumb.OBJ.id, addDirectlyToPresentation)
		}
		
		private function addDirectlyToPresentation(e):void
		{
			stage.dispatchEvent(new CEvent(CEvent.ADD_THIS_FOLDER_TO_PRESENTATION, JSON.parse(e.target.data)));
		}
		
		private function hideButtons()
		{
			publicBtn.visible = false
			companyBtn.visible = false
			driveBtn.visible = false
		}
		
		private function showButtons()
		{
			publicBtn.visible = true
			companyBtn.visible = true
			driveBtn.visible = true
		}
		
		private function refreshTheFolder(e:Event):void
		{
			Data.getInstance().GetSlidesInFolder(FolderList.HiglightedThumb.OBJ.id)
		}
		
		private function fillListWIthPublicPresentation(e:CEvent):void
		{
			
			_list.Clear()
			
			if (LeftMenu.filter == '')
				return _list.Clear()
			
			var justPublic:Array = []
			if (LeftMenu.filter != 'public')
			{
				
				LeftMenu.filter = 'company'
				publicBtn.y = 0
				companyBtn.y = 35
				driveBtn.y = (bkg.height) - 35
				//publicBtn.gotoAndStop(2);
				_list.visible = true
				_list.y = 70
			}
			else
			{
				publicBtn.y = 0
				companyBtn.y = (bkg.height) - 70
				driveBtn.y = (bkg.height) - 35
				//publicBtn.gotoAndStop(2);
				//companyBtn.gotoAndStop(1);
				_list.visible = true
				_list.y = 35
				
			}
			
			for (var i:int = 0; i < e.data.length; i++)
			{
				if (e.data[i].access_level == LeftMenu.filter)
				{
					justPublic.push(e.data[i])
				}
			}
			
			_list.createThumbs(PresentationList, justPublic, true, 0, true)
			_list.x = 0;
		}
		
		private function fillListWIthFolders(e:CEvent):void
		{
			_list.Clear()
			
			_list.createThumbs(FolderList, e.data, true, 0, true)
			
			if (_selectIDX == 999 && _list.Thiles.length > 0)
			{
				
				_list.Thiles[_list.Thiles.length - 1].Select();
			}
		}
		
		private function uploadFolder(e:MouseEvent):void
		{
			OCP.uploadPop.Show(FolderList.HiglightedThumb.OBJ.title)
		}
		
		private function showFolderInfo(e:Event):void
		{
			
			if (!form)
				form = new FolderForm();
			
			hideButtons()
			mPop.visible = false
			form.x = 12
			form.y = 5
			addChild(form);
			driveBtn.visible = false
			form.presentationnameT.text = unescape(decodeURI(FolderList.HiglightedThumb.OBJ.title))
			stage.focus = form.presentationnameT;
			form.presentationnameT.setSelection(0, 199)
			form.descriptionT.addEventListener(FocusEvent.FOCUS_IN, selectT)
			form.sT.text = FolderList.HiglightedThumb.OBJ.slidesCount + '  ' + OCP.LG.filesinthisFolder;
			form.descriptionT.text = unescape(decodeURI(FolderList.HiglightedThumb.OBJ.description));
			form.saveBtn.addEventListener(MouseEvent.CLICK, updateFolderData)
			form.cancelBtn.addEventListener(MouseEvent.CLICK, cancelFolder)
			_list.visible = false
		}
		
		private function selectT(e:FocusEvent):void
		{
			form.descriptionT.setSelection(0, 199)
		}
		
		private function updateFolderData(e:MouseEvent):void
		{
			Data.getInstance().updateFolderData(FolderList.HiglightedThumb.OBJ.id, form.descriptionT.text, form.presentationnameT.text);
			_selectIDX = 444;
			cancelFolder(null)
		}
		
		private function showNewFolerForm(e:MouseEvent, title:String = 'name of folder', subTitle:String = 'sub title', desc:String = 'description'):void
		{
			
			form = new FolderForm();
			hideButtons()
			form.x = 12
			form.y = 5
			addChild(form);
			driveBtn.visible = false
			_list.visible = false
			
			form.presentationnameT.text = 'name of folder';
			form.descriptionT.text = String(OCP.LG.descriptionT)
			stage.focus = form.presentationnameT;
			form.presentationnameT.setSelection(0, 199)
			form.sT.text = subTitle + '  files in this folder';
			form.descriptionT.text = desc
			form.saveBtn.addEventListener(MouseEvent.CLICK, createTheFolder)
			form.cancelBtn.addEventListener(MouseEvent.CLICK, cancelFolder)
		
		}
		
		private function createTheFolder(e:MouseEvent):void
		{
			Data.getInstance().createFolder(form);
			
			_selectIDX = 999
			cancelFolder(null)
		}
		
		private function cancelFolder(e:MouseEvent):void
		{
			showButtons()
			driveBtn.visible = true
			_list.visible = true
			removeChild(form);
			form = null;
		
		}
		
		private function deletFolder(e:MouseEvent)
		{
			
			var delPop = new DeleteFolderPop(FolderList.HiglightedThumb.OBJ.id, OCP.LG.doyouWantToDeleteFolderT)
			stage.addChild(delPop)
			mPop.visible = false
		}
		
		private function showMiniPop(e:CEvent)
		{
			
			mPop.visible = true
			mPop.x = 250.15
			mPop.y = e.data.localToGlobal(new Point(0, e.data.settingsBtn.y + 0 + e.data.settingsBtn.height / 2)).y - this.y
		}
		
		public function Resize()
		{
			bkg.height = stage.stageHeight - this.y
			
			var listHeight = bkg.height - 105
			if (Login.isLogedIn == 1)
			{
				listHeight = bkg.height - 37
			}
			
			_list.Resize(250, listHeight)
			
			if (LeftMenu.filter == 'public')
			{
				publicBtn.y = 0
				companyBtn.y = (bkg.height - 35) - 35
				driveBtn.y = (bkg.height - 35)
			}
			else if (LeftMenu.filter == 'company')
			{
				publicBtn.y = 0
				companyBtn.y = 35
				driveBtn.y = (bkg.height - 35)
			}
			else if (LeftMenu.filter == 'drive')
			{
				publicBtn.y = 0
				companyBtn.y = 35
				driveBtn.y = 70
			}
		}
	}

}