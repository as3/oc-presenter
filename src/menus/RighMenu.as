package menus
{
	
	import com.hurlant.util.Base64;
	import controllers.PresentationController;
	import data.Data;
	import data.Login;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import player.PresentationPlayer;
	import popups.DeletePop;
	import popups.DeletePresentationPop;
	import popups.InfoPresentationForm;
	import popups.Login;
	import popups.MoreMyPresentation;
	import popups.PDFPop;
	import popups.PopUpTimer;
	import popups.SharedPop;
	import popups.SharedStartPop;
	import thumbs.MYPresentationList;
	import views.ListView;
	
	public class RighMenu extends rightMenuLib
	{
		public static var SelectedPresentation:Object
		private var lastIndex:int = -1;
		private var listGrid:ListView;
		private var form:InfoPresentationForm = new InfoPresentationForm();
		private var mPop:MoreMyPresentation = new MoreMyPresentation()
		public function RighMenu() {
			addEventListener(Event.ADDED_TO_STAGE, init)	
		}
	
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(CEvent.SHOWING_ALL_PRESENTATIONS, showLogedInMenu);
			stage.addEventListener(CEvent.SHOWING_ALL_PRESENTATIONS, fillList);
			stage.addEventListener(CEvent.LOGED_IN, showLogedInMenu);
			stage.addEventListener(CEvent.LOGED_OUT, logedOut);
			stage.addEventListener(CEvent.LOGED_IN, onLogedIN);
			stage.addEventListener(CEvent.LOAD_MY_PRESENTATIONS_SLIDES, setCurrentPresentation);
			stage.addEventListener(CEvent.SHOW_DUPLICATE_FORM , showFormThatDuplicates)
			stage.addEventListener(CEvent.SHOW_PRESENTATION_INFO, showPublicInfo)
			this.tabEnabled = false;
			form.visible = false
			form.addEventListener(CEvent.HIDE_FORM, showLogedInMenu)
			myPresentations.visible = false
			myPresentations.newBtn.addEventListener(MouseEvent.CLICK, showNewForm)
			listGrid = new ListView(0, 1, true, 'vertical', 1, true, 'top', true);
			listGrid.x = 0
			listGrid.y = 37 
			listGrid.visible = false;
			addChild(listGrid);
			this.addChild(form);
			form.x = 13.15
			form.y = 13.15
			myPresentations.bkg.addEventListener(MouseEvent.CLICK, loadMyPresentations)
			
			stage.addEventListener(CEvent.SHOW_MINI_POP_RIGHT, showMiniPop)

			mPop.visible = false
			addChild(mPop);
			
			
			
			
		}
		
		
		
		
		
		
		
		
			
	
		
	//========================================== CODE SDOASJDOASHD
		private function showMiniPop(e:CEvent)
		{
		
			mPop.visible = true
			mPop.x = 222.35
			mPop.y = e.data.localToGlobal(new Point(0, e.data.settingsBtn.y + 0 + e.data.settingsBtn.height / 2)).y - this.y
		}
		
		
		private function onLogedIN(e:CEvent):void 
		{
			if (!PresentationPlayer.isPresenting) {
				Data.getInstance().loadMyPresentations(null);
				}
		}
		
		private function loadMyPresentations(e:MouseEvent):void 
		{
			Data.getInstance().loadMyPresentations(null)
		}
		
		private function showNewForm(e:MouseEvent):void
		{
			stage.dispatchEvent(new CEvent(CEvent.CLEAR_TIMELINE, OCP.LG.newPresentationT))
			
			var o:Object = new Object()
			o.title = OCP.LG.newPresentationT
				
			o.description = OCP.LG.decriptionT 
			o.slide_ids = [];
			MYPresentationList.HiglightedThumb = null
			lastIndex = -1
			listGrid.visible = false
			myPresentations.visible = false
			
			
			trace("SDASDA", Login.Name)
			//unescape(decodeURI(e.title))
			//unescape(decodeURI(e.description))
			form.Show(-1, OCP.LG.newPresentationT, OCP.LG.decriptionT, Login.Name, true, 'private', 0);
			
			
		}
		private function showPublicInfo(e:CEvent):void
		{	  
			var owner:String = Login.Name
			var shwoPRivacy:Boolean = true
			if (e.data.user && e.data.user.full_name) {
				owner = e.data.user.full_name
				shwoPRivacy = false
				}
			if (Login.SuperUser) {
				shwoPRivacy = true
			}
			trace(99, e.data.slide_ids)
			form.Show(e.data.instance_id, unescape(decodeURI( e.data.title)),  unescape(decodeURI( e.data.description)), 
						owner , shwoPRivacy  ,  e.data.access_level, e.data.slide_ids.length)
			listGrid.visible = false
			myPresentations.visible = false
		}	
		private function showFormThatDuplicates(e:CEvent):void 
		{
			lastIndex = -1
			form.Show(-1, unescape(decodeURI( e.data.title)), unescape(decodeURI( e.data.description)),
			          Login.Name, true, 'public', e.data.thiles.length);
			listGrid.visible = false
			myPresentations.visible = false	
		}
		private function setCurrentPresentation(e:Event):void
		{
			if (MYPresentationList.HiglightedThumb) {
				lastIndex = MYPresentationList.HiglightedThumb.OBJ.instance_id
				}
		}

		private function fillList(e:CEvent):void
		{
			myPresentations.visible = true
			listGrid.Clear()
			listGrid.createThumbs(MYPresentationList, e.data, true, 0, true)
			listGrid.Resize(250 ,  bkg.height-listGrid.y);
			if(SharedStartPop.isVisible) return
			if (lastIndex > -1)
			{
				
				for (var i:int = 0; i <listGrid.Thiles.length ; i++) 
				{
					var t = listGrid.Thiles[i]
					if (t.OBJ.instance_id == lastIndex ) {
						t.Select();
						return
						}
				}	
			}
			if (lastIndex == -1 && listGrid.Thiles.length > 0)
			{
				MYPresentationList(listGrid.Thiles[listGrid.Thiles.length-1]).Select()
			}	
		}

		private function showLogedInMenu(e:CEvent):void
		{
			if (Login.isLogedIn == 2)
			{
				listGrid.visible = true
				myPresentations.visible = true		
			}
			form.visible = false
		}
		
		private function logedOut(e:Event):void
		{
			listGrid.visible = false
			listGrid.Clear()
			myPresentations.visible = false
		}

	
		public function Resize()
		{
			bkg.height = stage.stageHeight - this.y
			form.y = 0
			listGrid.Resize(250 ,  bkg.height - listGrid.y);
		}

	}

}