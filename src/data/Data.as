package data
{
	
	import com.adobe.crypto.MD5;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import controllers.PresentationController;
	import fl.controls.RadioButton;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import popups.DeleteFilePop;
	import popups.DeleteFolderPop;
	import popups.LogInPop;
	import popups.MessageBox;
	import thumbs.MYPresentationList;
	
	public class Data
	{
		private var _login:Login;
		private var _s:Stage;
		private var PRELOADER:LoaderMax;
		private var urlLoader:URLLoader = new URLLoader()
		private var _loaders:Array = [];
		private var me:MessageBox;
		private var allListeners:Array = [];
		private static var _instance:Data;
		private var _cicrleLoader:CircleLoader2;
		public static var preloadedOBJ:Object;
		var max:LoaderMax;
		
		public function Data(s:Stage)
		{
			if (_instance)
			{
				throw new Error("Singleton... use getInstance()");
			}
			_instance = this;
			max = new LoaderMax({name: "mainQueue", maxconnections: 2, onOpen: ONOPEN, onComplete: ONCOMPLETE});
			_s = s;
			_s.addEventListener(CEvent.SAVE_A_COPY, saveCopyPresentation);
			_s.addEventListener(CEvent.DELETE_CURRENT_PRESENTATION, deleteCurrentPresentation);
			_s.addEventListener(CEvent.UPDATA_FOLDER_DATA, updateFolderData)
			_s.addEventListener(CEvent.DELETE_CURRENT_FOLDER, confirmedDeletFolder);
			_s.addEventListener(CEvent.DELETE_FILE, deleteFile);
			_s.addEventListener(CEvent.LOAD_PUBLIC_PRESENTATIONS, loadPublicPresentations)
			_cicrleLoader = new CircleLoader2()
			_s.addChild(_cicrleLoader)
			_cicrleLoader.visible = false
			getLanguages()
		}
		
		private function getLanguages():void
		{
			
			var r:URLRequest = new URLRequest(Globals.servicURL + '?what=get_multilanguage_labels&lang=' + _s.loaderInfo.parameters.lang)
			loadThis(r, storeLanguageWords)
		}
		
		private function storeLanguageWords(e):void
		{
			OCP.LG = JSON.parse(e.target.data).values
			_login = new Login(_s);
			_s.dispatchEvent(new CEvent(CEvent.LANGUAGE_HAS_ARIVED, null))
		}
		
		public function ONOPEN(e:Event):void
		{
			var d = DataLoader(e.target.getChildren()[0])
		
		}
		
		public function ONCOMPLETE(e:Event):void
		{
			trace("DONE", e.target.getChildren().length);
			var d:DataLoader = DataLoader(e.target.getChildren()[0])
			max.remove(d)
			d.dispose(true)
		}
		
		private function onLoad(e:Event):void
		{
			trace("Max Complete")
		}
		
		public function loadPublicPresentations(e = null):void
		{
			var r:URLRequest = new URLRequest(Globals.servicURL + '?what=get_public_presentations' + OCP.R)
			trace(323423 , r.url);
			loadThis(r, recivedPublic, true)
		}
		
		public function loadThis(r:URLRequest, e:Function, killAll:Boolean = true)
		{
			_cicrleLoader.x = _s.mouseX
			_cicrleLoader.y = _s.mouseY
			_s.addEventListener(Event.ENTER_FRAME, moveCircle)
			if (killAll)
			{
				while (allListeners.length > 0)
				{
					var remov = _loaders.pop()
					remov[0].removeEventListener(Event.COMPLETE, remov[1])
					remov[0].removeEventListener(Event.COMPLETE, removeMouse)
					remov[0].close()
					remov[0] = null
				}
				
			}
			_cicrleLoader.visible = true
			var u:URLLoader = new URLLoader()
			u.addEventListener(Event.COMPLETE, e);
			u.addEventListener(Event.COMPLETE, removeMouse);
			r.method = URLRequestMethod.POST
			_loaders.push([u, e]);
			
			u.load(r);
		}
		
		private function removeMouse(e:Event):void
		{
			_cicrleLoader.visible = false
			_s.removeEventListener(Event.ENTER_FRAME, moveCircle)
		}
		
		private function moveCircle(e:Event):void
		{
			_cicrleLoader.rotation += 4
			_cicrleLoader.x = _s.mouseX + 19
			_cicrleLoader.y = _s.mouseY
		}
		
		public function recivedPublic(e:Event):void
		{
			
			var o:Object = JSON.parse(e.target.data)
			if (o.public_presentations)
			{
				_s.dispatchEvent(new CEvent(CEvent.HERE_ARE_THE_PUBLIC_PRESENTATIONS, o.public_presentations))
			}
		}
		
		public function deleteFile(e:CEvent):void
		{
			var r:URLRequest = new URLRequest()
			var urlVar:URLVariables = new URLVariables()
			urlVar.group_slide_id = e.data[0]
			if ( e.data[1]) {
				urlVar.deletion_is_confirmed = 1
				}
			r.method = URLRequestMethod.POST
			r.url = Globals.servicURL + '?what=delete_group_or_slide'
			r.data = urlVar;
			loadThis(r, fileDeleted)
		}
		
		
		public function fileDeleted(e:Event):void
		{
		
			var dr:Object = JSON.parse(e.target.data)
			
			if (dr.status && dr.status == 'error') {
				var s:String =  "This file is used in " + dr.presentations_have_this_slide.length + ' presentations'
				var dp2:DeleteFilePop = new DeleteFilePop( dr.group_slide_id, s, true);
				_s.addChild(dp2)
				
			}
				
			else {
				_s.dispatchEvent(new Event(CEvent.REFRESH_CURRENT_FOLDER))
			}
				
				
			
		}
		
		
		public function confirmedDeletFolder(e:CEvent):void
		{
			var r:URLRequest = new URLRequest(Globals.servicURL + '?what=delete_group_or_slide')
			var urlVar:URLVariables = new URLVariables()
			urlVar['group_slide_id'] = e.data[0]
			if ( e.data[1]) {
				urlVar.deletion_is_confirmed = 1
				}
			r.method = URLRequestMethod.POST
			r.data = urlVar;
			loadThis(r, showDeleeteResponce)
		
		}
		
		public function showDeleeteResponce(e:Event):void
		{
			var dr:Object = JSON.parse(e.target.data)
			if (dr.status && dr.status == 'error') {
				var s:String =  "Some files in this folder are used inside" + dr.presentations_have_this_slide.length + ' presentation' +  dr.presentations_have_this_slide.length > 1? "s":"" 
				var dp2:DeleteFolderPop = new DeleteFolderPop( dr.group_slide_id, s, true);
				_s.addChild(dp2)
				
			}
				
			else {
				loadFolders()
				
			}
			
			
		}
		
		public function saveNewPositioning(groupID:int, idsOreder:String):void
		{
			
			var u:URLVariables = new URLVariables()
			u.group_id = groupID
			u.slides_order_ids = idsOreder
			var r:URLRequest = new URLRequest(Globals.servicURL + '?what=group_rearange' + OCP.R)
			r.data = u;
			loadThis(r, filesRearanged, true)
		
		}
		
		public function filesRearanged(e:Event):void
		{
			
			_s.dispatchEvent(new CEvent(CEvent.REFRESH_CURRENT_FOLDER, null))
		}
		
		public function createFolder(e = null):void
		{
			
			var r:URLRequest = new URLRequest(Globals.servicURL + "?what=create_slide_group")
			var urlVariable:URLVariables = new URLVariables()
			urlVariable.group_name = encodeURI(e.presentationnameT.text)
			urlVariable.group_subtitle = "subtitle"
			urlVariable.group_description = encodeURI(e.descriptionT.text)
			r.data = urlVariable
			loadThis(r, folderCreated)
		
		}
		
		public function folderCreated(e:Event):void
		{
			loadFolders()
		}
		
		public function updateFolderData(GroupID:int, desc:String, title:String):void
		{
			
			jsonLoader = new URLLoader()
			var r:URLRequest = new URLRequest(Globals.servicURL + "?what=group_edit")
			var urlVariable:URLVariables = new URLVariables()
			urlVariable.group_id = GroupID
			urlVariable.group_name = encodeURI(title)
			urlVariable.group_subtitle = "subtitle"
			urlVariable.group_description = encodeURI(desc)
			r.method = URLRequestMethod.POST
			r.data = urlVariable
			loadThis(r, folderUpdated)
		
		}
		
		public function folderUpdated(e:Event):void
		{
			loadFolders()
		}
		
		public function loadFolders(e = null):void
		{
			var r:URLRequest = new URLRequest(Globals.servicURL + "?what=get_slide_groups" + Globals.LANG + OCP.R)
			loadThis(r, foldersRecived)
		
		}
		
		public function foldersRecived(e:Event):void
		{
			trace("Here is your tree:" , e.target.data);
			var jsonOBJ = JSON.parse(e.target.data)
			_s.dispatchEvent(new CEvent(CEvent.FOLDERS_RECIVED, jsonOBJ.groups))
		}
		
		public function updateSLide(e:Object)
		{
			var type:String = 'create_txtimg_slide'
			var v:URLVariables = new URLVariables()
			v.session_id = LogInPop.session_id
			v.session_hash = MD5.hash(LogInPop.session_id + "R$TFGs2ab!ax")
			v["slide_name"] = encodeURI(e.numSlides.text)
			v["group_id"] = Uploader.GroupID
			v["slide_id"] = e.id
			var r:URLRequest = new URLRequest();
			r.data = v
			r.url = Globals.servicURL + '?what=' + type
			r.method = URLRequestMethod.POST
			trace("CALLING UPATE", r.url + v.toString())
			loadThis(r, slideUpdated)
		}
		
		private function slideUpdated(e):void
		{
			trace("Update this slide [DONE]", e.target.data)
		}
		
		public static function getInstance():Data
		{
			return _instance;
		}
		
		public function GetSlidesInFolder(id:int, handler:Function = null):void
		{
			if (!handler)
			{
				handler = parceFolderJSON
			}
			var s:String = Globals.servicURL + '?what=get_slides_by_groups' + OCP.R
			var r:URLRequest = new URLRequest(s)
			r.method = URLRequestMethod.POST
			var variables:URLVariables = new URLVariables()
			variables['group_id[]'] = id
			r.data = variables
			loadThis(r, handler)
		
		}
		
		public function parceFolderJSON(e:Event):void
		{
			var jsonOBJ:Object = JSON.parse(e.target.data)
			for (var i:int = 0; i < jsonOBJ.groups[0].pages.length; i++)
			{
				jsonOBJ.groups[0].pages[i].thumb_image = jsonOBJ.thumbs_path + jsonOBJ.groups[0].pages[i].thumb_image
			}
			if (jsonOBJ.groups && jsonOBJ.groups.length > 0)
			{
				_s.dispatchEvent(new CEvent(CEvent.SHOWING_FLODER_CONTENTS, jsonOBJ))
				
			}
		}
		
		public function viewFilesInFolder(e:CEvent):void
		{
			var s:String = Globals.servicURL + '?what=get_slides_by_groups' + OCP.R
			var r:URLRequest = new URLRequest(s)
			r.method = URLRequestMethod.POST
			var variables:URLVariables = new URLVariables()
			variables['group_id[]'] = e.data.id
			LasLoadedGroup = e.data.id
			r.data = variables
			loadThis(r, showFiles)
			
			t.text = e.data.title
			currentFolder = e.data;
			this.sc.text = e.data.slidesCount
			this.sc2.text = 'slides'
			if (e.data.slidesCount == 1)
			{
				sc2.text = 'slide'
			}
			showSubMenu()
			
			if (e.data.position > -1)
			{
				_s.dispatchEvent(new CEvent(CEvent.SHOWING_FLODER_CONTENTS, e.data.position))
				
			}
			SetTitles(null, e.data.title)
		
		}
		
		public function changeParentPage(id1:int, parent:int, f:Function = null)
		{
			
			var handler = changeParentPageComplete
			if (f)
			{
				handler = f
			}
			var s:String = Globals.servicURL + '?what=change_parent_page' + OCP.R
			var r:URLRequest = new URLRequest(s)
			r.method = URLRequestMethod.POST
			var variables:URLVariables = new URLVariables()
			variables['page_id'] = id1
			variables['new_parent_page_id'] = parent
			r.data = variables
			loadThis(r, handler)
		
		}
		
		private function changeParentPageComplete(e:Event):void
		{
			loadFolders()
		}
		
		public function showFiles(e:Event):void
		{
			
			jsonLoader.removeEventListener(Event.COMPLETE, showFiles)
			jsonOBJ = JSON.parse(e.target.data)
			var allGroupsIDs:Array = []
			_s.dispatchEvent(new CEvent(CEvent.HERE_ARE_THE_FILES_IN_THE_FOLDER, jsonOBJ.groups[0].pages))
		}
		
		
		
		private function saveCopyPresentation(e:CEvent):void
		{
			
			var r:URLRequest = new URLRequest(Globals.servicURL + '?what=user_update_timeline' + OCP.R)
			r.method = URLRequestMethod.POST;
			var v:URLVariables = new URLVariables()
			v.t_id = "-1"
			v.t_title = encodeURI(e.data.presentationnameT.text)
			v.slide_ids_csv = e.data.thiles;
			r.data = v;
			loadThis(r, newTimelineComplete);
		}
		
		private function saveCurrentPresentation(e:CEvent):void
		{
			
			var r:URLRequest = new URLRequest(Globals.servicURL + '?what=user_update_timeline' + OCP.R)
			var v:URLVariables = new URLVariables()
			v.t_id = e.data.instance_id
			v.t_title = encodeURI(e.data.title)
			v.t_description = encodeURI(e.data.description);
			v.access_level = e.data.access_level
			v.slide_ids_csv = e.thiles
			
			r.data = v;
			
			loadThis(r, saveCurrentPresentationComplete)
		}
		
		public function SavePresentation(id:int, title:String, description:String, thilsCSV:String, accesLevel = null)
		{
			
			var r:URLRequest = new URLRequest(Globals.servicURL + '?what=user_update_timeline')
			r.method = URLRequestMethod.POST;
			
			var v:URLVariables = new URLVariables()
			v.t_id = id
			v.t_title = encodeURI(title)
			v.t_description = encodeURI(description);
			v.slide_ids_csv = thilsCSV;
			v.access_level = accesLevel
			r.data = v;
			loadThis(r, saveCurrentPresentationComplete)
		}
		
		public function newTimelineComplete(e:Event):void
		{
			loadMyPresentations(null)
		
		}
		
		public function saveCurrentPresentationComplete(e:Event):void
		{
			loadMyPresentations(null)
		}
		
		public function loadMyPresentations(object:Object):void
		{
			var r:URLRequest = new URLRequest(Globals.servicURL + "?what=user_get_timelines" + OCP.R)
			loadThis(r, parceUserPresentationsJSON, false)
		
		}
		
		public function parceUserPresentationsJSON(e:Event):void
		{
			var o:Object = JSON.parse(e.target.data)
			var sortOnID:Array = o.timelines.sortOn("instance_id")
			if (o.status == 'success')
			{
				_s.dispatchEvent(new CEvent(CEvent.SHOWING_ALL_PRESENTATIONS, sortOnID))
			}
		
		}
		
		public function loadAndPlayTimelineID(e)
		{
			var r:URLRequest = new URLRequest(Globals.servicURL + "?what=user_get_shared_timeline"+ " " )
			var variable:URLVariables = new URLVariables()
			variable.instance_id = e;
			r.data = variable
			loadThis(r, presentationLoaded)
		}
		
		public function presentationLoaded(e:Event):void
		{
			var o:Object = JSON.parse(e.target.data)
			if (o.status == 'success')
			{
				_s.dispatchEvent(new CEvent(CEvent.PLAY_THIS_PRESENTATION, [o.timeline.slide_ids, 0, o.timeline.title]));
			}
			else
			{
				_s.dispatchEvent(new CEvent(CEvent.SHOW_EDIT_VIEW, null));
			}
		}
		
		public function deleteCurrentPresentation(e:Event):void
		{
			trace("S")
			var r:URLRequest = new URLRequest(Globals.servicURL + '?what=user_delete_timeline&t_id=' + MYPresentationList.HiglightedThumb.OBJ.instance_id)
			loadThis(r, showDeleteResponce)
		}
		
		public function showDeleteResponce(e:Event):void
		{
			loadMyPresentations(null)
		}
		
		public function LoadSlide(id:int):void
		{
			var s:String = Globals.servicURL + "?what=get_slides" + "&slide_id[]=" + id + OCP.R
			var r:URLRequest = new URLRequest(s)
			loadThis(r, slideIsHere, true)
		}
		
		public function slideIsHere(e:Event):void
		{
			
			var s:Object = JSON.parse(e.target.data)
			_s.dispatchEvent(new CEvent(CEvent.SHOW_SLIDE, s.pages[0]));
		}
		
		public function PreloadCurrentPResentation(e:Function, progress:Function, slideThumbs:Array  , startIDX:int = 0)
		{
			
			if (PRELOADER)
			{
				PRELOADER.unload();
			}
			PRELOADER = new LoaderMax({name: "mainQueue", onProgress: progress, onComplete: e})
			var slides = ''
			var r:URLRequest = new URLRequest()
			var howMany:int = slideThumbs.length > 45 ? 45:slideThumbs.length
			
			for (var i:int = 0; i < howMany; i++)
			{
				slides += "&slide_id[]=" + slideThumbs[i].id  
			}
			
			r.method = URLRequestMethod.POST
			r.url = Globals.servicURL + "?what=get_slides" + slides  + OCP.R
			loadThis(r, preloadedSlide)
		
		}
		
		private function handelPreloadError(e = null):void
		{
			trace("Preload error")
		}
		
		
		public function getImage(id:int, progresFunction:Function , FinishFunction:Function) {
			
			if (PRELOADER && PRELOADER.getLoader("BIG" + id )) {
				var imgL:ImageLoader = PRELOADER.getLoader("BIG" + id )
				imgL.prioritize(true);
				PRELOADER.pause()
				}
			
			}
			
		public function ResumePreload():void 
		{
			if(PRELOADER) {PRELOADER.resume()}
			
		}
			
		private function preloadedSlide(e):void
		{
			var o:Object = JSON.parse(e.target.data)
			Data.preloadedOBJ  = o
			PRELOADER.autoDispose = false
			PRELOADER.maxConnections = 1;
		
			for (var i:int = 0; i < o.pages.length; i++)
			{
				
				var r:URLRequest = new URLRequest()
				if (o.pages[i].content.bg_image)
				{
					r.url = Globals.baseURL + 'resources/files/images/' + o.pages[i].content.bg_image //+ OCP.R		
					trace(3432 , r.url);
				}
				else if (o.pages[i].type == "slideVideo")
				{
					
					r.url = Globals.baseURL + o.pages[i].thumb_image
					
				}
				PRELOADER.append(new ImageLoader(r, {name: "BIG" + o.pages[i].id, onError: handelPreloadError, onComplete: imagePreloaded}));
			}
			PRELOADER.load(true);
		}
		
		private function showProgress(e:LoaderEvent):void
		{
			
		}
		
		public function imagePreloaded(e):void
		{
			
			_s.dispatchEvent(new CEvent(CEvent.SLIDE_PRELOADED , ImageLoader(e.target).name))
		}
		
		
	
	}

}

