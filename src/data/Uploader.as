﻿package data
{
	import co.uk.mikestead.net.URLRequestBuilder;
	import com.adobe.crypto.MD5;
	import fl.controls.List;
	import flash.display.Stage;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.FileReferenceList;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.setTimeout;
	import popups.LogInPop;
	import views.ListView;
	
	
	public class Uploader extends EventDispatcher
	{
		
	
		private var _fileFilter:FileFilter;
		private var uploadedImages:Number = 0;
		private var localloadedImages:Number = 0;
		private var localBitmaps:Array = [];
		private var retry:int = 0;
		private var maxRetry:int = 2;
		private var totalFileSize:Number;
		private static var _instance:Uploader;
		private var _list:ListView;
		private var currentUploadItem:FileReference;
		public static var localOBJ:Object = new Object()
		public static var GroupID:int = 0;
		private var _s:Stage;
		
		
		public function Uploader(s:Stage) {   if(_instance){throw new Error("Singleton... use getInstance()");} 
		
			_instance = this;
			_s = s;

		}
		
		public static function getInstance():Uploader { return _instance;	}

		public function cancelPendingUploads(e:Event):void
		{
			if (currentUploadItem) {
				currentUploadItem.cancel();
				currentUploadItem = null
				uploadedImages = -1
				}
			
		}
		
		public function UploadList(list:ListView):void 
		{
			uploadedImages = 0
			localloadedImages = 0
			localBitmaps = []
			_list = list
			trace(3423 , _list)
			trace(31312 , _list.Thiles[uploadedImages].OBJ)
			var firstItem:FileReference = FileReference(_list.Thiles[uploadedImages].OBJ.data)
			
			uploadFileReference(firstItem);
		}
		
		
		public function uploadFileReference(f:FileReference)
		{
			
		
			var extension:String = f.name.substring(f.name.length - 4, f.name.length).toLocaleLowerCase();
			var type:String = 'create_video_slide'
			if (extension == ".jpg" || extension == ".png" || extension == "jpeg")
			{
				type = 'create_txtimg_slide'
			}
			
			var v:URLVariables = new URLVariables()
			v.session_id = LogInPop.session_id
			v.session_hash = MD5.hash(LogInPop.session_id + "R$TFGs2ab!ax")
			v["slide_name"] = encodeURI(f.name.toLocaleLowerCase())
			trace('2//2///2///2///2////' , v["slide_name"])
			v["group_id"] = Uploader.GroupID
			
			var request:URLRequest = new URLRequestBuilder(v).build();
			request.data = v
			request.url = Globals.servicURL + '?what=' + type
			request.method = URLRequestMethod.POST
			trace("Sesion id", request.url, request.data.toString())
			currentUploadItem = f;
			f.addEventListener(Event.COMPLETE, onUploadComplete)
			f.addEventListener(ProgressEvent.PROGRESS, imageUploadPRogress)
			f.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, fileUploadedResponse);
			f.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			
			//var cf:FileReference = new FileReference();
			//cf.data = '22'
			if (type == 'create_txtimg_slide')
			{
				f.upload(request, 'image')
			}
			else
			{
				f.upload(request, 'video')
			}
		}
		
		
		
		public function updateFileReference(f:FileReference, id:int = 0)
		{
			
		
			var extension:String = f.name.substring(f.name.length - 4, f.name.length).toLocaleLowerCase();
			var type:String = 'create_video_slide'
			if (extension == ".jpg" || extension == ".png" || extension == "jpeg")
			{
				type = 'create_txtimg_slide'
			}
			
			var v:URLVariables = new URLVariables()
			v.session_id = LogInPop.session_id
			v.session_hash = MD5.hash(LogInPop.session_id + "R$TFGs2ab!ax")
			v["slide_name"] = encodeURI(f.name.toLocaleLowerCase())
			trace('///////////////' , v["slide_name"])
			v["group_id"] = Uploader.GroupID
			if (id > 0) {
				v["slide_id"] = id
				
				trace("WE HAVE ID", id)
				}
			var request:URLRequest = new URLRequestBuilder(v).build();
			request.data = v
			request.url = Globals.servicURL + '?what=' + type
			request.method = URLRequestMethod.POST
			trace("Sesion id", request.url, request.data.toString())
			currentUploadItem = f;
			f.addEventListener(Event.COMPLETE, onUploadComplete)
			//f.addEventListener(ProgressEvent.PROGRESS, imageUploadPRogress)
			f.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, fileUpdateResponse);
			f.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			if (type == 'create_txtimg_slide')
			{
				f.upload(request, 'image')
			}
			else
			{
				f.upload(request, 'video')
			}
		}
		
		private function fileUpdateResponse(e:DataEvent):void 
		{
				var response:XML = XML(e.data);
			var j:Object = JSON.parse(response.toString())
			if (j.status == "success")
			{
				trace("File updatet succesfuly", response.toString())
			}
			_s.dispatchEvent(new Event(CEvent.ALL_IMAGES_UPLOADED))
			
			trace(123)
		}
		
		public function fileUploadedResponse(e:DataEvent):void
		{
			//_list.Thiles(uploadedImages).label = '-- ' + List(_list).getItemAt(uploadedImages).label
			_list.Thiles[uploadedImages].check.visible = true
			//_list.invalidate();
			trace("UPLOAD RESPONCE", e.data);
			//UPLOAD RESPONCE {"status":"success","message":"no_title_or_image"}
			var response:XML = XML(e.data);
			var j:Object = JSON.parse(response.toString())
			if (j.status == "success")
			{
				uploadedImages++
				retry = 0
				
				if (uploadedImages < _list.Thiles.length)
				{
					uploadFileReference(FileReference(_list.Thiles[uploadedImages].OBJ.data))
				}
				else
				{
					_s.dispatchEvent(new Event(CEvent.ALL_IMAGES_UPLOADED))
					
				}
			}
			else if (retry > maxRetry)
			{
				
				retry++
				setTimeout(uploadFileReference, 250, FileReference(_list.Thiles[uploadedImages].OBJ.data));
			}
			else
			{
				retry = 0
				uploadedImages++
				
				if (uploadedImages < _list.Thiles.length)
				{
					uploadFileReference(FileReference(_list.Thiles[uploadedImages].OBJ.data))
				}
				
				else
				{
					trace("ELSE")
					//box.browseBtn.visible = true;
					
				}
			}
		
		}
		
		public function onIOError(e:IOErrorEvent):void{trace("IOERROR", e)}
		public function onComplete(e:Event):void{trace("Complete")}
		
		public function uploadCOmplet(e:Event):void
		{
			
			trace("++++++++++++++++Uload Thumb complet");
		}
		
		public function onUploadComplete(e):void
		{
			trace("Done uploading imga", e.target.data);
			e.target.removeEventListener(Event.COMPLETE, onUploadComplete)
			e.target.removeEventListener(ProgressEvent.PROGRESS, imageUploadPRogress)
		
		}
		
		public function imageUploadPRogress(e:ProgressEvent):void
		{
			var percentLoaded:Number = e.bytesLoaded / e.bytesTotal * 100;
			trace(percentLoaded, uploadedImages, "PercentLoaded")
			if (_list && uploadedImages>-1 && _list.Thiles[uploadedImages]) {
				trace("DIS{")
			_s.dispatchEvent(new CEvent(CEvent.UPOADING, 'Uploading : ' + FileReference(_list.Thiles[uploadedImages].OBJ.data).name + ' ' + Math.round(percentLoaded) + "%" ))
			}
		}
		
		
	
	}

}

