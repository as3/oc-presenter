package  data
{
	import com.adobe.crypto.MD5;
	import com.adobe.webapis.URLLoaderBase;
	import flash.display._s;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.TimerEvent;
	import flash.net.navigateToURL;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.text.TextField;
	import flash.utils.setTimeout;
	import flash.utils.Timer;
	import mx.core.Singleton;

	public class Login extends EventDispatcher
	{
		
		public static var SuperUser:Boolean = false;
		public static var isLogedIn:int = 1;
		public static var session_id:String = '';
		public static var LoggedOBJ:Object;
		static private var _f:Function;
		public var timer:Timer;
		public static var Name:String = '';
		private var checker:URLLoader = new URLLoader();
		private var jsonLOader:URLLoader = new URLLoader();
		private var loader:URLLoader = new URLLoader();
		private var googlURLLoader:URLLoader = new URLLoader();
		private var _s:Stage 
		private var _user:String;
		private var _password:String;
		private static var _instance:Login;
		static private var googleOBJ:Object;
		static private var gotoLink:String;
		
		public function Login(s:Stage) {   if(_instance){throw new Error("Singleton... use getInstance()");} 
			_instance = this;
			_s = s;
			
			checkLogedIn()
		}
		public static function getInstance():Login{return _instance;	}	
		private function checkLogedIn() {
		
				var r:URLRequest = new URLRequest(Globals.servicURL + "?what=user_profile_get" + OCP.R)
					r.method = URLRequestMethod.POST;
					loader.addEventListener(Event.COMPLETE, checkedSession);
					loader.load(r)
					trace("CALL", r.url)
					
			}
		
		private function checkedSession(e:Event):void 
			{
				loader.removeEventListener(Event.COMPLETE, checkedSession);
				var o:Object = new Object();
				//trace("#$#:", e.target.data)
				o = JSON.parse(e.target.data);
				if (o.status == 'success') {
					showLogInResult(e)
					}
				else {
					showLogInResult(e)
					}
				
		}
			
		public  function LogUserIn(user:String , password:String, keep:Boolean = false):void 
		{
		
			var r:URLRequest 
			if( Login.isLogedIn == 1) {
				
					r = new URLRequest(Globals.servicURL + "?what=user_login");
					r.method = URLRequestMethod.POST;
					var uVars:URLVariables = new URLVariables ()
						uVars.username = user
						uVars.remain_logged_in = keep
						_user = user
						uVars.password_md5 =  MD5.hash(password);
						_password = password
					r.data = uVars;
					loader.addEventListener(Event.COMPLETE, showLogInResult);
					trace("SENT , ",keep)
					loader.load(r)
				
			}
		}
		
		private function showLogInResult(e:Event):void 
		{
		
				loader.removeEventListener(Event.COMPLETE , showLogInResult)
			var responce:Object = JSON.parse( loader.data)

		if (responce && responce.status == "success") {
				Login.isLogedIn = 2;
			
				Login.session_id = responce.user_data.session_id
				Login.LoggedOBJ = responce;
				Login.SuperUser = responce['user_data'].superuser == 1
				Login.Name = responce.user_data.full_name
				_s.dispatchEvent(new CEvent(CEvent.LOGED_IN,responce['user_data']))// responce['user_data']['full_name'] ));
				
					
					timer =  new Timer(100000, 0 );
					timer.addEventListener(TimerEvent.TIMER , refresh)
					timer.start()
				
				}
			else {	
				_s.dispatchEvent(new CEvent(CEvent.LOG_IN_FAIL, null));	
				}	
				
				
		}
		
		public static  function callGoogleLogIn(e=null):void {
	
				navigateToURL(new URLRequest(gotoLink), "_self")
				return
				//
				var r:URLRequest = new URLRequest(Globals.servicURL + "?what=google_login&cache=nui");
					r.method = URLRequestMethod.POST;
					var uVars:URLVariables = new URLVariables ()
			
					var loader:URLLoader = new URLLoader();
				 
					loader.addEventListener(Event.COMPLETE, hersTheLink);
					loader.load(r)
					
			}
			
			
		public function getGoogleLink(e=null) {
				
				var r:URLRequest = new URLRequest(Globals.servicURL + "?what=google_login&cache=nui" + OCP.R);
					r.method = URLRequestMethod.POST;
					var uVars:URLVariables = new URLVariables ()
					
					var loader:URLLoader = new URLLoader();
					 trace(34234 , r.url)
					loader.addEventListener(Event.COMPLETE, hersTheLink);
					loader.load(r)
				}
		public function hersTheLink(e:Event):void 
			{
				trace(3423 ,e.target.data)
				var t:TextField  = new TextField()
				var obl:Object = JSON.parse(e.target.data)
				if (obl.google_auth_url) {
					
					
				t.text = obl.google_auth_url
				
				gotoLink = t.text
				

				_s.dispatchEvent(new CEvent(CEvent.WE_HAVE_LINK, null))
				}
				//trace(">>>> HERES THE LINK", t.text)
			}

		
		public function AskforNewPassword(user:String, f:Function = null):void 
		{
			var r:URLRequest = new URLRequest(Globals.servicURL + "?what=user_reset_password");
					r.method = URLRequestMethod.POST;
					var uVars:URLVariables = new URLVariables ()
						
						uVars.username  = user ;
						uVars.hash  =  MD5.hash(user   + 'R$TFGs2ab!ax' )
						
					r.data = uVars;
					loader.addEventListener(Event.COMPLETE, f);
					loader.load(r)
				
		}

		public function LogOut() {
			
				Login.isLogedIn = 1;
				Login.SuperUser = false
				r = new URLRequest(Globals.servicURL + "?what=user_logout");
				loader.addEventListener(Event.COMPLETE, logoutComplete);
				loader.load(r)
				_s.dispatchEvent(new CEvent(CEvent.LOGED_OUT, null));	
			}
			
			
			
		private function logoutComplete(e:Event):void 
		{
			trace("Logout complete");
			loader.removeEventListener(Event.COMPLETE, logoutComplete);
			Login.isLogedIn = 1;
			
		}
	
		private function refresh(e:TimerEvent):void 
		{
			var r:URLRequest = new URLRequest(Globals.servicURL + "?what=user_profile_get" + OCP.R)
				r.method = URLRequestMethod.POST;
				checker.load(r)
		}
		
	}

}