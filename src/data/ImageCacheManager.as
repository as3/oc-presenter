package data
{
	
	
	
	import com.adobe.crypto.MD5;
	import flash.display.Loader;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.events.Event;
	import flash.net.URLLoaderDataFormat;
	import flash.utils.Dictionary;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	import thumbs.File;
	
	
	public class ImageCacheManager extends EventDispatcher
	{
		private static const imageDir:File = File.applicationStorageDirectory.resolvePath("cachedimages/");
		//private static const imageDir:File = File.applicationDirectory.resolvePath("cachedimages/");
		private static var instance:ImageCacheManager;
		private var pendingDictionaryByLoader:Dictionary = new Dictionary();
		private var pendingDictionaryByURL:Dictionary = new Dictionary();
		public var imageLoaded:Loader = new Loader();
		public function ImageCacheManager()
		{
		
		}
			
		public static function getInstance():ImageCacheManager
        {
            if (instance == null)
            {
                instance = new ImageCacheManager();
            }

            return instance;
        }
		public function getImageByURL(url:String):String {
			//addImageToCache(url);
			//return
		
			var cacheFile:File = new File(imageDir.nativePath +File.separator+ cleanURLString(url));
			if (cacheFile.exists) {
				trace("Local", cacheFile.url)
				imageLoaded.load(new URLRequest(cacheFile.url))
				return cacheFile.url;
				
			} else {
				addImageToCache(url);
				return url;
			}
			
		}
		
		public function Close() {
			
			
			
			if (imageLoaded && imageLoaded.contentLoaderInfo) {
				
			try {
				 imageLoaded.close();
				  imageLoaded.unloadAndStop(true);
				   imageLoaded = null;
				}
			catch (e) {
				
				}
           
           
           
        }
        imageLoaded = new Loader();
       
       
		
			
			
			
			
			}
		private  function addImageToCache(url:String):void{
			if(!pendingDictionaryByURL[url]){
				var req:URLRequest = new URLRequest(url);
				var loader:URLLoader = new URLLoader();
				loader.addEventListener(Event.COMPLETE,imageLoadComplete);
				loader.dataFormat = URLLoaderDataFormat.BINARY;
				loader.load(req);
				pendingDictionaryByLoader[loader] = url;
				pendingDictionaryByURL[url] = true;
			} 
		}
		private function imageLoadComplete(event:Event):void{
			var loader:URLLoader = event.target as URLLoader;
			var url:String = pendingDictionaryByLoader[loader];
			imageLoaded.loadBytes(loader.data)
			var cacheFile:File = new File(imageDir.nativePath +File.separator+ cleanURLString(url));
			var stream:FileStream = new FileStream();
			stream.open(cacheFile,FileMode.WRITE);
			stream.writeBytes(loader.data);
			stream.close();
			delete pendingDictionaryByLoader[loader]
			delete pendingDictionaryByURL[url];
		}
		private function cleanURLString(url:String):String{
			var hash:String = MD5.hash(url);
			return hash;
		}
		
	}
}