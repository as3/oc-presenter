﻿package
{
	import com.greensock.TweenMax;
	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.StageVideo;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.ui.Mouse;
	
	public class VPVasa extends VPVasaLib
	{
		
		private var nc:NetConnection;
		private var ns:NetStream;
		private var vid:Video;
		private var client:Object;
		private var _duration;
		private var _path;
		public static var IsPlaying:Boolean = false;
		private var _pauseOnClick:Boolean = true
		private var sVideo:StageVideo;
		
		public function VPVasa(puseOnClick:Boolean = true)
		{
			_pauseOnClick = puseOnClick
			addEventListener(Event.ADDED_TO_STAGE, init)
			this.addEventListener(MouseEvent.CLICK , resetTimer)
			playBtn.gotoAndStop('pause')
		}
		
		public function resetTimer(e:MouseEvent):void 
		{
			TweenMax.killTweensOf(progressBar)
			TweenMax.killTweensOf(playBtn)
			progressBar.alpha = 1
			playBtn.alpha = 1
			Mouse.show()
			TweenMax.to(progressBar, .6, {onStart:StartIT, alpha:0 , delay:1.2 } );
			TweenMax.to(playBtn, .6, { alpha:0 , delay:1.2 } );
			
		}
		
		private function StartIT(e=null):void 
		{
			Mouse.hide()
		}
		
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			this.addEventListener(MouseEvent.MOUSE_MOVE , resetTimer)
			if(stage.stageVideos.length > 0 && Globals.isMobile) {
			 sVideo = stage.stageVideos[0];
			 sVideo.attachNetStream(ns);
			 var point:Point  = localToGlobal(new Point(vidCont.x , vidCont.y))
     		 sVideo.viewPort = new Rectangle(point.x , point.y,1920 	,1080  );
			 vidCont.alpha = 0
			}

		}
		
		
		
		private function asyncErrorHandler(e:AsyncErrorEvent):void
		{
			trace("Async errormo fo")
		}
		
		public function showCOntrols(e:MouseEvent = null):void
		{
			progressBar.visible = true;
			playBtn.visible = true;
		}
		
		public function hideControls(e:MouseEvent = null):void
		{
			progressBar.visible = false;
		}
		
		public function pauseUnpose(e:MouseEvent):void
		{
			if (!VPVasa.IsPlaying)
			{
				trace("ADD ENTER FRAME LISTENER")
				addEventListener(Event.ENTER_FRAME, ShowProgress, false, 0, true);
				if (ns) {
					ns.resume()
					
					playBtn.gotoAndStop(2)
					
					trace("GO TO PAUS")
					}
				
				VPVasa.IsPlaying = true
			}
			else
			{
				removeEventListener(Event.ENTER_FRAME, ShowProgress);
				playBtn.visible = true;
				if (ns) {
					ns.pause()
					playBtn.gotoAndStop('play')
					}		
				VPVasa.IsPlaying = false
			}
		}
		
		private function onNetStatus(pEvent:NetStatusEvent)
		{
			
			switch (pEvent.info.level)
			{
				case "error": 
					trace("yep it`s an error");
					
					break;
				case "status": 
					switch (pEvent.info.code)
				{
					case "NetStream.Play.Start": 
						progressBar.pb.width = 0;
						progressBar.lb.width = 0;
						break;
					case "NetStream.Play.Stop": 
						playBtn.gotoAndStop('replay');
						
						
						vidCont.removeEventListener(MouseEvent.CLICK, pauseUnpose)
						playBtn.addEventListener(MouseEvent.CLICK, replayVideo)
						playBtn.mouseEnabled = true
						playBtn.mouseChildren = true
						VPVasa.IsPlaying = false
						break;
					case "": 
						break;
				}
					break;
			}
		}
		
		private function replayVideo(e:Event):void
		{
			trace("REPLAY BTN PRESSED")
			vidCont.addEventListener(MouseEvent.CLICK, pauseUnpose)
			playBtn.removeEventListener(MouseEvent.CLICK, replayVideo)
			playBtn.mouseEnabled = false
			playBtn.mouseChildren = false
			ns.seek(0);
			ns.resume();
			playBtn.gotoAndStop('pause')
		}
		
		public function setVideo(path, autoPlay:Boolean = true)
		{
		
			nc = new NetConnection();
			nc.connect(null);
			ns = new NetStream(nc);
			ns.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			ns.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus, false, 0, true);
			vid = new Video(1920, 1080);
			vid.smoothing = true;
			
			vidCont.addChild(vid);
			if (_pauseOnClick)
			{
				
				vidCont.addEventListener(MouseEvent.CLICK, pauseUnpose)
				trace("AND HERE")
			}
			vid.x = 0;
			vid.y = 0;
			client = new Object();
			ns.client = client;
			client.onMetaData = nsMetaDataCallback;
			
			playBtn.mouseEnabled = false
			playBtn.mouseChildren = false
			progressBar.visible = true;
			playBtn.visible = true
			progressBar.lb.addEventListener(MouseEvent.CLICK, gotoSecond);
			
			addChild(progressBar)
			if (sVideo ) {
				sVideo.attachNetStream(ns);
				trace("attached")
				}
			else{
				vid.attachNetStream(ns);
			}
			
			_path = path
			ns.play(path)
			playBtn.gotoAndStop('pause')
		
			addEventListener(Event.ENTER_FRAME, ShowProgress, false, 0, true);
			if (!autoPlay)
			{
				ns.pause();
				playBtn.gotoAndStop('play')
				playBtn.visible = true
				removeEventListener(Event.ENTER_FRAME, ShowProgress);
			}
		}
		
		public function playIt(e:Event):void
		{
			ns.resume()
			playBtn.gotoAndStop('play')
			addEventListener(Event.ENTER_FRAME, ShowProgress, false, 0, true);
			VPVasa.IsPlaying = true
		}
		
		private function ShowProgress(e:Event):void
		{
			var avarage = Math.round(ns.time) / _duration * progressBar.bck.width;
			progressBar.pb.width = avarage;
			progressBar.lb.width = ns.bytesLoaded / ns.bytesTotal * progressBar.bck.width;
			//if (progressBar.pb.width < 1920)
			//{	
				//playBtn.gotoAndStop('' )	
			//}
		}
		
		private function gotoSecond(e:MouseEvent):void
		{
			if (! VPVasa.IsPlaying)
			{
				
				progressBar.pb.width = e.localX;
				ns.seek(_duration *e.localX / progressBar.bck.width);
				ns.pause()
				
			}
			else
			{
				progressBar.pb.width = e.localX;
				ns.seek(_duration * e.localX / progressBar.bck.width);
				ns.resume();
				
			}
		}
		
		private function nsMetaDataCallback(mdata:Object):void
		{
			_duration = mdata.duration;
		}
		
		public function setColor(e:Number)
		{
			var colorRestore:ColorTransform = new ColorTransform()
			colorRestore.color = e;
		}
		
		public function IsPlaying():Boolean
		{
			return playBtn.visible
		}
		
		public function stopIt()
		{
			playBtn.visible = true;
			removeEventListener(Event.ENTER_FRAME, ShowProgress);
			if (ns)
			{
				ns.seek(0);
				progressBar.pb.width = 0;
				ns.pause();
			}
			playBtn.visible = true;
		}
		
		public function Destroy():void
		{
			TweenMax.killTweensOf(progressBar)
			TweenMax.killTweensOf(playBtn)
			VPVasa.IsPlaying = false
			removeEventListener(Event.ENTER_FRAME, ShowProgress);
			progressBar.removeEventListener(MouseEvent.CLICK, gotoSecond);
			if (ns)
			{
				ns.removeEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
				
				ns.close();
				ns = null
				
			}
			if (nc && nc.connected)
			{
				nc.close()
			}
		
		}
	
	}
}