package co.moodshare.pdf
{
	import co.moodshare.pdf.decoding.PNGDecoder;
	import co.moodshare.pdf.encoding.PNGEncoder;
	import co.moodshare.pdf.images.PNGImageAlpha;
	import co.moodshare.pdf.images.RawImage;
	import flash.display.Bitmap;
	import flash.display.PNGEncoderOptions;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	import org.alivepdf.encoding.JPEGEncoder;

	import org.alivepdf.encoding.TIFFEncoder;
	import org.alivepdf.images.ColorSpace;
	import org.alivepdf.images.DoJPEGImage;
	import org.alivepdf.images.DoPNGImage;
	import org.alivepdf.images.DoTIFFImage;
	import org.alivepdf.images.GIFImage;
	import org.alivepdf.images.ImageFormat;
	import org.alivepdf.images.JPEGImage;
	import org.alivepdf.images.PDFImage;
	import org.alivepdf.images.PNGImage;
	import org.alivepdf.images.TIFFImage;
	import org.alivepdf.images.gif.player.GIFPlayer;
	import org.alivepdf.layout.Resize;
	import org.alivepdf.layout.Size;
	import org.alivepdf.links.ILink;
	import org.alivepdf.pdf.PDF;

	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	/**
	 * 
	 * description
	 *
	 * @langversion ActionScript 3
	 * @playerversion Flash 9.0.0
	 *
	 * @author jamesrobb
	 * @since Jun 8, 2011
	 * 
	 */
	public class MSPDF extends PDF 
	{

		public function MSPDF(orientation : String = 'Portrait', unit : String = 'Mm', autoPageBreak : Boolean = true, pageSize : Size = null, rotation : int = 0)
		{
			super( orientation, unit, autoPageBreak, pageSize, rotation );
		}
		
		
		/**
		 * The addImageStream method takes an incoming image as a ByteArray. This method can be used to embed high-quality images (300 dpi) to the PDF.
		 * You must specify the image color space, if you don't know, there is a lot of chance the color space will be ColorSpace.DEVICE_RGB.
		 * 
		 * @param imageBytes The image stream (PNG, JPEG, GIF)
		 * @param colorSpace The image colorspace
		 * @param resizeMode A resizing behavior, like : new Resize ( Mode.FIT_TO_PAGE, Position.CENTERED ) to center the image in the page
		 * @param x The x position
		 * @param y The y position
		 * @param width The width of the image
		 * @param height The height of the image
		 * @param rotation The rotation of the image
		 * @param alpha The image alpha
		 * @param blendMode The blend mode to use if multiple images are overlapping
		 * @param keepTransformation Do you want the image current transformation (scaled, rotated) to be preserved
		 * @param link The link to associate the image with when clicked
		 * @example
		 * This example shows how to add an RGB image as a ByteArray into the current page :
		 * <div class="listing">
		 * <pre>
		 *
		 * myPDF.addImageStream( bytes, ColorSpace.DEVICE_RGB );
		 * </pre>
		 * </div>
		 * 
		 * This example shows how to add a CMYK image as a ByteArray into the current page, the image will take the whole page :
		 * <div class="listing">
		 * <pre>
		 * var resize:Resize = new Resize ( Mode.FULL_PAGE, Position.CENTERED ); 
		 * myPDF.addImageStream( bytes, ColorSpace.DEVICE_RGB, resize );
		 * </pre>
		 * </div>
		 * 
		 * This example shows how to add a CMYK image as a ByteArray into the current page, the image will take the whole page but white margins will be preserved :
		 * <div class="listing">
		 * <pre>
		 * var resize:Resize = new Resize ( Mode.RESIZE_PAGE, Position.CENTERED ); 
		 * myPDF.addImageStream( bytes, ColorSpace.DEVICE_CMYK, resize );
		 * </pre>
		 * </div>
		 */	 
		override public function addImageStream ( imageBytes:ByteArray,
												  colorSpace:String,
												  resizeMode:Resize=null,
												  x:Number=0,
												  y:Number=0,
												  width:Number=0,
												  height:Number=0,
												  rotation:Number=0,
												  alpha:Number=1,
												  blendMode:String="Normal",
												  link:ILink=null ):void
		{
			if ( streamDictionary[imageBytes] == null )
			{
				imageBytes.position = 0;
				
				var id:int = getTotalProperties ( streamDictionary )+1;
				
				if ( imageBytes.readUnsignedShort() == JPEGImage.HEADER )
				{
					image = new JPEGImage ( imageBytes, colorSpace, id );
					
				}else if ( PNGDecoder.isPNG( imageBytes ) ) {
					
					if( PNGDecoder.getColorType( imageBytes ) != 6 )
					{
						image = new PNGImage ( imageBytes, colorSpace, id );
					
					}else{
						
						var bmd : BitmapData = PNGDecoder.decode( imageBytes );
						addBitmapData( bmd, resizeMode, x, y, width, height, rotation, alpha, blendMode, link );
						return;
					}
					
				}else if ( !(imageBytes.position = 0) && imageBytes.readUTFBytes(3) == GIFImage.HEADER ) {
					
					imageBytes.position = 0;
					var decoder:GIFPlayer = new GIFPlayer(false);
					var capture:BitmapData = decoder.loadBytes( imageBytes );
					var bytes:ByteArray = PNGEncoder.encode ( capture );
					image = new DoPNGImage ( capture, bytes, id );
					
				} else if ( !(imageBytes.position = 0) && (imageBytes.endian = Endian.LITTLE_ENDIAN) && imageBytes.readByte() == 73 ){
					
					image = new TIFFImage ( imageBytes, colorSpace, id );
					
				} else throw new Error ("Image format not supported for now.");
				
				streamDictionary[imageBytes] = image;
				
			} else image = streamDictionary[imageBytes];
			
			setAlpha ( alpha, blendMode );
			placeImage( x, y, width, height, rotation, resizeMode, link );
		}
		
		
		public function addBitmapData ( bmd:BitmapData,
								 	    resizeMode:Resize=null,
									    x:Number=0,
									    y:Number=0,
									    width:Number=0,
									    height:Number=0,
									    rotation:Number=0,
									    alpha:Number=1,
									    blendMode:String="Normal",
									    link:ILink = null ,
										byts:ByteArray  = null):void
		{
		
	        var transparent : Boolean = bmd.transparent;
			var id : int;
			var trans:ByteArray = new ByteArray();
			if( transparent )
			{
				id = getTotalProperties ( streamDictionary ) + 1;
				trans = PNGEncoder.encodeGrayscale(bmd)
				image = new PNGImageAlpha(trans,  ColorSpace.DEVICE_GRAY, id, bmd.width, bmd.height);
				streamDictionary[trans] = image;
				setAlpha( 0 );
				placeImage( x, y, width, height, rotation, resizeMode, null );
			}
			
			id = getTotalProperties ( streamDictionary ) + 1;
			
			var bitmapData:BitmapData  = new BitmapData(bmd.width , bmd.height, false, 0xFFFFFF);
			bitmapData.copyPixels(bmd, bmd.rect, new Point(0, 0)); 
		
			var enc:ByteArray = new ByteArray()
			bitmapData.encode(bmd.rect, new PNGEncoderOptions(true), enc )
			image = new PNGImageAlpha(enc,  ColorSpace.DEVICE_RGB, id, bmd.width, bmd.height, transparent);
	
			streamDictionary[enc] = image;
			setAlpha ( alpha, blendMode );
			placeImage( x, y, width, height, rotation, resizeMode, link );
		}
		
		
		override protected function insertImages ():void
		{
			var filter:String = new String();
			var stream:ByteArray;
			var images : Array = [];
			var image:PDFImage;
		
			 
			for each ( image in streamDictionary )
			{
				images.push( image );
			}
			
			images.sortOn( [ "resourceId" ], Array.NUMERIC );
			
			
			for each ( image in images )
			{
				trace(992,image);
				newObj();
				image.n = n;
				write('<</Type /XObject');
				write('/Subtype /Image');
				write('/Width '+image.width);
				write('/Height '+image.height);
				
				if ( image.masked )
					write('/SMask '+(n-1)+' 0 R');
				
				if( image.colorSpace == ColorSpace.INDEXED ) 
					write ('/ColorSpace [/'+ColorSpace.INDEXED+' /'+ColorSpace.DEVICE_RGB+' '+((image as PNGImage).pal.length/3-1)+' '+(n+1)+' 0 R]');
				else
				{
					write('/ColorSpace /'+image.colorSpace);
					if( image.colorSpace == ColorSpace.DEVICE_CMYK ) 
						write ('/Decode [1 0 1 0 1 0 1 0]');
				}
				
				write ('/BitsPerComponent '+image.bitsPerComponent);
				
				if (image.filter != null ) 
					write ('/Filter /'+image.filter);
				
				if ( image is PNGImage || image is GIFImage )
				{
					if ( image.parameters != null ) 
						write (image.parameters);
					
					if ( image.transparency != null && image.transparency is Array )
					{
						var trns:String = '';
						var lng:int = image.transparency.length;
						for (var i:int=0;i<lng;i++) 
							trns += image.transparency[i]+' '+image.transparency[i]+' ';
						write('/Mask ['+trns+']');	
					}
				}
				
				stream = image.bytes;
				write('/Length '+stream.length+'>>');
				write('stream');
				buffer.writeBytes (stream);
				buffer.writeUTFBytes ("\n");
				write("endstream");
				write('endobj');
				
				if( image.colorSpace == ColorSpace.INDEXED )
				{
					newObj();
					var pal:String = (image as PNGImage).pal;
					write('<<'+filter+'/Length '+pal.length+'>>');
					writeStream(pal);
					write('endobj');
				}
			}
		}
		
	}
	
}
