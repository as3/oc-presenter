package co.moodshare.pdf.images
{
	import org.alivepdf.images.ColorSpace;
	import org.alivepdf.images.PDFImage;
	import org.alivepdf.images.PNGImage;

	import flash.utils.ByteArray;

	/**
	 * 
	 * description
	 *
	 * @langversion ActionScript 3
	 * @playerversion Flash 9.0.0
	 *
	 * @author jamesrobb
	 * @since Jun 7, 2011
	 * 
	 */
	public class PNGImageAlpha extends PNGImage 
	{



		public function PNGImageAlpha( imageStream : ByteArray,
								  colorSpace : String,
								  id : int,
								  wd : Number,
								  ht : Number,
								  isMasked : Boolean = false )
		{
			_width = wd;
			_height = ht;
			_masked = isMasked;
			
			super( imageStream, colorSpace, id );
		}
		
		/*
		 * @see http://www.w3.org/TR/PNG-Chunks.html
		 */
	
	}
	
}
