package 
{
import flash.events.Event;


public class CEvent extends Event
{
public var data:*; 	
public static const DRAG_THIS_IN:String = "DragThis" 
public static const HERE_ARE_THE_FILES_IN_THE_FOLDER : String = "HereAreTheFilesInTheFolder"
public static const SHOWING_FLODER_CONTENTS : String = "ShowingFolderContents";
public static const SHOWING_ALL_PRESENTATIONS : String = "ShowingAllPresentations";
public static const FOLDERS_RECIVED : String = "foldersRecived"
public static const REFRESH_CURRENT_FOLDER : String = "RefreshCurrentFolder"
public static const INSERT_THIS:String = "insertThis"
public static const START_PRESENTATION : String =  "startPresentation"
public static const LOAD_MY_PRESENTATIONS_SLIDES : String =  "LoadMYPresentationSlides"
public static const REFRESH_GROUPS : String = "RefreshGroups"
public static const LOGED_IN:String = "OnLogIN"
public static const LOGED_OUT:String = "OnLogOUT"
public static const SET_POSITION:String = "setPosition"
public static const PLAY_THIS_PRESENTATION:String = "PlayThisPresentation"
public static const SHOW_PRESENTATION_INFO:String = "showPresentationInfo"
public static const REMO_PLAY:String = "Play"
public static const REMO_VOLUME:String = "Volume"
public static const REMO_LIGNT:String = "Light" 
public static const SHOW_MINI_POP:String = "ShowMiniPop"
public static const LOAD_PRESENTATION_SLIDE:String = "LoadPresentationSlides"
public static const UPLOAD_SLIDES:String = 'Upload Slides'
public static const DELETE_CURRENT_PRESENTATION:String = "DeleteCurrentPresentation"
public static const DELETE_CURRENT_FOLDER:String = "DeleteCurrentFolder"
public static const SHOW_SLIDE_INFO:String = "ShowSlideINFO"
public static const HERE_ARE_THE_PUBLIC_PRESENTATIONS:String = 'hereAreThePublicPresentations'
public static const CLEAR_TIMELINE:String = "ClearTimeLine"
public static const LOAD_NEXT:String =  "loadNext"
public static const LOAD_PREV:String =  "loadPrev"
public static const UPDATA_FOLDER_DATA:String = 'UpdateFolderData'
public static const SHOW_FOLDER_INFO:String = "ShowFolderInfo";
public static const ADD_THIS_FOLDER_TO_PRESENTATION:String = "addThisFolderToPresentation"
public static const ALL_IMAGES_UPLOADED:String = "AllImagesUploaded"
public static const HIDE_DRIVE_MENU:String =  'hideBreadCrums'
public static const VIEW_SLIDE_IN_THIS_FOLDER:String =  "viewSlideinthisFolder"
public static const START_AUTO_PLAY:String ="StartAutoPlay"
public static const ENABLE_PRESENTATION_BUTTONS:String = "EnableButtons"
public static const SHOW_SLIDE:String = "show_slide"
public static const SAVE_A_COPY:String = "SaveACopy"
public static const CREATE_NEW_PRESENTATION:String = "CreateNewPresentation"
public static const DELETE:String = "delete";
public static const HIDE_FORM :String = "hideForm";
public static const DELETE_FILE:String = "deleteFile";
public static const UPOADING:String = "upoading";
public static const STARTED:String = "started";
public static const SHOW_EDIT_VIEW:String = "showEditView";
public static const DRAG_FILE:String = "dragFile";
public static const LOG_IN_FAIL:String = "logInFail";
public static const DELETE_THILE:String = "deleteThile";
public static const SHOW_DUPLICATE_FORM:String = "showDuplicateForm";
public static const ADD_THIS_SLIDE:String = "AddThisSlide";
public static const WE_HAVE_LINK:String = "weHaveLink";
public static const SHOW_SIDE_TREE:String = "showSideTree";
public static const SHOW_REMO_POP:String = "showRemoPop";
public static const LOADING_MAIN_VIEW:String = "loadingMainView";
static public const LOAD_SLIDE:String = "loadSlide";
static public const LOAD_PUBLIC_PRESENTATIONS:String = "loadPublicPresentations";
static public const SAVE_PRESENTATION:String = "savePresentation";
static public const LANGUAGE_HAS_ARIVED:String = "languageHasArived";
static public const SHOW_MINI_POP_RIGHT:String = "showMiniPopRight";
static public const BIG:String = "_________________________88o_____o88888888\n";
static public const SLIDE_PRELOADED:String = "slidePreloaded"
static public const PRELOAD_COMPLETE:String = "preloadComplete";
static public const FORCE_DELETE_FILE:String = "forceDeleteFile";
static public const REMOTE_CONNECTED:String = "remoteConnected";
static public const REMOTE_SHOW_SLIDE:String = "remoteShowSlide";
static public const AIR_SERVER_CONNECTED:String = "airServerConnected";

public function CEvent(type:String, data:*)
{
this.data= data;
super(type);
}
}
}