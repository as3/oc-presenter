package thumbs
{
	import com.greensock.*;
	import controllers.DriveController;
	import data.DriveController;
	import data.Uploader;
	import fl.transitions.easing.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.net.*;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import popups.DeleteFilePop;
	import uk.soulwire.utils.display.DisplayUtils;
	
	public class FileList extends driveFileListUploadLIB implements IThumb
	{
		private var loader:Loader = new Loader();
		private var img:Bitmap = new Bitmap();
		private var twSpeed:Number = .2;
		private var delPresPop:DeleteFilePop;
		private var _fileRef:FileReferenceList;
		public var idDragging:Boolean = false;
		public var id:int
		public var isVideo:Boolean = false;
		public var OBJ:Object;
		public var hotZone:Sprite
		public var isFolder:Boolean = false;
		public var Parent:Sprite;
		
		public function FileList()
		{
			//gotoAndStop(2)
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, showImage)
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ldrError);
			imageArea.buttonMode = false
			dr.addEventListener(MouseEvent.MOUSE_DOWN, startThisDrag)
			delBtn.addEventListener(MouseEvent.CLICK, deletThis)
			updateBtn.addEventListener(MouseEvent.CLICK, updateImage)
			TextField(numSlides).backgroundColor = 0xA44091;
			TextField(numSlides).border = false
			folderIcon.visible = false
		
		}
		
		private function updateImage(e:MouseEvent):void 
		{
			_fileRef = new FileReferenceList();
			_fileFilter = new FileFilter("Image", "*.jpeg;*.jpg;*.png;*.avi;*.mov;*.mpg;*.mp4;*.flv;*.wma;*.wmv;");
			_fileRef.browse([_fileFilter]);
			_fileRef.addEventListener(Event.SELECT, _onImageSelect);
		}
		
		private function _onImageSelect(e:Event):void 
		{
			trace("this is the file reference >>>>>>", OBJ.id)
			Uploader.getInstance().updateFileReference(_fileRef.fileList[0], OBJ.id);
		}
		
		private function addToPresentation(e:MouseEvent):void
		{
		}
		
		public function Edit(e:Boolean)
		{
			if (e)
			{
				TextField(numSlides).type = TextFieldType.INPUT
				numSlides.setSelection(0, numSlides.text.length)
				addChild(numSlides);
				addChild(delBtn);
				addChild(updateBtn);
			}
			else
			{
				TextField(numSlides).type = TextFieldType.DYNAMIC
				numSlides.setSelection(0, 0)
				addChild(dr);
			}
			
			delBtn.visible = e
			updateBtn.visible = e
			TextField(numSlides).background = e
		
		}
		
		public function Unselect()
		{
		}
		
		public function Select()
		{
			
		}
		
		public function Fill(e, n)
		{
			OBJ = e
			id = e.id
			this.inc.visible = false
			if (e.thumb_image)
			{
				loader.load(new URLRequest(Globals.baseURL + e.thumb_image))
			}
			if (e.type == 'slideTextImage')
			{
				
			}
			
			else if(e.type == 'slideVideo')
			{
				isVideo = true
				this.inc.visible = true
			}
			
			else if (e.type == 'folder') {
				folderIcon.visible = true
				}
			numSlides.text = decodeURI(e.title)
			Edit(DriveController.editPositions)
		}
		
		private function startThisDrag(e:MouseEvent):void
		{
			
			dispatchEvent(new Event(CEvent.DRAG_THIS_IN))
		}
		
		private function deletThis(e:MouseEvent):void
		{
			delPresPop = new DeleteFilePop(id, OCP.LG.doyouWantToDeleteFileT)
			stage.addChild(delPresPop)
			OCP.prompt = delPresPop
		}
		
		private function ldrError(e:Event):void
		{
		}
		
		public function Reset(e = null)
		{
			TweenMax.killTweensOf(this)
		}
		
		private function showImage(e:Event):void
		{
			img = Bitmap(loader.content)
			img.smoothing = true
			DisplayUtils.fitIntoRect(img, new Rectangle(0, 0, imageArea.width, imageArea.height), false)
			addChild(img)
			this.cacheAsBitmap = true
		}
	
	}
}