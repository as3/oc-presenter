package thumbs
{
	import com.greensock.*;
	import data.Data;
	import fl.transitions.easing.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.net.*;
	import flash.text.TextFormat;
	import popups.DeleteFilePop;
	import uk.soulwire.utils.display.DisplayUtils;
	
	public class MYPresentationList extends presentationListLIB implements IThumb
	{
		
		private var loader:Loader = new Loader();
		private var img:Bitmap = new Bitmap();
		private var twSpeed:Number = .2;
		private var delPresPop:DeleteFilePop;
		public var idDragging:Boolean = false;
		public var id:int
		public var isVideo:Boolean = false;
		public var OBJ:Object;
		public var hotZone:Sprite
		public var isFolder:Boolean = false;
		public static var HiglightedThumb:Object
		
		public function MYPresentationList()
		{
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, showImage)
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ldrError);
			highLight.addEventListener(MouseEvent.MOUSE_DOWN, startThisDrag)
			highLight.visible = false
			settingsBtn.visible = false
			settingsBtn.addEventListener(MouseEvent.CLICK, showSettingsButton)
			this.addEventListener(MouseEvent.ROLL_OVER, showSettingsIcon)
			this.addEventListener(MouseEvent.ROLL_OUT, hideSettings)
			this.addEventListener(MouseEvent.CLICK, Select)
			numSlides.mouseEnabled = false
		
		}
		
		private function showSettingsButton(e:MouseEvent):void
		{
			HightLIght()
			stage.dispatchEvent(new CEvent(CEvent.SHOW_MINI_POP_RIGHT, this));
		
		}
		
		private function hideSettings(e:MouseEvent):void
		{
			settingsBtn.visible = false
		}
		
		private function showSettingsIcon(e:MouseEvent):void
		{
			settingsBtn.visible = true
		}
		
		private function addToPresentation(e:MouseEvent):void
		{
		
		}
		
		public function HightLIght()
		{
			var textFormat = new TextFormat()
			if (MYPresentationList.HiglightedThumb)
			{
				MYPresentationList.HiglightedThumb.glow.visible = true
				TweenMax.to(MYPresentationList.HiglightedThumb.numSlides, 0, {tint: 0x3AACBE})
				
			}
			MYPresentationList.HiglightedThumb = this
			glow.visible = false
			TweenMax.to(numSlides, 0, {tint: 0xA44191})
		}
		
		public function Unselect()
		{
		}
		
		public function Select(e = null)
		{
			if (e && e.target == settingsBtn)
			{
				return
			}
			HightLIght()
			stage.dispatchEvent(new CEvent(CEvent.LOAD_MY_PRESENTATIONS_SLIDES, OBJ));
		}
		
		public function Fill(e, n)
		{
			OBJ = e
			id = e.instance_id
			
			if (e.thumb_image)
			{
				loader.load(new URLRequest(Globals.servicURL + e.thumb_image))
			}
			if (e.type == 'slideTextImage')
			{
				this.inc.visible = false
			}
			else
			{
				isVideo = true
			}
			var tf:TextFormat = new TextFormat()
			tf.italic = true
			tf.color = 0x666666
			numSlides.text = unescape(decodeURI(e.title)) + " "
			numSlides.setTextFormat(tf, numSlides.text.length - 1)
		}
		
		private function startThisDrag(e:MouseEvent):void
		{
			dispatchEvent(new Event(CEvent.DRAG_THIS_IN))
		}
		
		private function deletThis(e:MouseEvent):void
		{
			delPresPop = new DeleteFilePop(id)
			stage.addChild(delPresPop)
			OCP.prompt = delPresPop
		}
		
		private function ldrError(e:Event):void
		{
		}
		
		public function Reset(e = null)
		{
			TweenMax.killTweensOf(this)
		}
		
		private function showImage(e:Event):void
		{
			img = Bitmap(loader.content)
			img.smoothing = true
			this.cacheAsBitmap = true
		}
	
	}
}