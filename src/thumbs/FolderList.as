package thumbs 
{
	import com.greensock.*;
	import data.Data;
	import fl.transitions.easing.*;
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	import flash.text.TextFormat;
	import popups.DeleteFilePop;
	
	public class FolderList extends folderListLIB implements IThumb {
	
		private var loader:Loader = new Loader();
		private var img:Bitmap = new Bitmap();
		private var twSpeed:Number = .2;
		private var delPresPop:DeleteFilePop;
		public var idDragging:Boolean = false;
		public var id:int
		public var isVideo:Boolean = false;
		public var OBJ:Object;
		public var hotZone:Sprite
		public var isFolder:Boolean = false;
		public static var HiglightedThumb:Object 
		public static var YPosition:Number = 0 
		
		public function FolderList()
		{
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, showImage)
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ldrError);
			glow.addEventListener(MouseEvent.MOUSE_DOWN, startThisDrag)
			highLight.visible = false
			settingsBtn.visible = false
			settingsBtn.addEventListener(MouseEvent.CLICK , showSettingsButton)
			this.addEventListener(MouseEvent.ROLL_OVER , showSettingsIcon)
			this.addEventListener(MouseEvent.ROLL_OUT , hideSettings)
			this.addEventListener(MouseEvent.CLICK , Select)	
		}
		
		private function showSettingsButton(e:MouseEvent):void 
		{
			HightLIght()
			stage.dispatchEvent(new CEvent(CEvent.SHOW_MINI_POP , this));
		}
		
		private function hideSettings(e:MouseEvent):void 
		{
			settingsBtn.visible = false
		}
		
		private function showSettingsIcon(e:MouseEvent):void 
		{
			settingsBtn.visible = true
			
		}
		
		private function addToPresentation(e:MouseEvent):void 
		{
			
		}
		
		public function HightLIght() {
			var textFormat = new TextFormat()
			if (FolderList.HiglightedThumb) {	
				textFormat.color =  0x3AACBE
				
				FolderList.HiglightedThumb.glow.visible = true
				FolderList.HiglightedThumb.title.setTextFormat(textFormat)
				
				}	
			FolderList.HiglightedThumb = this
			glow.visible = false			
			textFormat.color =  0xA44191
			title.setTextFormat(textFormat)
		
			}
			
	
			
		public function Unselect() {}
		public function Select(e = null) {
			
			if ( this.x >  this.parent.width) {
				return
				}
			HightLIght()
			if (e && e.target.name == 'settingsBtn') return 
				Data.getInstance().GetSlidesInFolder(OBJ.id);
				stage.dispatchEvent(new CEvent(CEvent.VIEW_SLIDE_IN_THIS_FOLDER , OBJ))
			}

		public function Fill(e , n, level  = 1, parent = null )
		{
			OBJ = e
			id = e.id
			Parent = parent
			gotoAndStop(level)
			
			if (e.thumb_image) {
				loader.load(new URLRequest(Globals.servicURL + e.thumb_image))
			}
			if (e.type == 'slideTextImage') {
				this.inc.visible  = false
				}
			else {
				isVideo = true
				}
				title.text =  unescape(decodeURI(e.title))
				title.mouseEnabled = false
		}
		
		
		
		private function startThisDrag(e:MouseEvent):void 
		{
			dispatchEvent(new Event(CEvent.DRAG_THIS_IN))			
		}

		private function deletThis(e:MouseEvent):void
		{	
			delPresPop = new DeleteFilePop(id)
			stage.addChild(delPresPop)
			OCP.prompt = delPresPop	
		}

		private function ldrError(e:Event):void{}

		public function Reset(e=null)
		{
			TweenMax.killTweensOf(this)
		}
		
		private function showImage(e:Event):void
		{
			img = Bitmap(loader.content)
			img.smoothing = true
		
			
			this.cacheAsBitmap = true
		}
		
		
	
	}
}