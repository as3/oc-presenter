package thumbs
{
	import com.greensock.*;
	import data.Data;
	import fl.transitions.easing.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.net.*;
	import org.pastila.utils.json.formatJSON;
	import utils.DisplayUtils;
	
	//public class MySlide extends slideLIB implements IThumb
	public class MySlide extends slideLIBPublic implements IThumb
	{
		
		private var loader:Loader = new Loader();
		public var img:Bitmap = new Bitmap();
		private var twSpeed:Number = .2;
		public var idDragging:Boolean = false;
		public var id:int = 9
		public var fliped:Boolean = false
		public var OBJ:Object;
		public var hotZone:Sprite
		public var isFolder:Boolean = false;
		public var isVideo:Boolean = false;
		private var baseURL:String = Globals.servicURL + 'resources/files/images/';
		public var VideoName:String = ''
		public static var HiglightedThumb:Object
		
		public function MySlide()
		{
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, showImage)
			loader.contentLoaderInfo.addEventListener("ioError", ldrError);	
			menu.playBtn.addEventListener(MouseEvent.CLICK, Select)
			//menu.infoBtn.addEventListener(MouseEvent.CLICK, infoClickd)
			menu.colseBtn.addEventListener(MouseEvent.CLICK, deletThis)
			menu.visible = false
			this.addEventListener(MouseEvent.ROLL_OVER, OnRollOver)
			this.addEventListener(MouseEvent.ROLL_OUT, OnRollOUT)	
			inc.mouseEnabled = false
			inc.mouseChildren = false
			
			highLight.visible = false
			//title.visible = false;
			inc.visible = false;
			imageArea.addEventListener(MouseEvent.MOUSE_DOWN, startThisDrag)
		
		}
		
		private function OnRollOUT(e:MouseEvent):void
		{
			menu.visible = false
		}
		
		private function OnRollOver(e:MouseEvent):void
		{
			menu.visible = true
		}
		
		//private function infoClickd(e:MouseEvent):void
		//{
			//stage.dispatchEvent(new Event(CEvent.SHOW_SLIDE_INFO))
		//}
		
		public function HightLIght()
		{
			if (MySlide.HiglightedThumb)
			{
				MySlide.HiglightedThumb.highLight.visible = false
			}
			MySlide.HiglightedThumb = this
			highLight.visible = true
		
		}
		
		public function UnHighLight(e){}
		public function Unselect()
		{
			highLight.visible = false
		}

		public function Select(e = null)
		{
			if (e)
			{
				if (e == "JUSTPLAY" || e.target.name == 'playBtn')
				{
					HightLIght()
					dispatchEvent(new CEvent(CEvent.LOAD_SLIDE, this))
				
					
					
				}
				else if (e.target.name == 'infoBtn' || e.target.name == 'colseBtn')
					return
				
			}
		}
		
			
		public function Fill(e, n)
		{
			
			OBJ = e
			id = e.id
		//	trace("SDASDAS", JSON.stringify(e))
			
			if (stage)
			{
				stage.dispatchEvent(new CEvent(CEvent.ENABLE_PRESENTATION_BUTTONS, null))
			}
			
			if (e.type == 'slideVideo' || ( OBJ.id && String(OBJ.id).substring(0,1) == "V"))
			{
				isVideo = true;
				inc.visible = true;
				inc.mouseEnabled = false
				inc.mouseChildren  =  false
				if ( OBJ.id && String(OBJ.id).substring(0, 1) == "V") {
					id = String(OBJ.id).substring(1)
					}
				else {
					OBJ.id = "V" + String(OBJ.id) 
					}
			}
			
			//if (e.thumb_image)
			//{
				//var r2:URLRequest = new URLRequest(Globals.baseURL + e.thumb_image)
				//loader.load(r2)
				//trace(12312312 , r2.url)
				//VideoName = e.thumb_image
				//FillSlide(e)
			//}
			//else
			//{
				var r:URLRequest = new URLRequest(Globals.baseURL + Globals.thumbURL + id + ".jpg")
				if (isVideo) {
					r.url = Globals.baseURL + Globals.videothumbURL +id + '.jpg'
					
					}
				loader.load(r)
				
				FillSlide(e)
			//}
			menu.slideNumber.mouseEnabled = false
            
			//title.text = n
		}
		
		private function FillSlide(e):void
		{
			//title.text = e.title
			numSlides.text = ""	
		}
		
		private function FillPresentation(e):void
		{
			//title.text = e.title
		}
		
		private function FillFolder(e):void
		{
			//title.text = e.title
			numSlides.text = e.slidesCount + " slides in this group"
			isFolder = true
		}
		
		private function startThisDrag(e:MouseEvent):void
		{
			HightLIght()
			dispatchEvent(new Event(CEvent.DRAG_THIS_IN))
		}
		
		private function addSlides(e:MouseEvent):void
		{
			stage.dispatchEvent(new CEvent(CEvent.UPLOAD_SLIDES, id));
		}
		
		private function deletThis(e:MouseEvent):void
		{
			this.dispatchEvent(new CEvent(CEvent.DELETE, this))
		}
		
		private function ldrError(e:IOErrorEvent):void
		{
			//var r:URLRequest = new URLRequest(Globals.baseURL + 'modules/ocpresenter/resources/slide_video_thumbs/' +id + '.jpg')
				//loader.load(r)
			//return		
			
		
		}
		
		//private function loadThumb(e:Event):void
		//{
			//var o:Object = JSON.parse(e.target.data)
			//if (o.status == 'success')
			//{
				//if (o.pages.length > 0 && o.pages[0].type == 'slideVideo')
				//{
					//isVideo = true
					//inc.visible = true;
					//var r:URLRequest = new URLRequest(Globals.baseURL + o.pages[0].thumb_image)
					////loader.load(r)
				//}
			//}
		//}
		
		public function Reset(e = null)
		{
			TweenMax.killTweensOf(this)
			//title.text = e
			menu.slideNumber.text = e
		}
		
		private function showImage(e:Event):void
		{
			img = Bitmap(loader.content)
			img.smoothing = true
			DisplayUtils.fitIntoRect(img, new Rectangle(0, 0, imageArea.width, imageArea.height), false)
			imageArea.addChild(img)
			img.mask = imageArea.m
			this.cacheAsBitmap = true
		}
	
	}
}