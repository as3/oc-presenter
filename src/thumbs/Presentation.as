package thumbs
{
	import com.greensock.*;
	import fl.transitions.easing.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.net.*;
	import flash.text.TextFormat;
	import org.pastila.utils.json.formatJSON;
	import utils.DisplayUtils;
	
	public class Presentation extends presentationLIB implements IThumb
	{
		private var loader:Loader = new Loader();
		private var img:Bitmap = new Bitmap();
		private var twSpeed:Number = .2;
		public var idDragging:Boolean = false;
		public var id:int
		public var OBJ:Object;
		public var hotZone:Sprite
		private var baseURL:String = Globals.servicURL + 'resources/files/images/';
		public static var HiglightedThumb:Object 
		
		public function Presentation()
		{
			
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, showImage)
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ldrError);
			imageArea.buttonMode = false
			imageArea.addEventListener(MouseEvent.MOUSE_DOWN, startThisDrag)
			rama.mouseEnabled = false
			highLight.visible = false
			this.infoBtn.addEventListener(MouseEvent.CLICK, showPresentationInf);
			
		}
		
		public function Unselect()
		{
		}
		
		public function Select()
		{

		}
		
		private function addToPresetnation(e:MouseEvent):void
		{
			stage.dispatchEvent(new CEvent(CEvent.INSERT_THIS, OBJ))
		}
		
		private function showPresentationInf(e:MouseEvent):void
		{
			stage.dispatchEvent(new CEvent(CEvent.SHOW_PRESENTATION_INFO, OBJ))
		}
		
		public function Fill(e, n)
		{
			OBJ = e
			id = e.instance_id
			
			//trace("::::::" , e.slide_ids_csv, JSON.stringify(e))
			var myFormat:TextFormat = numSlides.getTextFormat(0, 1);
			myFormat.font = "Lato Italic"
			myFormat.size = "10"
			var thumbID 
			
			if (e.slide_ids_csv != null && e.slide_ids_csv != '')
			{
				
				var a:Array = String(OBJ.slide_ids_csv).split(",")
				var r:URLRequest = new URLRequest();	
				OBJ.slide_ids = a
				if (OBJ.slide_ids == undefined) {
					OBJ.slide_ids = []
					}
					
					
					
					
				if (a[0].indexOf('V') > -1) {
					thumbID = String(a[0]).substring(1)
					r.url = Globals.baseURL + Globals.videothumbURL + thumbID +'.jpg'
					//trace("Load Video Slide" , r.url)
					loader.load(r)
						
					}
				else {
					thumbID = String(a[0])
					r.url = Globals.baseURL + Globals.thumbURL + thumbID +'.jpg'
					//trace("Load IMG Slide" , r.url)
				
					loader.load(r)
					}
			
			
			
				
			}
			else {
				OBJ.slide_ids = []
				
				
				}
				numSlides.text = OBJ.slide_ids.length + "   " +  OCP.LG.slidesInPresentationT
				numSlides.setTextFormat(myFormat, 3, String(numSlides.text).length);
			
			//trace("SSSLIDE IDS" , OBJ.slide_ids)
			title.text = unescape(unescape(decodeURI(e.title)))
			OBJ.title = title.text 
			imgDisplay.addToBtn.addEventListener(MouseEvent.CLICK, addToPresetnation)
			imgDisplay.playGroupBtn.addEventListener(MouseEvent.CLICK, playPresentationButton)
			imgDisplay.mouseEnabled = true
			imgDisplay.mouseChildren = true
		}
		
		private function playPresentationButton(e:MouseEvent):void
		{
			
		var obj:Array = []
		trace(332423 , OBJ.slide_ids)
		for (var i:int = 0; i < OBJ.slide_ids.length; i++) 
		{
			obj.push (OBJ.slide_ids[i])
		}	
		stage.dispatchEvent(new CEvent(CEvent.PLAY_THIS_PRESENTATION, 	[obj, 0, "TITLU PC"]))
		
		}
		
		private function startThisDrag(e:MouseEvent):void
		{
			dispatchEvent(new Event(CEvent.DRAG_THIS_IN))
			
		}
		
	
		
		private function showDeleeteResponce(e:Event):void
		{
			
			stage.dispatchEvent(new Event(CEvent.REFRESH_GROUPS))
		}
		
		private function ldrError(e:Event):void{}
		public function Reset(e = null)
		{
			TweenMax.killTweensOf(this)
		}
		
		private function showImage(e:Event):void
		{
			img = Bitmap(loader.content)
			img.smoothing = true
			//img.width = imageArea.width
			//img.height = imageArea.height
			DisplayUtils.fitIntoRect(img, new Rectangle(0, 0, imageArea.width, imageArea.height), false)	
			imageArea.addChild(img)
			addChild(imgDisplay)
			this.cacheAsBitmap = true
		}
	
	}
}