package thumbs
{
	import com.greensock.*;
	import controllers.DriveController;
	import data.DriveController;
	import data.Uploader;
	import fl.transitions.easing.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.net.*;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import popups.DeleteFilePop;
	import popups.DeleteFolderPop;
	import uk.soulwire.utils.display.DisplayUtils;
	
	public class File extends driveFileLIB implements IThumb
	{
		private var loader:Loader = new Loader();
		private var img:Bitmap = new Bitmap();
		private var twSpeed:Number = .2;
		private var delPresPop:DeleteFilePop;
		private var _fileRef:FileReferenceList;
		private var delFolder:DeleteFolderPop;
		public var idDragging:Boolean = false;
		public var id:int
		public var isVideo:Boolean = false;
		public var OBJ:Object;
		public var hotZone:Sprite
		public var isFolder:Boolean = false;
		
		public function File()
		{
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, showImage)
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ldrError);
			imageArea.buttonMode = false
			dr.addEventListener(MouseEvent.MOUSE_DOWN, startThisDrag)	
			TextField(numSlides).backgroundColor = 0xA44091;
			TextField(numSlides).border = false
			folderIcon.visible = false
			
			editMenu.updateBtn.addEventListener(MouseEvent.CLICK, updateImage)
			editMenu.delBtn.addEventListener(MouseEvent.CLICK, deletThis)
		}
		
		private function updateImage(e:MouseEvent):void 
		{
			_fileRef = new FileReferenceList();
			_fileFilter = new FileFilter("Image", "*.jpeg;*.jpg;*.png;*.avi;*.mov;*.mpg;*.mp4;*.flv;*.wma;*.wmv;");
			_fileRef.browse([_fileFilter]);
			_fileRef.addEventListener(Event.SELECT, _onImageSelect);
		}
		
		private function _onImageSelect(e:Event):void 
		{	
			Uploader.getInstance().updateFileReference(_fileRef.fileList[0], OBJ.id);
		}

		public function Edit(e:Boolean)
		{
			if (e)
			{
				TextField(numSlides).type = TextFieldType.INPUT
				numSlides.setSelection(0, numSlides.text.length)
				addChild(numSlides);
				addChild(editMenu);
			}
			else
			{
				TextField(numSlides).type = TextFieldType.DYNAMIC
				numSlides.setSelection(0, 0)
				addChild(dr);
			}
			
			editMenu.visible = e	
		
			TextField(numSlides).background = e
		}
		
		public function Select()
		{
		}
		
		public function Fill(e, n)
		{
			OBJ = e
			id = e.id
			isFolder = true
			folderIcon.visible = true
			inc.visible = false
			isVideo = false
		
		
			if (e.thumb_image)
			{
				
				if (e.video_content && e.video_content.thumbnail) {
					loader.load(new URLRequest(Globals.baseURL   + e.video_content.thumbnail))
					}
				else {
					loader.load(new URLRequest(Globals.baseURL   + e.thumb_image))
					}
				trace("Load thumbnail" ,  JSON.stringify(OBJ) )
			}
			if (e.type == 'slideTextImage')
			{
				isFolder = false
				folderIcon.visible = false
			}
			
			else if(e.type == 'slideVideo')
			{
				isVideo = true
				isFolder = false
				addChild(this.inc)
				this.inc.visible = true
				folderIcon.visible = false
			}
			
			else if (e.type == 'folder') {
				isFolder = true
				inc.visible = false
				folderIcon.visible = true
				}
			numSlides.text = decodeURI(e.title)
			if (isVideo) {
				editMenu.updateBtn.visible = false
				
				}
			Edit(DriveController.editPositions)
		}
		
		private function startThisDrag(e:MouseEvent):void
		{
			dispatchEvent(new Event(CEvent.DRAG_THIS_IN))
		}
		
		private function deletThis(e:MouseEvent):void
		{
			if (isFolder) {
				delFolder = new DeleteFolderPop(id, OCP.LG.doyouWantToDeleteFolderT)
				stage.addChild(delFolder)
				OCP.prompt = delPresPop
				}
			else {
				delPresPop = new DeleteFilePop(id,OCP.LG.doyouWantToDeleteFileT)
				stage.addChild(delPresPop)
				OCP.prompt = delPresPop
				}
			
		}
		
		private function ldrError(e:Event):void
		{
		}
		
		public function Reset(e = null)
		{
			TweenMax.killTweensOf(this)
		}
		
		private function showImage(e:Event):void
		{
			img = Bitmap(loader.content)
			img.smoothing = true
			DisplayUtils.fitIntoRect(img, new Rectangle(0, 0, imageArea.width, imageArea.height), false)
			addChild(img)
			addChild(editMenu)
			addChild(inc)
			inc.mouseEnabled= false
			img.y = imageArea.y + imageArea.height - img.height
			this.cacheAsBitmap = true
		}
	
	}
}