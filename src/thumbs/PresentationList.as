package thumbs
{
	import com.greensock.*;
	import data.Data;
	import fl.transitions.easing.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.net.*;
	import flash.text.TextFormat;
	import popups.DeleteFilePop;
	import uk.soulwire.utils.display.DisplayUtils;
	
	public class PresentationList extends presentationListLIB implements IThumb
	{
		
		private var loader:Loader = new Loader();
		private var img:Bitmap = new Bitmap();
		private var twSpeed:Number = .2;
		private var delPresPop:DeleteFilePop;
		public var idDragging:Boolean = false;
		public var id:int
		public var isVideo:Boolean = false;
		public var OBJ:Object;
		public var hotZone:Sprite
		public var isFolder:Boolean = false;
		public static var HiglightedThumb:Object
		
		public function PresentationList()
		{
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, showImage)
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ldrError);
			highLight.addEventListener(MouseEvent.MOUSE_DOWN, startThisDrag)
			highLight.visible = false
			settingsBtn.visible = false
			this.addEventListener(MouseEvent.CLICK, Select)
			numSlides.mouseEnabled = false
			numSlides.width = glow.width + 5
		
		}
		
		private function hideSettings(e:MouseEvent):void
		{
			settingsBtn.visible = false
		}
		
		private function showSettingsIcon(e:MouseEvent):void
		{
			settingsBtn.visible = true
		}
		
		private function addToPresentation(e:MouseEvent):void
		{
		
		}
		
		public function HightLIght()
		{
			var textFormat = new TextFormat()
			if (PresentationList.HiglightedThumb)
			{
				PresentationList.HiglightedThumb.glow.visible = true
				TweenMax.to(PresentationList.HiglightedThumb.numSlides, 0, {tint: 0x3AACBE})
			}
			
			PresentationList.HiglightedThumb = this
			glow.visible = false
			TweenMax.to(numSlides, 0, {tint: 0xA44191})
		}
		
		public function Unselect()
		{
		}
		
		public function Select(e = null)
		{
			HightLIght()
			stage.dispatchEvent(new CEvent(CEvent.LOAD_PRESENTATION_SLIDE, OBJ));
		
		}
		
		public function Fill(e, n, level = 1, parent = null)
		{
			OBJ = e
			id = e.id
			gotoAndStop(level)
			if (e.thumb_image)
			{
				loader.load(new URLRequest(Globals.servicURL + e.thumb_image))
			}
			if (e.type == 'slideTextImage')
			{
				this.inc.visible = false
			}
			else
			{
				isVideo = true
			}
			numSlides.text = unescape(decodeURI(e.title))
		}
		
		private function startThisDrag(e:MouseEvent):void
		{
			dispatchEvent(new Event(CEvent.DRAG_THIS_IN))
		}
		
		private function deletThis(e:MouseEvent):void
		{
			delPresPop = new DeleteFilePop(id)
			stage.addChild(delPresPop)
			OCP.prompt = delPresPop
		}
		
		private function ldrError(e:Event):void
		{
		}
		
		public function Reset(e = null)
		{
			TweenMax.killTweensOf(this)
		}
		
		private function showImage(e:Event):void
		{
			img = Bitmap(loader.content)
			img.smoothing = true
			
			this.cacheAsBitmap = true
		}
	
	}
}