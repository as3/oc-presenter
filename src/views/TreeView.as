package  views
{
	import com.freshplanet.lib.ui.scroll.web.ScrollController;
	import com.greensock.*;
	import data.Data;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import popups.*;
	import thumbs.FolderList;
	
	public class TreeView extends ThumbSlidelib
	{
		
		private var thumbContainer:Sprite = new Sprite();
		private var colums:int = 212312;
		private var rows:int = 3;
		private var spacingX:int = 7;
		private var spacingY:int = 2;
		private  var thiles:Array = []
		private var tweenLength:Number = 0
		private var currentPage:Number = 0
		private var totalPages:Number = 0;
		private var thumbsPerPage:Number = 0;
		private var dotContainer:Sprite =new Sprite();
		private var dots:Array = [];
		private var mousePont:Point;
		private var insertPosition;	
		private var dragableHome:Point;	
		private var lastFilped:Sprite;
		private var _b:Bitmap;	
		private var dwidth;
		private var dheight;
		private var _th;
		private var _tw;
		private var bkg:Sprite;
		private var _insertLine:BlueLine = new BlueLine()
		private var _editPosition:Boolean = false;
		private var _scale:Number;
		private var _orientation:String = '';
		private var _dragedThumbnail:MovieClip;
		private var _selectedThumbnail:MovieClip;
		public static var focusedGrid:TreeView 
		private var level:int = 1;
		private var arrowSize:Number;
		private var _showArrows:Boolean = true;
		private var _alignVertical:String = 'center';
		private var _alignHorizontal:String = 'left';
		private var _scroll:ScrollController;
		private var nub:Nub = new Nub()
		private var _leftSpacing:Number = 14;
		
		public function TreeView( showBkg:Number = undefined , alpha:Number = undefined, editPos:Boolean = false, orient:String = 'vertical',
								  scale:Number = 1, showArrows:Boolean = true, alignVertical = 'top', sortAlphabeticly:Boolean = false, showBackground:Boolean = true) 
		{
			orientation = orient
			this.addEventListener(MouseEvent.ROLL_OVER , setFocusOnThis)
			_scale = scale
			addEventListener(Event.ADDED_TO_STAGE , init)
			editPosition = editPos
			_alignVertical = alignVertical
		
			bkg = new Sprite()
			bkg.graphics.beginFill(0x313138)
			bkg.graphics.drawRect(0, 0, 100, 100)
			bkg.graphics.endFill()

			if (showBackground == false) {
				bkg.visible = false
				}
			
			bkg.mouseEnabled = false
			addChild(bkg);	
		}

		private function setFocusOnThis(e:MouseEvent):void 
		{
			TreeView.focusedGrid = this
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addChild(dotContainer)
			addChild(thumbContainer)
			addChild(insertLine)
			insertLine.visible = false
			this.tabChildren = false
			_scroll = new ScrollController();
			stage.addEventListener(MouseEvent.MOUSE_WHEEL , doScroll)
			nub.visible = false
			addChild(nub)
			
		}
		
		private function doScroll(e:MouseEvent):void 
		{
			if (bkg.hitTestPoint(stage.mouseX, stage.mouseY)) {
				_scroll.setCurrentScrollPosition(_scroll.getCurrentScrollPosition() - e.delta*12)
			}
			
		}

		public function showInserLocation():void {
			stage.addEventListener(MouseEvent.MOUSE_MOVE , calculateInsertPosition)
			}
	
			
		public function createThumbs( th:Object, groups:Object,  dragables:Boolean = false, 
									  position:int = 0, showEdit:Boolean = false, parent:Sprite = null) {
			stage.removeEventListener(MouseEvent.MOUSE_MOVE , calculateInsertPosition)
			var	localObjects:Object = new Object()
				editPosition = dragables	
				
				
			for (var i:int = 0; i < groups.length; i++) 
			{
				
			var thumb:MovieClip = MovieClip(new th());
				thumb.addEventListener(CEvent.LOAD_SLIDE, highGhligh)
				thumb.addEventListener(CEvent.DRAG_THIS_IN, checkDragability)
				thumb.addEventListener(CEvent.DELETE_THILE , deletThumb)
				
				thumb.Fill(groups[i], i, level, parent)
				
				Thiles.push(thumb)
				thumbContainer.addChild(thumb)
				level++;
				if (groups[i].child_pages && groups[i].child_pages.length > 0) {
					
					createThumbs(th, groups[i].child_pages, dragables, Thiles.length, showEdit,thumb )
					}
					level--;
			}
			
			stage.addEventListener(MouseEvent.MOUSE_UP , removeDrag)
			_th = new th().height
			_tw = new th().width
		    createGrid(dwidth, dheight)
		    insertLine.insertPosition  = Thiles.length 
		}  
		
		private function deletThumb(e):void 
		{
			thumbContainer.removeChild(e.currentTarget)
			Thiles.splice(Thiles.indexOf(e.currentTarget), 1)	
			createGrid(dwidth, dheight)
		
		}
		
		public function unHighLight() {
				if (selectedThumbnail) { 
					selectedThumbnail.Unselect() 
					selectedThumbnail = null
					}
				
			}
		public function highGhligh(e:CEvent) {
			if (selectedThumbnail) {
				selectedThumbnail.Unselect()
				}
			selectedThumbnail = e.data
			thumbContainer.y =  -tweenLength * currentPage
			//dispatchEvent(new CEvent(CEvent.SHOW_SLIDE, e.data))
			}
	
			
		private function checkDragability(e):void 
		{
					dragedThumbnail = e.currentTarget
					mousePont = new Point(dragedThumbnail.mouseX, dragedThumbnail.mouseY - this.y)
					dragableHome = new Point(dragedThumbnail.x, dragedThumbnail.y)
					thumbContainer.setChildIndex(dragedThumbnail , thumbContainer.numChildren - 1)
					dragedThumbnail.mouseChildren = false
					dragedThumbnail.mouseEnabled = false
					stage.addEventListener(MouseEvent.MOUSE_MOVE , moveDragg)
					
					var p:Point  = dragedThumbnail.localToGlobal(new Point(dragedThumbnail.mouseX, dragedThumbnail.mouseY))
					dragedThumbnail.x = p.x - dragedThumbnail.mouseX
					dragedThumbnail.y = p.y - dragedThumbnail.mouseY
					stage.addChild(dragedThumbnail)
					stage.dispatchEvent(new CEvent(CEvent.DRAG_THIS_IN, this))
		}
		
		private function moveDragg(e:MouseEvent):void 
		{
			dragedThumbnail.x =  stage.mouseX  -  mousePont.x 
			dragedThumbnail.y =  stage.mouseY -  mousePont.y  - this.y
			calculateInsertPosition()
			e.updateAfterEvent()
		}
		
		private function removeDrag(e:MouseEvent):void 
		{
			
			stage.removeEventListener(MouseEvent.MOUSE_MOVE , calculateInsertPosition)
			stage.removeEventListener(MouseEvent.MOUSE_MOVE , moveDragg)
			
			
				if ( dragedThumbnail && Thiles.indexOf(dragedThumbnail) > -1 ) {
					
					
					thumbContainer.addChild(dragedThumbnail)
					var keep = dragedThumbnail 
					dragedThumbnail.mouseChildren = true
					dragedThumbnail.mouseEnabled = true
					dragedThumbnail = null
					keep.Select()
					keep.x = dragableHome.x
					keep.y = dragableHome.y
					return
					
					}

				if (dragedThumbnail && Thiles.indexOf(dragedThumbnail) == -1 ) {
				
						for (var i:int = 0; i < Thiles.length; i++) 
						{
							
							
							
							if (Thiles[i].settingsBtn.visible) {
								
								Data.getInstance().changeParentPage(dragedThumbnail.id, Thiles[i].id)
								dragedThumbnail.mouseChildren = true
								dragedThumbnail.mouseEnabled = true
								
								dragedThumbnail = null
								return
								}
						}
					
					}
					return
			
		}
		
		private function calculateInsertPosition(e=null):void 
		{
			if (bkg.mouseX < 100 && bkg.mouseX > 0 &&
				bkg.mouseY < 100 && bkg.mouseY > 0&&  x == 0 ) 
				{
					if (thiles.length > 1) {
						 insertLine.visible = true
						}
					   
						var  xPos =   Math.round(bkg.mouseX / 100 * colums  ) 
						var  yPos =   Math.floor(bkg.mouseY / 100 * rows)
						var gridPosition =  yPos * colums  +     xPos
						var insertPosition:int
						var _x:Number
						var _y:Number
						
						if ( colums  == 1 ) { 
							xPos = 0
							yPos =   Math.round(bkg.mouseY / 100 * rows)
							insertLine.gotoAndStop(1)
							insertPosition =  currentPage * thumbsPerPage + yPos
							 _x = (_tw + spacingX) * xPos 
							 _y = (_th + spacingY) * yPos 
							insertLine.x =  thumbContainer.x +(_tw/2 + spacingX)
							insertLine.y = _y + arrowSize  +  spacingY/2
						}
						else {
							 insertLine.gotoAndStop(2)
							 insertPosition =  currentPage * thumbsPerPage + gridPosition
							 _x = (_tw + spacingX) * xPos 
							 _y = (_th + spacingY) * yPos
							
								insertLine.x = _x + thumbContainer.x -  spacingX/2
								insertLine.y = _y + arrowSize + _th / 2 +  spacingY / 2	
							

						}
						insertLine.insertPosition = insertPosition 
						}
					else {
						insertLine.visible = false
						}
		}

			
		private function showBitmap():void 
			{
				thumbContainer.mouseEnabled = false
				thumbContainer.mouseChildren = false
				thumbContainer.cacheAsBitmap = true
				thumbContainer.opaqueBackground = true
			}
		
	
		private function createGrid(w:Number , h:Number, tween:Boolean = false):void 
		{
			
			TweenMax.killChildTweensOf(thumbContainer)
			if(Thiles.length == 0) return
			if (w < _tw || h < _th) return
			var xMulti:int = 0
			var currentRow:int = 0
			
			for (var j:int = 0; j < Thiles.length ; j++) 
			{
				var rows:Number = Math.floor( w / (_tw + spacingX) )
				
				var thumb = Thiles[j]
				thumb.Reset(j+1)
				
				if( j != 0 &&  j %(rows) == 0 ){
					currentRow ++ 
					xMulti = 0
				}	
				
				if (j > 0 && j % thumbsPerPage  == 0) {
					xMulti = 0
					
					}
				var _x = (_tw + spacingX) * xMulti 
				var _y = (_th + spacingY) * currentRow;
				
				if (thumb != dragedThumbnail && j >= currentPage * thumbsPerPage && j < currentPage * thumbsPerPage +  thumbsPerPage && tween) {
						TweenMax.to( thumb, .6, { x:_x, y:_y } )
						
					}
				else if( thumb != dragedThumbnail){
					thumb.x = _x
					thumb.y = _y
					}
				xMulti++		
			}
			
				thumbContainer.y = ( -tweenLength )
				thumbContainer.x = _leftSpacing
				nub.nub.height = dheight * ( dheight /thumb.y )
				_scroll.addScrollControll(thumbContainer, this,  new Rectangle(0, 0, w,  h), nub);
		}
	
			
		public function Resize(w:Number , h:Number ) {
				nub.x = w - 10// +5
				dwidth = w 
				dheight = h  
				bkg.width = w
				bkg.height = h
				createGrid(dwidth , dheight)
				
			}		

		public function Clear(e =null):void 
			{
					TweenMax.killChildTweensOf(thumbContainer)
				if(stage){
					stage.removeEventListener(MouseEvent.MOUSE_UP , removeDrag)
					}
				while (dotContainer.numChildren){dotContainer.removeChildAt (0)}
					
					Thiles = []
					while (thumbContainer.numChildren){thumbContainer.removeChildAt (0)}
					currentPage = 0
					totalPages = 0 		
			}
			//========= Getter Setters======================
			public function get Thiles():Array 
			{
				return thiles;
			}
			
			public function set Thiles(value:Array):void 
			{
				thiles = value;
			}
			
			public function get insertLine():BlueLine 
			{
				return _insertLine;
			}
			
			public function set insertLine(value:BlueLine):void 
			{
				_insertLine = value;
			}
			
			public function get editPosition():Boolean 
			{
				return _editPosition;
			}
			
			public function set editPosition(value:Boolean):void 
			{
				_editPosition = value;
			}
			
			public function get orientation():String 
			{
				return _orientation;
			}
			
			public function set orientation(value:String):void 
			{
				_orientation = value;
			}
			
			public function get dragedThumbnail():MovieClip 
			{
				return _dragedThumbnail;
			}
			
			public function set dragedThumbnail(value:MovieClip):void 
			{
				_dragedThumbnail = value;
			}
			
			public function get selectedThumbnail():MovieClip 
			{
				return _selectedThumbnail;
			}
			
			public function set selectedThumbnail(value:MovieClip):void 
			{
				_selectedThumbnail = value;
			}
			
	}

}