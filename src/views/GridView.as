package  views
{
	import com.greensock.*;
	import com.greensock.layout.AlignMode;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import popups.*;
	import thumbs.MySlide;
	import thumbs.Slide;
	
	public class GridView extends ThumbSlidelib
	{
		
		private var thumbContainer:Sprite = new Sprite();
		private var colums:int = 212312;
		private var rows:int = 3;
		private var spacing:int = 10;
		private var arrPrev:Sprite  = new ArrowLeft();
		private var arrNext:Sprite = new ArrowRight();
		private  var thiles:Array = []
		private var tweenLength:Number = 0
		private var currentPage:Number = 0
		private var totalPages:Number = 0;
		private var thumbsPerPage:Number = 0;
		private var dotContainer:Sprite =new Sprite();
		private var dots:Array = [];
		private var mousePont:Point;
		private var yOffset:Number;
		private var xOffset:Number;
		private var insertPosition;	
		private var dragableHome:Point;	
		private var theMask:Sprite 
		private var dwidth;
		private var dheight;
		private var _th;
		private var _tw;
		private var bkg:Sprite;
		private var _insertLine:BlueLine = new BlueLine()
		private var _editPosition:Boolean = true;
		private var _scale:Number;
		private var _orientation:String = '';
		private var _dragedThumbnail:MovieClip;
		private var _selectedThumbnail:MovieClip;
		public static var focusedGrid:GridView 
		
		private var arrowSize:Number;
		private var _showArrows:Boolean = true;
		private var _alignVertical:String = 'center';
		public function GridView( showBkg:Number = undefined , alpha:Number = undefined, editPos:Boolean = true, orient:String = 'vertical',
							scale:Number = 1, showArrows:Boolean = true, alignVertical = 'top', sortAlphabeticly:Boolean = false) 
		{
			
			orientation = orient
			this.addEventListener(MouseEvent.ROLL_OVER , setFocusOnThis)
			_scale = scale
			addEventListener(Event.ADDED_TO_STAGE , init)
			_alignVertical = alignVertical
				
			bkg = new Sprite()
			bkg.graphics.beginFill(0x666622)
			bkg.graphics.drawRect(0, 0, 100, 100)
			bkg.graphics.endFill()
			bkg.visible = false
			bkg.mouseEnabled = false
			addChild(bkg);	

			if (!showArrows) {
				arrPrev = new Sprite()
				arrNext = new Sprite();
				_showArrows = showArrows
				}
		}

		private function setFocusOnThis(e:MouseEvent):void 
		{
			GridView.focusedGrid = this
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addChild(dotContainer)
			addChild(thumbContainer)
			theMask = new Sprite()
			theMask.graphics.beginFill(0xffffff)
			theMask.graphics.drawRect(0, 0, 100, 100)
			theMask.graphics.endFill()
			addChild(_insertLine)
			_insertLine.visible = false
			
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, TraceWeel)
			arrPrev.addEventListener(MouseEvent.CLICK, arrowPrevDown, false, 0, true)
			arrPrev.addEventListener(MouseEvent.ROLL_OVER, navPREV, false, 0, true)
			addChild(arrPrev)
			arrNext.addEventListener(MouseEvent.CLICK, arrowNextDown, false, 0, true)
			arrNext.addEventListener(MouseEvent.ROLL_OVER, navNEXT, false, 0, true)
			addChild(arrNext)
			
			arrowSize = arrPrev.height;
		
			arrPrev.visible = false
			arrNext.visible = false
			this.tabChildren = false
		}

		
		private function navNEXT(e:MouseEvent):void 
		{
			
			if (dragedThumbnail && !TweenMax.isTweening(thumbContainer)) {
				dots[currentPage].pink.visible = false
				currentPage +=  1	
				var dest:Number = arrowSize +  currentPage  * -tweenLength
				tweenThumbContainer(dest)
				thumbContainer.mouseEnabled = false
				thumbContainer.mouseChildren = false
				}
		}
		
		private function navPREV(e:MouseEvent):void 
		{
			if (dragedThumbnail && !TweenMax.isTweening(thumbContainer)) {
				dots[currentPage].pink.visible = false
				currentPage -=  1	
				var dest:Number = arrowSize +  currentPage  * -tweenLength
				tweenThumbContainer(dest)
				thumbContainer.mouseEnabled = false
				thumbContainer.mouseChildren = false
				}
		}
		
		private function tweenThumbContainer(dest:Number):void 
		{
				if (orientation == 'horizontal') {
					TweenMax.to(thumbContainer, .7, { x: dest, onComplete:showDots } )
					}
				else {
					TweenMax.to(thumbContainer, .7, { y: dest, onComplete:showDots } )
					}
		}
		
				
		private function arrowPrevDown(e:MouseEvent):void 
		{	
			if (currentPage > 0) {
					if(currentPage-1==0) arrPrev.visible = false
					arrNext.visible = true
					dots[currentPage].pink.visible = false
					currentPage--
					tweenThumbContainer(arrowSize +  currentPage * - tweenLength)
				}
			
			}
			
	
			
		private function arrowNextDown(e:MouseEvent):void 
		{
				if (currentPage < totalPages - 1) {
					if(currentPage+1==totalPages-1) arrNext.visible = false
					arrPrev.visible = true
					dots[currentPage].pink.visible = false
					currentPage++
					tweenThumbContainer(arrowSize + currentPage * -tweenLength)
					}
		}
		
		
		private function TraceWeel(e:MouseEvent):void 
		{
			if(!TweenMax.isTweening(thumbContainer) && this.hitTestPoint( stage.mouseX,  stage.mouseY) && GridView.focusedGrid == this) {
			if ( e.delta == -3 ){
				focusedGrid.arrowNextDown(e)
				}
			else {
				arrowPrevDown(e)
				}
			}
			e.stopPropagation()
		}
		
		public function showInserLocation():void {
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE , calculateInsertPosition)
			}
	
		public function createThumbs( th:Object , groups:Object,  dragables:Boolean = false, position:int = -1) {
			
			
			stage.removeEventListener(MouseEvent.MOUSE_MOVE , calculateInsertPosition)
			if (position == -1) {
				position = _insertLine.insertPosition
				}
			for (var i:int = 0; i < groups.length; i++) 
			{
				
			var thumb:MovieClip = MovieClip(new th());
				thumb.addEventListener(CEvent.LOAD_SLIDE, highGhligh)
				thumb.addEventListener(CEvent.DRAG_THIS_IN, checkDragability)
				thumb.addEventListener(CEvent.DELETE , deletThumb)
				
				thumbContainer.addChild(thumb);
				thumb.Fill(groups[i], i)
					
				
				Thiles.splice(position +i , 0, thumb)
				
				
			}
			
			stage.addEventListener(MouseEvent.MOUSE_UP , removeDrag)
			if (Thiles.length > 0 ) {
				_th = Thiles[0].height
				_tw = Thiles[0].width
				createGrid(dwidth, dheight)
				_insertLine.insertPosition  = Thiles.length 
				}
			
		}
		
		private function deletThumb(e):void 
		{
			thumbContainer.removeChild(e.currentTarget)
			Thiles.splice(Thiles.indexOf(e.currentTarget), 1)	
			createGrid(dwidth, dheight)
		}
		
		public function unHighLight() {
				if (selectedThumbnail) { 
					selectedThumbnail.Unselect() 
					selectedThumbnail = null
					}
				
			}
		public function highGhligh(e:CEvent) {
			if (selectedThumbnail) {
				selectedThumbnail.Unselect()
				}
			selectedThumbnail = e.data
			//if (orientation == 'horizontal') {
				//thumbContainer.x = arrowSize + ( -tweenLength * currentPage)
				//thumbContainer.y = 0
				//}
			//else {
				//thumbContainer.y = arrowSize + ( -tweenLength * currentPage)
				//thumbContainer.x = 0
				//}
			dispatchEvent(new CEvent(CEvent.LOAD_SLIDE, e.data))
			}
		public function getNextThumb() {
			
			if (selectedThumbnail) {
				var idx = Thiles.indexOf(selectedThumbnail)
				
				if ( idx < Thiles.length -1 && idx > -1 ) {
					selectedThumbnail.Unselect()
					selectedThumbnail = Thiles[idx+1]
					selectedThumbnail.Select("JUSTPLAY")
					}
				if ((idx + 1) % thumbsPerPage == 0  && idx != -1) {
						arrowNextDown(null)
						}
				else if ( idx == 0) {
					arrPrev.visible = false
					arrNext.visible = true
					dots[0].pink.visible = false
					currentPage = 0
					tweenThumbContainer(0)
					//tweenThumbContainer(arrowSize +  1 * tweenLength)
					}
				}
		}
		
		public function getPrevThumb():void 
			{
				

				if (selectedThumbnail) {
				var idx = Thiles.indexOf(selectedThumbnail)
				if (  idx > 0 ) {
					selectedThumbnail.Unselect()
					selectedThumbnail = Thiles[idx-1]
					selectedThumbnail.Select("JUSTPLAY")
					}
				}
				if ((idx ) % thumbsPerPage == 0  && idx - 1 > 0) {
						arrowPrevDown(null)
						}
			}
			
			
		private function checkDragability(e):void 
		{
			trace("DRAGGING")
			dragedThumbnail = e.currentTarget
		
			mousePont = new Point(dragedThumbnail.mouseX, dragedThumbnail.mouseY - this.y)
			dragableHome = new Point(dragedThumbnail.x, dragedThumbnail.y)
			thumbContainer.setChildIndex(dragedThumbnail , thumbContainer.numChildren - 1)
			dragedThumbnail.mouseChildren = false
			dragedThumbnail.mouseEnabled = false
			stage.addEventListener(MouseEvent.MOUSE_MOVE , moveDragg)
			
			var p:Point  = dragedThumbnail.localToGlobal(new Point(dragedThumbnail.mouseX, dragedThumbnail.mouseY))
			dragedThumbnail.x = p.x - dragedThumbnail.mouseX
			dragedThumbnail.y = p.y - dragedThumbnail.mouseY
			stage.addChild(dragedThumbnail)
		
			//if (!editPosition ) {
				//stage.dispatchEvent(new CEvent(CEvent.DRAG_THIS_IN, this))
			//}
		}
		
		private function moveDragg(e:MouseEvent):void 
		{
			dragedThumbnail.x =  stage.mouseX  -  mousePont.x 
			dragedThumbnail.y =  stage.mouseY -  mousePont.y  - this.y
			dragedThumbnail.alpha = .5
			calculateInsertPosition()
			e.updateAfterEvent()
		}
		
		private function removeDrag(e:MouseEvent):void 
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE , calculateInsertPosition)
		
			
			if (name == 'TIMELINE') {
				//trace("addChild file to timeling")
			}
			
			if (dragedThumbnail && Thiles.indexOf(dragedThumbnail) > -1 ) {
				
				stage.removeEventListener(MouseEvent.MOUSE_MOVE , moveDragg)
				dragedThumbnail.alpha = 1
				dragedThumbnail.mouseChildren = true
				dragedThumbnail.mouseEnabled = true
				
				var _X = thumbContainer.localToGlobal(dragableHome).x
				var _Y = thumbContainer.localToGlobal(dragableHome).y
				var _tolerance:Number = 3
				trace(Math.abs(_X-dragedThumbnail.x), "X difference")
				if (Math.abs(_X-dragedThumbnail.x) <  _tolerance &&
					Math.abs(_Y-dragedThumbnail.y) <  _tolerance ) {
					dragedThumbnail.x = dragableHome.x
					dragedThumbnail.y = dragableHome.y
					thumbContainer.addChild(dragedThumbnail)
					var keep = dragedThumbnail 
					dragedThumbnail = null
					trace("IN")
					
					keep.Select("JUSTPLAY")
						_insertLine.visible = false
					
					return
					}
					
				if (dragedThumbnail.OBJ) {
					dispatchEvent(new CEvent(CEvent.ADD_THIS_SLIDE,	dragedThumbnail.OBJ ))
					
					}
				else{
					dispatchEvent(new CEvent(CEvent.CEVENT,	dragedThumbnail.id ))
				}
				
					var dragablePosition = Thiles.indexOf(dragedThumbnail)
				if (_insertLine.visible ) {
					if ( dragablePosition < _insertLine.insertPosition) {
						 _insertLine.insertPosition -= 1 
						}
					var th = 	Thiles.splice(dragablePosition, 1);
						
						Thiles.splice(_insertLine.insertPosition, 0, dragedThumbnail )
						var p:Point  = thumbContainer.globalToLocal(new Point(dragedThumbnail.x, dragedThumbnail.y))
						dragedThumbnail.x = p.x 
						dragedThumbnail.y = p.y
						thumbContainer.addChild(dragedThumbnail)
						dragedThumbnail = null
						createGrid( dwidth , dheight, true)
						
						
					}
				else {
					
					dragedThumbnail.x = dragableHome.x
					dragedThumbnail.y = dragableHome.y
					thumbContainer.addChild(dragedThumbnail)
					dragedThumbnail = null
					createGrid(dwidth, dheight)
					}			
			}
			
				_insertLine.visible = false
		}
		
		private function calculateInsertPosition(e=null):void 
		{
		
			if (editPosition && bkg.mouseX < 100 && bkg.mouseX > 0 &&
				bkg.mouseY < 100 && bkg.mouseY > 0 ) 
				{
					
					if (thiles.length > 1) {
						 _insertLine.visible = true
						}
						
						
						var  xPos =   Math.round(bkg.mouseX / 100 * colums  )
						var  yPos =   Math.floor(bkg.mouseY / 100 * rows)
						var gridPosition =  yPos * colums  +     xPos
						var insertPosition:int
						var _x:Number
						var _y:Number
						if ( colums  == 1 ) { 
							xPos = 0
							yPos =   Math.round(bkg.mouseY / 100 * rows)
							_insertLine.gotoAndStop(1)
							insertPosition =  currentPage * thumbsPerPage + yPos
							 _x = (_tw + spacing) * xPos + xOffset;
							 _y = (_th + spacing) * yPos + yOffset;
							_insertLine.x =  thumbContainer.x + xOffset +(_tw/2 )
							 if (orientation == 'horizontal') {
								
								_insertLine.y = _y  +  spacing/2	
								 }
							else {
								_insertLine.y = _y + _th / 2  +  spacing / 2
								
								if (!arrNext.visible && totalPages ==1) {
									
									_insertLine.y -= arrNext.height +  _th/2 + spacing/2 
									trace("AICI ASA")
								}
								
							} 
						}
						else {
							 _insertLine.gotoAndStop(2)
							 insertPosition =  currentPage * thumbsPerPage + gridPosition
							 _x = (_tw + spacing) * xPos + xOffset;
							 _y = (_th + spacing) * yPos + yOffset;
							 if (orientation == 'horizontal') {
								 _insertLine.x = _x + arrowSize-  spacing/2
								 _insertLine.y = _y  + _th / 2 //+  spacing / 2	
								 }
							else {
								_insertLine.x = _x + thumbContainer.x -  spacing/2
								_insertLine.y = _y + arrowSize + _th / 2-  spacing / 2	
							}

						}
						_insertLine.insertPosition = insertPosition 
					
						
				
						}
					else {
						
						_insertLine.visible = false
						}
					
						
		}
		
		
		
	
			
		private function showBitmap():void 
			{
				thumbContainer.mouseEnabled = false
				thumbContainer.mouseChildren = false
				thumbContainer.cacheAsBitmap = true
				thumbContainer.opaqueBackground = true
		
			}
		
		
		private function createGrid(w:Number , h:Number, tween:Boolean = false):void 
		{
			
			TweenMax.killChildTweensOf(thumbContainer)
			
			if(Thiles.length == 0) return
			if (w < _tw || h < _th) return
			
			
			
			var calcH:Number = Math.floor(h / (_th + spacing))
			var calcW:Number = Math.floor(w / (_tw + spacing))

		
			rows = Math.floor(calcH)
			thumbsPerPage = calcW * rows
			
			arrowSize = 0;
			totalPages =  Math.ceil(Thiles.length / thumbsPerPage)
			
			
			
				
			if (totalPages > 1) {	
				arrowSize = 50;
				if (orientation == 'horizontal') { 
							w = dwidth - arrowSize * 2
						}
						else {
							h = dheight -  arrowSize * 2
							
						}
						
						
						 calcH = Math.floor(h / (_th + spacing))
			 calcW = Math.floor(w / (_tw + spacing))

		
			rows = Math.floor(calcH)
			thumbsPerPage = calcW * rows
						
			
		
				
				}
				else {
					currentPage = 0
					}
					
					
					
				var firstItem:int = currentPage * thumbsPerPage
				colums = Math.ceil(thumbsPerPage / rows)
				
				
				
		 if (totalPages > 150 || totalPages < 0) return
		 	if (orientation == 'horizontal' ) {
				yOffset = ( h - (_th + spacing) * calcH) / 2 + spacing/2
				xOffset = ( w - (_tw + spacing) * calcW) / 2 + spacing/2
				}
			else {
				yOffset = ( h - (_th + spacing) * calcH) / 2 + spacing/2
				xOffset = ( w - (_tw + spacing) * calcW) / 2 + spacing/2
				}
			//yOffset = 0;
			var extraSpace  = 0
			var pageIntervl  = yOffset + arrowSize + 200
			var currentRow =  0 
			var xMulti = 0 
			
			
			
			for (var j:int = 0; j < Thiles.length ; j++) 
			{
				
				var thumb = Thiles[j]
				
				thumb.Reset(j+1)
				
				if( j != 0 &&  j %(thumbsPerPage/rows) == 0 ){
					currentRow ++ 
					xMulti = 0
				}	
				
				if (j > 0 && j % thumbsPerPage  == 0) {
					extraSpace++
					if (orientation == 'horizontal') {currentRow = 0}
					xMulti = 0
					
					}
					
					  	var _x =  (_tw + spacing) * xMulti + xOffset;
						var _y =  (_th + spacing) * currentRow + (extraSpace * pageIntervl) + yOffset;
						if (orientation == 'horizontal') {	
							pageIntervl  = xOffset + arrowSize + stage.stageWidth
							
							 _x =  (_tw + spacing) * xMulti + xOffset + (extraSpace * pageIntervl);
							 
							 _y = (_th + spacing) * currentRow  + yOffset;
						  }
						if (thumb != dragedThumbnail && j >= currentPage * thumbsPerPage && j < currentPage * thumbsPerPage +  thumbsPerPage && tween) {
								TweenMax.to( thumb, .6, { x:_x, y:_y } )
								
							}
						else if( thumb != dragedThumbnail){
							thumb.x = _x
							thumb.y = _y
						
							}
						if (firstItem == j) {
							currentPage = extraSpace
							}
						
					    xMulti++	
						
			}
		
			
			var thX = _tw / 2 + xOffset
			var thY = _th / 2 + yOffset
			
			
			if (orientation == 'horizontal') {
				tweenLength =  pageIntervl
			
				//thumbContainer.x = arrowSize + ( -tweenLength * currentPage) + xOffset
				thumbContainer.x =arrowSize + currentPage * -tweenLength
				
				
				thumbContainer.y = 0
				bkg.x = thX - _tw/2 + arrowSize
				bkg.y = thY - _th / 2 
				bkg.width  =  ( _tw + spacing) * colums - spacing
				bkg.height =  dheight - yOffset * 2
				}
			else {
				tweenLength = calcH * (_th + spacing) + pageIntervl
				thumbContainer.y = arrowSize + ( -tweenLength * currentPage)
				thumbContainer.x = 0
				bkg.x = thX - _tw/2 
				bkg.y = thY - _th / 2 + arrowSize
				bkg.width  =  dwidth - xOffset * 2
				bkg.height = ( _th + spacing) * rows - spacing
				
				}
				
				
					//trace(thumbContainer.x ,435435435)
			createDots()
			arrangeArrows(thumbContainer)
			
		
			//trace(Thiles[0].x ,thumbContainer.x, this.x, 32423432)
			
		}
		
		
		private function showDots():void 
			{
				thumbContainer.mouseEnabled = true
				thumbContainer.mouseChildren = true
				
				dots[currentPage].pink.visible = true
				arrPrev.visible = true
				
				arrNext.visible = true
				if(currentPage==0) arrPrev.visible = false
				if(currentPage==totalPages-1) arrNext.visible = false	
			}
			
		public function gotToPage(e:MouseEvent):void 
		{
			dots[currentPage].pink.visible = false
			currentPage =  e.currentTarget.id		
			var dest:Number = arrowSize +  e.currentTarget.id  * -tweenLength
			tweenThumbContainer(dest)
			
		}	
		private function createDots():void 
		{

			dotContainer.removeChildren()
			dotContainer.y = 0
			dots = []
			
		
			for (var j:int = 0; j < totalPages; j++) 
			{
				var d:dot = new dot()
					d.addEventListener(MouseEvent.CLICK , gotToPage, false, 0, true)
					d.id = j  
					d.pink.visible = false
					if (orientation == 'horizontal') {
						d.x = j * (d.width + 5) ;
						d.y = 0
						}
					else {
						d.y = j * (d.height + 5) ;
						d.x = 0
						}
					dotContainer.addChild(d)
					dots.push(d)
			}
		
			if (dots.length == 1 ) {
				dotContainer.visible = false
				arrPrev.visible = false
				arrNext.visible = false
				
				}
			else if(dots.length > 0){
				arrPrev.visible = true
				arrNext.visible = true
				if(currentPage==0) arrPrev.visible = false
				if(currentPage==totalPages-1) arrNext.visible = false
				dotContainer.visible = true
				if (dots[currentPage]) {
					dots[currentPage].pink.visible = true
					}
				
				if (orientation == 'horizontal') {
					dotContainer.x =  bkg.width / 2 - dotContainer.width/2 + bkg.x
					dotContainer.y =  dheight- dotContainer.height - 5
					}
				else {
					dotContainer.y =  bkg.height / 2 - dotContainer.height/2 + bkg.y
					dotContainer.x =  dwidth - dotContainer.width+ 5
					}
				
			}
		}
		
	
		
		private function arrangeArrows(e):void 
			{		
			if(!_showArrows) return
			if (orientation == 'horizontal') {
					arrPrev.bkg.height = arrNext.bkg.height =  e.height ;
					arrPrev.bkg.width =  arrNext.bkg.width =  arrowSize
					arrNext.arr.x = arrowSize/2 
					arrPrev.arr.x = arrowSize/2
					arrNext.arr.y = arrPrev.bkg.height / 2 
					arrPrev.arr.y = arrPrev.bkg.height / 2 
					arrNext.arr.rotation = 0
					arrPrev.arr.rotation =  0
					arrNext.x = dwidth- arrowSize
					arrNext.y = thumbContainer.y + yOffset +5
					arrPrev.x = 0
					arrPrev.y = thumbContainer.y + yOffset +5
					
				
				}
			else {
					arrPrev.bkg.width = arrNext.bkg.width =  bkg.width
					arrPrev.bkg.height =  arrNext.bkg.height =  arrowSize
					arrNext.arr.y = arrowSize/2 
					arrPrev.arr.y = arrowSize/2 
					arrNext.arr.x = arrPrev.bkg.width / 2
					arrPrev.arr.x = arrPrev.bkg.width / 2
					arrNext.arr.rotation = 90
					arrPrev.arr.rotation =  90
					arrNext.y = dheight - arrowSize
					arrNext.x = thumbContainer.x + xOffset 
					arrPrev.y = 0
					arrPrev.x = thumbContainer.x + xOffset 
					
				}
			if (Thiles.length == 0) {
					arrPrev.visible = false
					arrNext.visible = false}
			
			}
			
		public function Resize(w:Number , h:Number ) {
			
			currentPage = 0
			if (orientation == 'horizontal') { 
				dwidth = w //- arrowSize *2
				dheight = h  
				}
			else {
				dwidth = w -10 
				
				dheight = h  //-  arrowSize *2
				}
			
			
			
			theMask.width = w
			theMask.height  = h
			addChild(theMask)
			
			createGrid(dwidth , dheight)
			thumbContainer.mask = theMask
			
			
			
			}		
			
		
			
		public function Clear(e =null):void 
			{
					TweenMax.killChildTweensOf(thumbContainer)
				if(stage){
					stage.removeEventListener(MouseEvent.MOUSE_UP , removeDrag)
					}
			
				
				thumbContainer.removeChildren()
				dotContainer.removeChildren()
				
			
				arrPrev.visible = false
				arrNext.visible = false
				
				thiles = []
				
				currentPage = 0
				totalPages = 0 		
				createGrid(dwidth , dheight);
				
				
			}
			//========= Getter Setters======================
			public function get Thiles():Object 
			{
				return thiles;
			}
			
			public function set Thiles(obj:Object):void 
			{
				
				Clear()
				
				createThumbs(MySlide, obj, true, 0)
				createGrid(dwidth, dheight);
				this.visible = true
				
				
			}
		
			
			public function get editPosition():Boolean 
			{
				return _editPosition;
			}
			
			public function set editPosition(value:Boolean):void 
			{
				_editPosition = value;
			}
			
			public function get orientation():String 
			{
				return _orientation;
			}
			
			public function set orientation(value:String):void 
			{
				_orientation = value;
			}
			
			public function get dragedThumbnail():MovieClip 
			{
				return _dragedThumbnail;
			}
			
			public function set dragedThumbnail(value:MovieClip):void 
			{
				_dragedThumbnail = value;
			}
			
			public function get selectedThumbnail():MovieClip 
			{
				return _selectedThumbnail;
			}
			
			public function set selectedThumbnail(value:MovieClip):void 
			{
				_selectedThumbnail = value;
			}
			
	}

}