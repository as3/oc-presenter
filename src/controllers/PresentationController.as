package controllers
{
	import com.hurlant.util.Base64;
	import data.Data;
	import data.Login;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import popups.DeletePop;
	import popups.DeletePresentationPop;
	import popups.MoreMyPresentation;
	import popups.PDFPop;
	import popups.PopUpTimer;
	import popups.SharedPop;
	import thumbs.CEvent;
	import thumbs.MYPresentationList;
	import thumbs.MySlide;
	import views.GridView;
	import views.ListView;
	
	
	public class PresentationController extends presentationController
	{
	
		private var _view:ListView;
		private var timelineHash:String;
		private var currentSlide:Object;
		private var currentPresentation:Object;
		private var loadId:MovieClip;
		private var delPop:DeletePop;
		public static var NumSlides:Array = [];
		public static var DragThumb:Sprite;
		
		private var mPop:MoreMyPresentation = new MoreMyPresentation()
		//private var mPop:menuPopMyPresentation = new menuPopMyPresentation()
		public function PresentationController(view:ListView)
		{
			addEventListener(Event.ADDED_TO_STAGE, init)
			_view = view
			_view.editPosition = true
			_view.name = 'TIMELINE';
			_view.addEventListener(CEvent.LOAD_SLIDE, LoadSlide)
			hideLogedInControls(null)		
		}

		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(CEvent.DRAG_THIS_IN, externalSlideDragging);
			stage.addEventListener(CEvent.CLEAR_TIMELINE, Clear)
			stage.addEventListener(CEvent.DELETE_CURRENT_PRESENTATION, Clear);
			stage.addEventListener(CEvent.LOGED_IN, showControls);
			stage.addEventListener(CEvent.LOAD_MY_PRESENTATIONS_SLIDES, showSlidesInUserPresentation);
			stage.addEventListener(CEvent.INSERT_THIS, addSlidesToPresentation)
			stage.addEventListener(CEvent.ADD_THIS_FOLDER_TO_PRESENTATION, addFolderContent)
			stage.addEventListener(CEvent.SAVE_PRESENTATION , saveForm);
			stage.addEventListener(CEvent.LOGED_OUT, hideLogedInControls)
			
			t.text = OCP.LG.customPresentationT
			t.width = _view.width > 600 ? _view.width : 600
			
		
			
			this.playBtn.addEventListener(MouseEvent.CLICK, startPresenting)
			settingsBtn.addEventListener(MouseEvent.CLICK, showMoreMenu)
			
		
			mPop.Hide()
			addChild(mPop);
		}
		
		private function hideMoreMenu(e:MouseEvent):void 
		{
			mPop.Hide();
		}
		
		private function showMoreMenu(e:MouseEvent):void 
		{
			mPop.Show();
			mPop.x  = settingsBtn.x;
			mPop.y  = settingsBtn.y;
			
		}
	
	//================== Same functions from side menu-=---	

		private function startPresenting(e:MouseEvent):void
		{
			
			if (_view.Thiles.length > 0) {
				_view.Thiles[0].Select("JUSTPLAY")
				}
				
		}
		
		private function addFolderContent(e:CEvent):void 
		{
				
				var a:Array = []
				var s:Array = e.data.groups[0].pages
				for (var i:int = 0; i < s.length ; i++) 
				{	
					if (s[i].type != 'folder') {
						a.push(s[i])
						} 
				}
				_view.createThumbs(MySlide, a, true)
				enableBasicControls()
				PresentationController.NumSlides = _view.Thiles
				
		}	

		private  function formTimeline(e):String {
			var csv:Array = []
			for (var i:int = 0; i < e.length; i++) 
			{
				csv.push( (e[i].isVideo?'V':'' )+ e[i].id )
			}
			
			return  '[' + csv + ']'
		}
		
	
		private function showSlidesInUserPresentation(e:CEvent):void
		{
			_view.Clear()
		
			t.text = unescape(decodeURI(e.data.title))
			var o:Object
			
			 o = e.data.slide_ids
			
			if (o &&  o.length > 0 && o[0] != "")
			{
				var parecebleOBJ:Array = []
				for (var i:int = 0; i < o.length; i++)
				{
					parecebleOBJ.push({"id": o[i], "title": ""})
				}
				_view.createThumbs(MySlide, parecebleOBJ, true, 0)
				enableBasicControls()
				PresentationController.NumSlides = _view.Thiles
			}
		}	
		private function enableBasicControls(e:Event=null) {
			playBtn.mouseEnabled  = true
			playBtn.alpha  = 1
			settingsBtn.mouseEnabled  = true
			settingsBtn.alpha  = 1
			
			}
		private function playPresentation(e:CEvent):void
		{
			_view.Clear()
	
			t.text = unescape(decodeURI(e.data.title))
			var o:Object
			
			 o = e.data.slide_ids
			
			if (o &&  o.length > 0 && o[0] != "")
			{
				var parecebleOBJ:Array = []
				for (var i:int = 0; i < o.length; i++)
				{
					parecebleOBJ.push({"id": o[i], "title": ""})
				}
				_view.createThumbs(MySlide, parecebleOBJ, true, 0)
				enableBasicControls()
				PresentationController.NumSlides = _view.Thiles
			}
			
			StartPresentation(null)
			stage.focus = _view
			
		}
		
		private function addSlidesToPresentation(e:CEvent):void
		{
			var o:Object = e.data.slide_ids
			if (o &&  o.length > 0 && o[0] != "")
			{
				var parecebleOBJ:Array = []
				for (var i:int = 0; i < o.length; i++)
				{
					parecebleOBJ.push({"id": o[i], "title": ""})
				}
			
				_view.createThumbs(MySlide, parecebleOBJ, true,  _view.Thiles.length )
				enableBasicControls()
				PresentationController.NumSlides = _view.Thiles
			}
		}

		
		private function parceUserPresentationsJSON(e:Event):void
		{
			removeListeners()
			var o:Object = JSON.parse(e.currentTarget.content)
			var sortOnID:Array = o.timelines.sortOn("instance_id")
			if(o.status == 'success') {
			 	stage.dispatchEvent(new CEvent(CEvent.SHOWING_ALL_PRESENTATIONS , sortOnID))
			}
		
		}
		
		
		private function saveForm(e:CEvent):void 
		{
			
			Data.getInstance().SavePresentation(e.data[0],  e.data[1], e.data[2], formTimeline(_view.Thiles), e.data[3])
			
		}
		
		public function StartPresentation(e:Event):void
		{
			_view.Thiles[0].Select("JUSTPLAY")	
		}

		private function externalSlideDragging(e:CEvent):void
		{
			trace("DAGING INTO PRES" , e.data , _view)
			if(e.data != _view ) {
				loadId = e.data.dragedThumbnail
				PresentationController.DragThumb = e.data.dragedThumbnail
				stage.addEventListener(MouseEvent.MOUSE_UP, loadGroupInPresentation)
				//_view.editPosition = true
				
				_view.dragedThumbnail = e.data.dragedThumbnail
				_view.showInserLocation()
				
			}
			else {
				//_view.editPosition = false
				}
		}
		
		private  function loadGroupInPresentation(e:MouseEvent):void
		{
			PresentationController.DragThumb = null
			trace("---------", JSON.stringify(loadId.OBJ))
			_view.dragedThumbnail = null
			stage.removeEventListener(MouseEvent.MOUSE_UP, loadGroupInPresentation)
			if (stage.mouseX > _view.x && stage.mouseY > _view.y && 
				stage.mouseX < stage.stageWidth && stage.mouseY < stage.stageHeight){
			
				if (loadId.OBJ.owner || loadId.OBJ.type == 'folder')
				{
					Data.getInstance().GetSlidesInFolder(loadId.OBJ.id, addDirectlyToPresentation);
				}
				else if (loadId.OBJ.slide_ids_csv) {
					
					var o = loadId.OBJ.slide_ids_csv.split(',')
					var parecebleOBJ:Array = []
						for (var i:int = 0; i < o.length; i++)
						{
						
							parecebleOBJ.push({"id": o[i], "title": ""})
						}
				
					_view.createThumbs(MySlide, parecebleOBJ, true,-1)
					enableBasicControls()
					PresentationController.NumSlides = _view.Thiles
					
					} 
				else
				{
					_view.createThumbs(MySlide, [loadId.OBJ], true,-1 )
					enableBasicControls()
					PresentationController.NumSlides = _view.Thiles
				
				}
				
			}
		}
	
		private function addDirectlyToPresentation(e):void
		{
			var jsonOBJ = JSON.parse(e.target.data)
			for (var i:int = 0; i < jsonOBJ.groups[0].pages.length; i++) 
			{
				jsonOBJ.groups[0].pages[i].thumb_image = jsonOBJ.thumbs_path + jsonOBJ.groups[0].pages[i].thumb_image
			}
			stage.dispatchEvent(new CEvent(CEvent.ADD_THIS_FOLDER_TO_PRESENTATION, jsonOBJ));
		}
		
		private function LoadSlide(e:CEvent):void
		{	
			var obj:Array = []
			
			for (var i:int = 0; i < PresentationController.NumSlides.length; i++) 
			{
				obj.push(PresentationController.NumSlides[i].OBJ.id)
			}
			var selectedID:int = _view.Thiles.indexOf(MySlide.HiglightedThumb)
			stage.dispatchEvent(new CEvent(CEvent.PLAY_THIS_PRESENTATION , [obj, selectedID, "TITLU PC" ]))
			
		}
		
		private function showControls(e:Event):void 
		{
			
		}
		
		private function hideLogedInControls(e:Event):void 
		{
			t.text = OCP.LG.customPresentationT
			
		}
		public function Resize() {
			
			settingsBtn.x = lines.width - settingsBtn.width -25;
			}
		private function Clear(e = null):void
		{
		
			_view.Clear()
			PresentationController.NumSlides = []
			playBtn.mouseEnabled  = false
			playBtn.alpha  = .5
			settingsBtn.mouseEnabled = false
			settingsBtn.alpha = .5
			
			
			if (e && e.data) {
				t.text = e.data
				}
		}

	}

}