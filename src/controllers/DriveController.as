package controllers
{
	import data.Data;
	import data.Login;
	import data.Uploader;
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.text.TextFormat;
	import menus.LeftMenu;
	import popups.DeleteFolderPop;
	import thumbs.CEvent;
	import thumbs.File;
	import thumbs.FileList;
	import thumbs.FolderList;
	import thumbs.Presentation;
	import thumbs.PresentationList;
	import thumbs.Slide;
	import views.ListView;
	
	public class DriveController
	{
		private var LasLoadedGroup:int = 0;
		private var _view:ListView;
		private var currentFolder:Object;
		private var jsonOBJ:Object;
		private var _bar:BarView;
		private var _stage:Stage;
		private var textFormat:TextFormat;
		private var parecebleOBJ:Array;
		private var o:Object;
		public static var editPositions:Boolean = false;
		
		var jsonLoader:URLLoader = new URLLoader()
		public static var Thumbs:Array
		
		public function DriveController(view:DisplayObject, bar:BarView)
		{
			
			_view = view
			_bar = bar
			_bar.d.visible = false
			_bar.p.visible = true
			_view.name = "DriveView"
			_stage = _view.stage
			_view.addEventListener(CEvent.SET_POSITION , setDrivePosition)
			_stage.addEventListener(CEvent.DRAG_THIS_IN, checkEdit)
			_stage.addEventListener(CEvent.FOLDERS_RECIVED, onFolderRecived)
			_stage.addEventListener(CEvent.SHOWING_FLODER_CONTENTS, showFiles)
			_stage.addEventListener(CEvent.DELETE_CURRENT_FOLDER, confirmedDeletFolder);
			_stage.addEventListener(CEvent.VIEW_SLIDE_IN_THIS_FOLDER, loadingFolder);
			_stage.addEventListener(CEvent.VIEW_SLIDE_IN_THIS_FOLDER, preloading)
			_stage.addEventListener(CEvent.LOAD_PUBLIC_PRESENTATIONS,preloading)
			_bar.d.addAllBtn.addEventListener(MouseEvent.CLICK, addAllFilesToCurrentPresentation)
			_bar.p.addAllBtn.addEventListener(MouseEvent.CLICK, addAllSlidesToCurrentPresentation)
			_bar.p.playBtn.addEventListener(MouseEvent.CLICK, playThisPresentation)
			_bar.p.infoBtn.addEventListener(MouseEvent.CLICK, dispatchShowPresentationInfo)
			_bar.d.listViewBtn.addEventListener(MouseEvent.CLICK, showListView)
			_bar.d.editBtn.addEventListener(MouseEvent.CLICK, setEdit)
			_bar.d.gridViewBtn.addEventListener(MouseEvent.CLICK, showGridVeiw)
			_bar.d.infoBtn.addEventListener(MouseEvent.CLICK, showFolderInfo)
			_bar.d.uploadBtn.addEventListener(MouseEvent.CLICK, uploadFolder)
			_bar.d.delBtn.addEventListener(MouseEvent.CLICK, deletFolder)
			
		
			_bar.d.gridViewBtn.gotoAndStop(2)
			DriveController.Thumbs = _view.Thiles
			_stage.addEventListener(CEvent.HERE_ARE_THE_PUBLIC_PRESENTATIONS, recivedPublic)
			_stage.addEventListener(CEvent.LOAD_PRESENTATION_SLIDE, showSlidesInUserPresentation)
			_bar.homeView.addEventListener(MouseEvent.CLICK, showSideMenu)
			
			textFormat = new TextFormat()
			textFormat.color =  '#99499E' 
			
		}
		
		private function setDrivePosition(e:CEvent):void 
		{
			if (_bar.d.visible) {
				Data.getInstance().saveNewPositioning(FolderList.HiglightedThumb.id , formTimeline(e.data))
				}
			
			
		}
		
		private  function formTimeline(e):String {
			var csv:Array = []
			for (var i:int = 0; i < e.length; i++) 
			{
				csv.push( (e[i].isVideo?'V':'' )+ e[i].id )
			}
			
			return  '[' + csv + ']'
		}
		
		
		private function playThisPresentation(e:MouseEvent):void 
		{
			_stage.dispatchEvent(new CEvent(CEvent.PLAY_THIS_PRESENTATION, [o, 0 , 0]))
			
			
		}

		private function showSideMenu(e:MouseEvent):void
		{
			_stage.dispatchEvent(new CEvent(CEvent.SHOW_SIDE_TREE, null))
		}
		
		private function showSlidesInUserPresentation(e:CEvent):void
		{
			_bar.d.visible = false
		
			_bar.p.visible = true
			_view.Clear()
			
			o = e.data.slide_ids
			
			_bar.t.text = LeftMenu.filter + '/' + e.data.title
			_bar.sc.text = 0 + '  '+ OCP.LG.slidesT
					
			if (o && o.length > 0 && o[0] != "")
			{
				parecebleOBJ = []
				for (var i:int = 0; i < o.length; i++)
				{
					parecebleOBJ.push({"id": o[i], "title": ""})
				}
				_view.createThumbs(Slide, parecebleOBJ, true, 0, false)
				_bar.t.text = LeftMenu.filter + '/' + e.data.title
				_bar.sc.text = o.length + '  ' + OCP.LG.slidesT
			}
		
			
		}
		private function recivedPublic(e:CEvent):void
		{
			
			_bar.visible = true
			_bar.d.visible = false
			_bar.p.visible = false
			
			
			_bar.t.text  = LeftMenu.filter
		
			_bar.sc.text = e.data.length + '  ' + OCP.LG.presentationsT
		
			_view.visible = true
			_view.Clear()
			var showArray:Array = []
			for (var i:int = 0; i < e.data.length; i++)
			{
				if (Login.isLogedIn == 2 && e.data[i].access_level == LeftMenu.filter)
				{
					showArray.push(e.data[i])
				}
			}
			
			if (Login.isLogedIn == 1)
			{
				showArray = e.data
			}
			
			_view.createThumbs(Presentation, showArray, false, 0, false)
		}
		
		
		private function preloading(e:Event):void
		{
			_bar.d.visible = false
			_bar.p.visible = false
			//_bar.preload.gotoAndPlay(2)
			_view.Clear();
		}
		
		private function loadingFolder(e:CEvent):void
		{
			_bar.d.visible = true
			_bar.p.visible = false
			_bar.t.text = LeftMenu.filter + '/'  + unescape(decodeURI(e.data.title))
			
		}
		
		private function confirmedDeletFolder(e:Event):void
		{
			_view.Clear();
		}
		
		private function setEdit(e:MouseEvent):void
		{
			DriveController.editPositions = !DriveController.editPositions
			
			for (var i:int = 0; i < _view.Thiles.length; i++)
			{
				var DriveFileList = _view.Thiles[i].Edit(DriveController.editPositions)
			}
			if (!DriveController.editPositions)
			{
				for (var j:int = 0; j < _view.Thiles.length; j++)
				{
					var OBJ = _view.Thiles[j].OBJ
					
					if (Uploader.GroupID == 0 && _view.Thiles[j].numSlides.text != OBJ.title)
					{
						Data.getInstance().updateFolderData(OBJ.id, OBJ.description, _view.Thiles[j].numSlides.text)
					}
					
					else if (_view.Thiles[j].numSlides.text != (OBJ).title)
					{
						Data.getInstance().updateSLide(_view.Thiles[j])
					}
					
				}
				
			}
		
		}
		
		private function checkEdit(e:CEvent = null)
		{
			if (!DriveController.editPositions)
			{
				//_view.showInserLocation()
			}
		}
		
		
		
		
		private function showFiles(e:CEvent)
		{
			
			_bar.d.visible = true
			_bar.d.addAllBtn.alpha = 1
			jsonOBJ = e.data
			_bar.sc.text = jsonOBJ.groups[0].pages.length + '  '+ OCP.LG.filesT
			Uploader.GroupID = jsonOBJ.groups[0].id
			trace("Group id is", Uploader.GroupID, JSON.stringify(jsonOBJ))
			_bar.visible = true;
			_view.visible = true
			showView();
			//_bar.preload.gotoAndStop(1)
	
		
		}
		
		private function showView():void
		{
			_bar.visible = true
			if (_bar.d.listViewBtn.currentFrame == 1)
			{
				showGridVeiw(null);
			}
			else
			{
				showListView(null);
			}
		}
		
		private function onFolderRecived(e:CEvent):void
		{
			_bar.visible = true;
			_bar.d.visible = false
			_bar.p.visible = false
			_bar.t.text = LeftMenu.filter
			
			_bar.sc.text = e.data.length + '  ' +OCP.LG.foldersT
			
			_view.Clear()
			jsonOBJ = e.data
			
			Uploader.GroupID = null
			if (e.data.length > 0)
			{
				showView()
			}
			
			//_bar.preload.gotoAndStop(1)
		}
		
		private function showGridVeiw(e:MouseEvent):void
		{
			
			_view.Clear()
			
			_bar.d.gridViewBtn.gotoAndStop(2)
			_bar.d.listViewBtn.gotoAndStop(1)
			if (jsonOBJ.groups && jsonOBJ.groups.length > 0 && jsonOBJ.groups[0].pages.length > 0)
			{
				_view.createThumbs(File, jsonOBJ.groups[0].pages, false,0,true)
			}
			else if (jsonOBJ)
			{
				
				_view.createThumbs(File, jsonOBJ, false, 0 , true)
				_view.visible = true
			}
		
		}
		
		private function showListView(e:MouseEvent):void
		{
			_bar.d.listViewBtn.gotoAndStop(2)
			_bar.d.gridViewBtn.gotoAndStop(1)
			_view.Clear()
			if (jsonOBJ.groups && jsonOBJ.groups.length > 0 && jsonOBJ.groups[0].pages.length > 0)
			{
				
				_view.createThumbs(FileList, jsonOBJ.groups[0].pages, false, 0, false)
			}
			
			else if (jsonOBJ)
			{
				_view.createThumbs(FileList, jsonOBJ, false, 0, false)
			}
		
		}
		
		
		private function dispatchShowPresentationInfo(e:MouseEvent):void 
		{
			_stage.dispatchEvent(new CEvent(CEvent.SHOW_PRESENTATION_INFO,  PresentationList.HiglightedThumb.OBJ))
		}
		
		private function addAllSlidesToCurrentPresentation(e:MouseEvent):void 
		{
			var o:Object = new Object()
			o.OBJ = jsonOBJ
			trace(32432, PresentationList.HiglightedThumb.OBJ)
			_stage.dispatchEvent(new CEvent(CEvent.INSERT_THIS, PresentationList.HiglightedThumb.OBJ))
		}
		private function addAllFilesToCurrentPresentation(e:MouseEvent):void
		{
			var o:Object = new Object()
			o.OBJ = jsonOBJ
			_stage.dispatchEvent(new CEvent(CEvent.ADD_THIS_FOLDER_TO_PRESENTATION, jsonOBJ))
		}
		
		private function showFolderInfo(e:MouseEvent):void
		{
			_stage.dispatchEvent(new Event(CEvent.SHOW_FOLDER_INFO));
		}
		
		private function deletFolder(e:MouseEvent)
		{
			
			var delPop = new DeleteFolderPop(jsonOBJ.groups[0].id, OCP.LG.doyouWantToDeleteFolderT)
			_stage.addChild(delPop)
		
		}
		
		private function uploadFolder(e:MouseEvent):void
		{
			Uploader.GroupID = jsonOBJ.groups[0].id
			OCP.uploadPop.Show(_bar.t.text)
		}
		public function BarLoading(s:String) {
			_bar.t.text = s
			
			_bar.sc.visible = false
			_bar.d.addAllBtn.alpha = .5
			_bar.d.visible = false
			_bar.p.visible = false
			//_bar.preload.visible = true
			//_bar.preload.gotoAndPlay(2)
		}
		
		public function Resize(x, y, w, h)
		{
			_bar.x = x
			_bar.y = y
			_bar.lines.width = w
			_bar.d.addAllBtn.x = w - 80
			_bar.p.addAllBtn.x = w - 80
			_bar.sc.x = _bar.d.addAllBtn.x - 25
			
		}
		
		
		
		
	
	}

}