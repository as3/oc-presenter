package player
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.TweenMax;
	import controllers.PresentationController;
	import data.Data;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	import popups.MoreInPresentation;
	import slides.slideTextImage;
	import slides.slideVideo;
	import thumbs.MySlide;
	import utils.Gesture;
	import views.GridView;
	
	public class PresentationPlayer extends Sprite
	{
		
		public static var isPresenting:Boolean = false;
		public static var goForward:Boolean = true;
		public static var Slides:Array = [];
		public static  var presentationID:int ;
		public var _slyde;
		private var bit:Bitmap;
		private var tweenSpeed:Number = .5;
	
		private var autoPlayTimer:Timer;
		private var myView:GridView
		private var downBar:bBar;
		private var cont:Sprite;
		private var _isLoading:Boolean = false;
		private var _viewHeight:int = 128;
		private var mPop:MoreInPresentation = new MoreInPresentation()
		private var indexofcurrentslide:Number = 0;
		
		public function PresentationPlayer()
		{
			addEventListener(Event.ADDED_TO_STAGE, init)
		}
		
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			mPop.visible = false
			this.addChild(mPop);
			
			bit = new Bitmap()
			stage.addEventListener(KeyboardEvent.KEY_DOWN, detectKey);
			stage.addEventListener(CEvent.LOAD_PREV, loadPrevSlide);
			stage.addEventListener(CEvent.LOAD_NEXT, loadNextSlide);
			stage.addEventListener(Event.RESIZE, Resize)
			stage.addEventListener(CEvent.START_AUTO_PLAY, autoPlayPresentation);
			stage.addEventListener(CEvent.SHOW_SLIDE, showSlide);
			stage.addEventListener(CEvent.SLIDE_PRELOADED, preloadedSlide)
			
			downBar = new bBar()
			downBar.loadBar.visible = false
			downBar.addEventListener(MouseEvent.ROLL_OVER, popUp)
			downBar.addEventListener(MouseEvent.ROLL_OUT, popDown)
			downBar.menu.homeBtn.addEventListener(MouseEvent.CLICK, showEditeView)
			//downBar.menu.remoBtn.addEventListener(MouseEvent.CLICK, dispatchRemo)
			//downBar.menu.exitBtn.addEventListener(MouseEvent.CLICK, goFullScreen)
			downBar.menu.settingsBtn.addEventListener(MouseEvent.CLICK, settingMenu)
			
			downBar.nub.addEventListener(MouseEvent.MOUSE_DOWN, startDragBar)
			cont = new Sprite()
			this.addChild(cont);
			this.addChild(downBar)
			
			myView = new GridView(undefined, undefined, true, 'horizontal', .5)
			myView.name = "PlayerGrid"
			myView.addEventListener(CEvent.LOAD_SLIDE, LoadSlide)
			downBar.addChild(myView)
			myView.y = 36
			Resize()
			
			var gesture = new Gesture();
			addChild(gesture);
		}
		
		private function settingMenu(e:MouseEvent):void 
		{
			
			this.addChild(mPop)
			mPop.Show()
			mPop.y = downBar.y  - 100
		}
		
	
		
		private function startDragBar(e:MouseEvent):void
		{
			trace("ADDED LISTENER")
			downBar.removeEventListener(MouseEvent.ROLL_OVER, popUp)
			downBar.removeEventListener(MouseEvent.ROLL_OUT, popDown)
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveDragBar)
			stage.addEventListener(MouseEvent.MOUSE_UP, removeDragRESIZE)
		}
		
		private function removeDragRESIZE(e:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveDragBar)
			stage.removeEventListener(MouseEvent.MOUSE_UP, removeDragRESIZE)
			downBar.addEventListener(MouseEvent.ROLL_OVER, popUp)
			downBar.addEventListener(MouseEvent.ROLL_OUT, popDown)
			//Resize()
		}
		
		private function moveDragBar(e:MouseEvent):void
		{
			if (stage.mouseY < stage.stageHeight - 137)
			{
				downBar.y = stage.mouseY - 12 //- downBar.nub.y 
				//downBar.rotation +=1
				downBar.bkg.bkg2.height = stage.stageHeight - downBar.y - 47.35 //- downBar.bkg.height
				myView.Resize(stage.stageWidth, downBar.bkg.bkg2.height)
				_viewHeight = downBar.bkg.bkg2.height
			//	trace("moveing bar", downBar.y)
			}
		}
		
		public function PlayPresentation(slideIDs:Array, selectedID:int = 0, title:String = 'title')
		{
			
		 if (CONFIG::debug != true) {
				 stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE
				 }
			
			this.visible = true
			myView.orientation = 'horizontal'
			var t:TextFormat = new TextFormat()
			t.color = 0xffffff
			TextField(downBar.menu.position).setTextFormat(t)		 
			var obj:Array = []
			
			for (var i:int = 0; i < slideIDs.length; i++)
			{
				obj.push({"id": slideIDs[i], "title": ""})	
			}
			
			myView.Thiles = obj
			PresentationPlayer.Slides = myView.Thiles
			PresentationPlayer.isPresenting = true
			if (!PresentationController.NumSlides || PresentationController.NumSlides.length == 0) {
				PresentationController.NumSlides = myView.Thiles;
				}
			
			var idx = myView.Thiles.indexOf(MySlide.HiglightedThumb ) 
			
			Data.getInstance().PreloadCurrentPResentation(Ready, showProgress, PresentationPlayer.Slides);
			myView.Thiles[selectedID].Select("JUSTPLAY")
		
		}
		
		
		
		private function Ready(e):void 
		{
			//_s.dispatchEvent(new CEvent(CEvent.PRELOAD_COMPLETE , ""))
			var t:TextFormat = new TextFormat()
			t.color = 0xA34091
			
			TextField(downBar.menu.position).setTextFormat(t)
		
		}
		
		private function preloadedSlide(e:CEvent):void 
		{
			
			var t:TextFormat = new TextFormat()
			t.color = 0xffffff
			TextField(downBar.menu.position).setTextFormat(t)
			
			for (var i:int = 0; i < myView.Thiles.length; i++) 
			{
				if (myView.Thiles[i].id == String(e.data).substring(3)) {
					MySlide(myView.Thiles[i]).bkg.gotoAndStop(2)
					
					
					}
			}
		}
		
		private function showProgress(e:LoaderEvent):void
		{
			//LoaderMax.
			//trace(33333, (e.target))
		}
		private function showEditeView(e:MouseEvent):void
		{
			
			MySlide.HiglightedThumb = null
			PresentationPlayer.isPresenting = false;
			myView.Clear()
			if (_slyde)
			{
				cont.removeChild(_slyde)
				_slyde.Destroy()
				_slyde = null
				
			}
			stage.dispatchEvent(new CEvent(CEvent.SHOW_EDIT_VIEW, null))
		}
		
		private function dispatchRemo(e:MouseEvent):void
		{
			stage.dispatchEvent(new CEvent(CEvent.SHOW_REMO_POP, null));
		}
		
		
		
		public function popDown(e=null):void
		{
			if (mouseY < downBar.y || e==null)
			{
				downBar.mouseEnabled = false
				TweenMax.to(downBar, .3, {y: stage.stageHeight - 12, ease: Globals.easeOut})
			}
		}
		
		private function popUp(e = null):void
		{
			if(TweenMax.isTweening(downBar))return
			TweenMax.to(downBar, .3, {y: stage.stageHeight - downBar.height, ease: Globals.easeOut})
		
		}
		
		public static function easeOut(t:Number, b:Number, c:Number, d:Number):Number
		{
			var s = .70158;
			return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
		
		}
		
		private function goNExtSlide(e:TimerEvent):void
		{
			
			trace("453245345", indexofcurrentslide, myView.Thiles.length)
			if (VPVasa.IsPlaying || _isLoading)
				return
					
					indexofcurrentslide++;
			if (indexofcurrentslide == myView.Thiles.length)
			{
				indexofcurrentslide = 0;
			}
			MySlide(myView.Thiles[indexofcurrentslide]).Select('JUSTPLAY')
		trace("sssss")
		}
		
		private function autoPlayPresentation(e:CEvent):void
		{
			indexofcurrentslide = 0;
			autoPlayTimer = new Timer(e.data * 1000)
			autoPlayTimer.addEventListener(TimerEvent.TIMER, goNExtSlide)
			this.addEventListener(MouseEvent.CLICK, StopAutoPlay)
			autoPlayTimer.start()
		
		}
		
		private function loadPrevSlide(e:Event):void
		{
			
			if (myView.Thiles.indexOf(myView.selectedThumbnail) == 0)
			{
				TweenMax.from(_slyde, tweenSpeed, {x: 100, ease: OCP.easeOut})
				return
			}
			PresentationPlayer.goForward = false
			myView.Thiles[myView.Thiles.indexOf(myView.selectedThumbnail) - 1].Select("JUSTPLAY");
		
		}
		
		private function loadNextSlide(e:Event):void
		{
			if (myView.Thiles.indexOf(myView.selectedThumbnail) == myView.Thiles.length - 1)
			{
				TweenMax.from(_slyde, tweenSpeed, {x: -100, ease: OCP.easeOut})
				return
			}
			PresentationPlayer.goForward = true
			myView.Thiles[myView.Thiles.indexOf(myView.selectedThumbnail) + 1].Select("JUSTPLAY");
		
		}
		
		private function LoadSlide(e:CEvent):void
		{
			
			
			downBar.loadBar.width = 0
			downBar.loadBar.visible = true
			_isLoading = true
			if (_slyde)
			{
				var bd:BitmapData = new BitmapData(Globals._DIM.width, Globals._DIM.height, false, 0x313138)
				bd.draw(_slyde)
				bit.bitmapData = bd
				bit.alpha = 1
				//bit.x = 0
				cont.addChild(bit)
				_slyde.Destroy()
				
			}
			var currentSlide:MySlide = e.data
			indexofcurrentslide = myView.Thiles.indexOf(currentSlide)
			downBar.menu.position.text  = (indexofcurrentslide+1) + '/' + myView.Thiles.length
		
			if (Data.preloadedOBJ ) {
				
				for (var j:int = 0; j < Data.preloadedOBJ.pages.length; j++) 
				{
					
					if (Data.preloadedOBJ.pages[j].id == currentSlide.id ) {
						trace("HERE IT IS")
						showSlide(new CEvent("", Data.preloadedOBJ.pages[j]))
						
						return
						}
				}	
				
			}
			
				Data.getInstance().LoadSlide(e.data.id);		
		}
		
		
		
		private function showSlide(e:CEvent):void
		{
			
			if (!PresentationPlayer.isPresenting || e.data == null)
			{
				return
			}
			if (_slyde)
			{
				_slyde.rotation = 32;
				cont.removeChild(_slyde)
				_slyde.Destroy()
				_slyde = null
				
			}
		
			var slideJ:Object = e.data//slideJSON.pages[0]
			switch (slideJ.type)
			{
				case 'slideTextImage': 
					_slyde = new slideTextImage(slideJ)
					break;
				case 'slideVideo': 
					_slyde = new slideVideo(slideJ)
					break;
				default:
			
			}
			stage.focus = _slyde
			
			cont.addChild(_slyde)
			_slyde.Resize(e)
			_slyde.addEventListener(ProgressEvent.PROGRESS, displayLoadProgress)
			_slyde.addEventListener(Event.COMPLETE, slydeLoaded)
			//cont.addChild(bit)
			bit.alpha = 1
			bit.x = 0
			if (PresentationPlayer.goForward)
			{
				TweenMax.from(_slyde, tweenSpeed, {x: Globals._DIM.width, ease: PresentationPlayer.easeOut})
				TweenMax.to(bit, tweenSpeed, {x: -Globals._DIM.width, ease: PresentationPlayer.easeOut, onComplete: disposeBitmap})
			}
			else
			{
				TweenMax.from(_slyde, tweenSpeed, {x: -Globals._DIM.width, ease: PresentationPlayer.easeOut})
				TweenMax.to(bit, tweenSpeed, {x: Globals._DIM.width, ease: PresentationPlayer.easeOut, onComplete: disposeBitmap})
			}
			_isLoading = false
		
		}
		
		private function slydeLoaded(e:Event):void
		{
			_slyde.removeEventListener(Event.COMPLETE, slydeLoaded)
			_slyde.removeEventListener(ProgressEvent.PROGRESS, displayLoadProgress)
			downBar.loadBar.visible = false
		}
		
		private function displayLoadProgress(e:ProgressEvent):void
		{
			downBar.loadBar.width = downBar.bkg.width * (e.bytesLoaded / e.bytesTotal)
		}
		
		public function PlayPause(e:Event):void
		{
			try
			{
				_slyde.pauseUnpose();
			}
			catch (e)
			{
				trace(e)
			}
		}
		
		function detectKey(e:KeyboardEvent):void
		{
			
			
			if (e.keyCode == 38) {
				popUp()
				}
			if (e.keyCode == 40) {
				popDown()
				}
			if (e.keyCode == 27)
			{
				showEditeView(null)
				return
			}
			if (_slyde)
			{
				if (e.keyCode == 32 || e.keyCode == 39 || e.keyCode == 34)
				{
					_slyde.NEXT(e)
					
				}
				else if (e.keyCode == 37 || e.keyCode == 33)
				{
					
					_slyde.PREV(e)
					
				}
				
			}
		
		}
		
		function StopAutoPlay(e:MouseEvent)
		{
			trace("STOP IT")
			if (autoPlayTimer)
			{
				autoPlayTimer.stop();
				this.removeEventListener(MouseEvent.CLICK, StopAutoPlay)
			}
		}
		
		public function disposeBitmap():void
		{
			if (bit.bitmapData)
			{
				bit.bitmapData.dispose()
			}
			if (_slyde)
			{
				_slyde.TransitionDone();
			}
		}
		
		public function closeSlide(e:Event):void
		{
			if (_slyde)
			{
				_slyde.Destroy()
				removeChild(_slyde)
				groupCont.visible = true
				backGround.visible = true
				_slyde = null
			}
		}
		
		public function Resize(e:Event = null):void
		{
			if (_slyde)
			{
				_slyde.Resize()
			}
			
			downBar.loadBar.x = stage.stageWidth / 2
			downBar.bkg.width = stage.stageWidth
			downBar.loadBar.width = downBar.bkg.width
			downBar.nub.x = downBar.bkg.width / 2
			downBar.menu.x = stage.stageWidth - downBar.menu.width - 7
			myView.Resize(stage.stageWidth, _viewHeight)
			downBar.y = stage.stageHeight - 12
			mPop.x = downBar.menu.x + mPop.width - mPop.width/2+ 30
			mPop.y = downBar.y  - 100
		}
		
		public function Clear()
		{
			removeChildren()
		}
		
		public function Stop(e = null):void
		{
			
			PresentationPlayer.isPresenting = false
			
			if (autoPlayTimer)
			{
				autoPlayTimer.stop();
			}
			this.visible = false
			if (_slyde)
			{
				cont.removeChild(_slyde)
				_slyde.Destroy()
				_slyde = null
				
			}
		}
	}

}