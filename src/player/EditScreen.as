package player
{
	import com.greensock.TweenMax;
	import controllers.DriveController;
	import controllers.PresentationController;
	import data.Data;
	import data.Login;
	import data.Uploader;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.ui.Mouse;
	import flash.utils.setTimeout;
	import menus.LeftMenu;
	import menus.RighMenu;
	import player.EditScreen;
	import player.PresentationPlayer;
	import popups.CreateUserPop;
	import popups.LogInPop;
	import popups.SharedStartPop;
	import popups.UploadPop;
	import slides.slideVideo;
	import thumbs.File;
	import thumbs.FileList;
	import utils.Gesture;
	import views.GridView;
	import views.ListView;
	
	public class EditScreen extends Sprite
	{
		var _:Array = [GridView, File, slideVideo, FileList];
		private var backGround:BackGround = new BackGround();
		private var lineDrag:verticalDragLine = new verticalDragLine()
		private var loginPop:LogInPop;
		private var createUserPop:CreateUserPop;
		private var presentationContoller:PresentationController;
		private var driveController:DriveController;
		
		private var topMenu:TopMenu
		private var leftM:LeftMenu
		private var rightMenu:RighMenu;
		private var timelineHash:String;
		private var myView:ListView
		private var publicView:ListView
		private var _editScreen:EditScreen
	
		private var dragbarInitialPosition:Number = 187;
		public function EditScreen():void
		{
			addEventListener(Event.ADDED_TO_STAGE, init)
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			Globals._DIM = backGround
			setTimeout(doFocus, 100);
			myView = new ListView(undefined, undefined, true, 'vertical', .5)
			
			stage.addEventListener(CEvent.LOGED_IN, loadUserPresentations)
			stage.addEventListener(CEvent.LOGED_OUT, logedOut)
			stage.addEventListener(CEvent.LOG_IN_FAIL, logedOut)
			stage.addEventListener(CEvent.SHOW_SIDE_TREE, showTreeMenu)
			stage.addEventListener(CEvent.WE_HAVE_LINK, showGgoogleBtn)
			stage.scaleMode = StageScaleMode.NO_SCALE
			stage.align = StageAlign.TOP_LEFT
			stage.addEventListener(Event.RESIZE, Resize)
			stage.tabChildren = false;
			addChild(backGround)
			
			topMenu = new TopMenu();
			topMenu.version.text = "v 1.0"
			topMenu.rightSide.downloadBtn.visible = false
			topMenu.rightSide.settingsBtn.visible = false
			topMenu.rightSide.logInBtn.addEventListener(MouseEvent.CLICK, showLogInPop)
			topMenu.rightSide.createBtn.addEventListener(MouseEvent.CLICK, showNewUserPOP)
			topMenu.rightSide.fullScreenBtn.addEventListener(MouseEvent.CLICK, goFullScreenInteractive)
			topMenu.rightSide.logGoogleBtn.addEventListener(MouseEvent.CLICK, Login.callGoogleLogIn)
			topMenu.tabEnabled = false;
			topMenu.rightSide.tabEnabled = false;
			addChild(topMenu)
			publicView = new ListView(undefined, undefined, false, 'verical', 1, false, 'top', false, false);
			
			addChild(publicView)
			loginPop = new LogInPop()
			
			createUserPop = new CreateUserPop()
			createUserPop.box.userCreated.doneBtn.addEventListener(MouseEvent.CLICK, showLogInPop)
			
			leftM = new LeftMenu();
			leftM.visible = false
			leftM.y = topMenu.height
			rightMenu = new RighMenu()
			addChild(rightMenu)
			
			presentationContoller = new PresentationController(myView)
			rightMenu.tabEnabled = false
			
			var drive_bar:BarView = new BarView();
			addChild(drive_bar);
			
			driveController = new DriveController(publicView, drive_bar)
			new Uploader(stage);
			OCP.uploadPop = new UploadPop();
			
			addChild(presentationContoller)
			addChild(topMenu)
			stage.stageFocusRect = false;
			lineDrag.nub.addEventListener(MouseEvent.MOUSE_DOWN, startResize)
			
			
			addChild(lineDrag)
			//addChild(presentationContoller)
			addChild(topMenu)
			rightMenu.x = stage.stageWidth - rightMenu.bkg.width
			lineDrag.x = rightMenu.x - dragbarInitialPosition
			lineDrag.y = topMenu.height
			addChild(myView)
			addChild(presentationContoller)
			addChild(rightMenu)
			addChild(leftM)
			stage.addChild(createUserPop)
			stage.addChild(OCP.uploadPop);
			stage.addChild(loginPop)
			publicView.y = topMenu.height + 61.10
			Show(null)
			Resize(null)
		
		}
		
		private function showGgoogleBtn(e:CEvent):void 
		{
			topMenu.rightSide.logGoogleBtn.visible = true
			topMenu.rightSide.logGoogleBtn.addEventListener(MouseEvent.CLICK, Login.callGoogleLogIn)
		}
		
		private function showLogInPop(e = null):void
		{
			
			if (Login.isLogedIn == 1)
			{
				loginPop.Show();
			}
			else
			{
				
				Login.getInstance().LogOut();
				Data.getInstance().loadPublicPresentations();
				
			}
		}
		
		private function showTreeMenu(e:CEvent):void
		{
			if (leftM.visible)
			{
				leftM.visible = false
			}
			else
			{
				leftM.visible = true
			}
			
			Resize()
		}
		
		private function doFocus(event:Event = null):void
		{
			if (ExternalInterface.available)
			{
				ExternalInterface.call("setupSWFObjectFocus");
			}
		}
		
		private function startResize(e:MouseEvent):void
		{
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveLine)
			stage.addEventListener(MouseEvent.MOUSE_UP, removeLineDrag)
		}
		
		private function removeLineDrag(e:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_UP, removeLineDrag)
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveLine)
			if (lineDrag.x + 200 > rightMenu.x)
			{
				TweenMax.to(lineDrag, .2, {x: rightMenu.x - dragbarInitialPosition, onUpdate: Resize})
				
			}
			else if (lineDrag.x < leftM.x + leftM.bkg.width + (leftM.visible ? 300 : 0))
			{
				TweenMax.to(lineDrag, .2, {x: leftM.x + leftM.bkg.width + (leftM.visible ? 300 : 0), onUpdate: Resize})
			}
		
		}
		
		private function moveLine(e:MouseEvent):void
		{
			
			if (mouseX + 40 > (leftM.visible ? 300 : 0) && mouseX + 7 < rightMenu.x)
			{
				lineDrag.x = mouseX
				lineDrag.bkg.width = stage.stageWidth
				lineDrag.bkg.height = stage.stageHeight - topMenu.height
				Resize(null)
				e.updateAfterEvent()
			}
		}
		
		private function showNewUserPOP(e:MouseEvent):void
		{
			createUserPop.Show()
		}
		
		private function logedOut(e:Event):void
		{
			topMenu.rightSide.logInBtn.gotoAndStop(1);
			topMenu.rightSide.userN.text = '';
			topMenu.rightSide.downloadBtn.visible = false
			topMenu.rightSide.settingsBtn.visible = false
			topMenu.rightSide.createBtn.visible = true
			if (LogInPop.isLogedIn == 2)
			{
				leftM.visible = true
			}
			else
			{
				leftM.visible = false
			}
			
			if (!PresentationPlayer.isPresenting)
			{
				driveController.BarLoading("Public Presentations")
				Data.getInstance().loadPublicPresentations();
			}
			Resize(null)
		}
		
		private function loadUserPresentations(e:CEvent):void
		{
			
			topMenu.rightSide.settingsBtn.visible = true
			topMenu.rightSide.createBtn.visible = false;
			topMenu.rightSide.logInBtn.gotoAndStop(2);
			topMenu.rightSide.userN.text = e.data['full_name'];
			if (Login.SuperUser)
			{
				topMenu.rightSide.userN.text += " $"
			}
			leftM.visible = true
			topMenu.rightSide.userDetalis.addEventListener(MouseEvent.CLICK, showUserInfo)
			topMenu.rightSide.settingsBtn.addEventListener(MouseEvent.CLICK, showUserInfo)
			this.loginPop.visible = false;
			publicView.x = leftM.visible ? leftM.bkg.width : 0
			Resize(null)
			
			if (e.data['password_reset'] == 1)
			{
				createUserPop.ShowUpdate(true)
			}
		}
		
		private function showUserInfo(e:MouseEvent):void
		{
			createUserPop.ShowUpdate()
		}
		
		private function goFullScreenInteractive(e = null)
		{
			
			if (stage.displayState == StageDisplayState.NORMAL)
			{
				topMenu.rightSide.fullScreenBtn.gotoAndStop(2)
				stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
				
			}
			else
			{
				Mouse.show()
				topMenu.rightSide.fullScreenBtn.gotoAndStop(1)
				
				stage.displayState = StageDisplayState.NORMAL;
			}
		}
		
		public function Show(e:Event):void
		{
			this.visible = true
		}
		
		public function Resize(e:Event = null):void
		{
			
			
			backGround.width = stage.stageWidth
			backGround.height = stage.stageHeight
			
			rightMenu.x = stage.stageWidth - rightMenu.bkg.width
			if (lineDrag.x + 10 > rightMenu.x)
			{
				lineDrag.x = rightMenu.x - 10
			}
			rightMenu.y = topMenu.height
			rightMenu.Resize()
			
			topMenu.rightSide.x = stage.stageWidth - topMenu.rightSide.fullScreenBtn.x + 18
			topMenu.logo.visible = true
			topMenu.version.visible = true
			if (topMenu.rightSide.x < 300)
			{
				topMenu.logo.visible = false
				topMenu.version.visible = false
			}
			lineDrag.bkg.width = stage.stageWidth - lineDrag.x
			lineDrag.bkg.height = stage.stageHeight - topMenu.height
			lineDrag.line.height = stage.stageHeight - topMenu.height
			
			var isLeft = leftM.visible ? leftM.bkg.width : 0
			
			if (lineDrag.x < isLeft)
			{
				lineDrag.x = isLeft
			}
			
			lineDrag.visible = true
			publicView.x = isLeft
			
			myView.Resize(rightMenu.x - lineDrag.x - 10, stage.stageHeight - 155)
			myView.x = lineDrag.x + 10
			
			myView.y = topMenu.height + 61.10//presentationContoller.y + presentationContoller.lines.height + presentationContoller.lines.y
			presentationContoller.x = myView.x + 5
			
			presentationContoller.y = topMenu.height
			
			presentationContoller.lines.width = stage.stageWidth - lineDrag.x - leftM.bkg.width 
			presentationContoller.Resize();
			publicView.Resize(lineDrag.x - isLeft, stage.stageHeight - publicView.y)
			driveController.Resize(isLeft, topMenu.height, lineDrag.x - isLeft, stage.stageHeight - publicView.y);
			topMenu.bkg.width = stage.stageWidth;
			leftM.Resize()
			lineDrag.nub.y = lineDrag.line.height / 2 - lineDrag.nub.height / 2 + topMenu.height / 2
		
		}
	
	}
}